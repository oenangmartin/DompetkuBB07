package com.indosatapps.dompetku;

import java.util.Vector;

import javax.microedition.pim.Contact;
import javax.microedition.pim.PIM;
import javax.microedition.pim.PIMItem;

import net.rim.blackberry.api.pdap.BlackBerryContact;
import net.rim.blackberry.api.pdap.BlackBerryContactList;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.DialogClosedListener;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.text.TextFilter;
import net.rim.device.api.util.StringUtilities;

import org.json.me.JSONObject;

import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.ButtonFieldCustom;
import com.indosatapps.dompetku.field.EditFieldCustom;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.PasswordEditFieldCustom;
import com.indosatapps.dompetku.field.PleaseWaitPopupScreen;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.Connection;
import com.indosatapps.dompetku.utils.PageResultListener;

public class TransferOpLain extends MainScreen implements FieldChangeListener{
	Mediator mediator;
	EditFieldCustom editNoTujuan, editJumlah;
	PasswordEditFieldCustom editPIN;
	ButtonFieldCustom btnSubmit;
	PageResultListener listener;
	Vector numberList = new Vector();
	
	public TransferOpLain(Mediator mediator, PageResultListener listener) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.listener = listener;
		this.mediator = mediator;
		HeaderField header = new HeaderField("Operator Lain", 0);
		add(header);
		
		editNoTujuan = new EditFieldCustom("Nomor Tujuan", "", Constant.deviceWidth-30, 16, false, true, FIELD_HCENTER);
		editNoTujuan.setFilter(TextFilter.get(TextFilter.PHONE));
		editJumlah = new EditFieldCustom("Jumlah", "", Constant.deviceWidth-30, 10, false, false, FIELD_HCENTER);
		editJumlah.setFilter(TextFilter.get(TextFilter.NUMERIC));
		editPIN = new PasswordEditFieldCustom("PIN", "", Constant.deviceWidth-30, 6, FIELD_HCENTER);
		editPIN.setFilter(TextFilter.get(TextFilter.NUMERIC));
		
		editNoTujuan.setBtnSearchListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context) {
				try{
					BlackBerryContactList list = (BlackBerryContactList)PIM.getInstance().
												openPIMList(PIM.CONTACT_LIST, PIM.READ_ONLY);
					PIMItem contact = list.choose();
					processContact(contact);
				}catch (Exception e) {
					System.out.println(">> Contact Picker error: "+e.getMessage());
					e.printStackTrace();
					Dialog.alert("Gagal membuka Kontak.");
				}
			}
		});
		
		btnSubmit = new ButtonFieldCustom("KIRIM", Constant.deviceWidth/3, FIELD_HCENTER);
		btnSubmit.setEditable(true);
		btnSubmit.setMargin(10, 0, 10, 0);
		btnSubmit.setChangeListener(this);
		
		VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL);
		scr.setMargin(15, 15, 15, 15);
		scr.add(editNoTujuan.getField());
		scr.add(editJumlah.getField());
		scr.add(editPIN.getField());
		scr.add(btnSubmit);
		
		add(scr);
	}

	public void resetFieldState(){
        editNoTujuan.setIsError(false);
        editJumlah.setIsError(false);
        editPIN.setIsError(false);
    }
	
	public void processContact(PIMItem contact){
		if(contact!=null && contact instanceof BlackBerryContact){
			numberList.removeAllElements();
			int telephonesCount = contact.countValues(Contact.TEL);
			if(telephonesCount > 0){
			    for(int i=0; i< telephonesCount; ++i) {
			    	String number = contact.getString(Contact.TEL, i);
			    	if(number.indexOf(' ') > -1)
			    		number = StringUtilities.removeChars(number, " ");
			        if(number.indexOf("-") > -1)
			        	number = StringUtilities.removeChars(number, "-");
			        if(number.startsWith("+62"))
			        	number = "0"+number.substring(3);
			        
			        int attribute = contact.getAttributes(BlackBerryContact.TEL, i);
	                if(attribute == Contact.ATTR_MOBILE){
	                	numberList.addElement(new String[]{"Mobile: ", number});
	                }else if (attribute == Contact.ATTR_HOME){
	                	numberList.addElement(new String[]{"Home: ", number});
			    	}else if (attribute == Contact.ATTR_WORK){
			    		numberList.addElement(new String[]{"Work: ", number});
			    	}
	            }
			    
			    if(numberList.size()>0){
			    	if(numberList.size() == 1){
			    		String[] msisdn = (String[]) numberList.elementAt(0);
				    	editNoTujuan.setText(msisdn[1]);
			    	}else{
			    		ContactDialog d = new ContactDialog(numberList);
			    		d.doModal();
			    	}
			    }else{
			    	Dialog.alert("Nomor tidak ditemukan.");
			    }
			}else{
				//no number
				Dialog.alert("Nomor tidak ditemukan.");
			}
        }
	}
	
	public void fieldChanged(Field field, int context) {
		if(field == btnSubmit){
			String mTujuan = editNoTujuan.getText().toString();
			String mJumlah = editJumlah.getText().toString();
			String mPIN = editPIN.getText().toString();
            
            resetFieldState();
            if(mTujuan.length() == 0){
                editNoTujuan.setIsError(true);
                Dialog.alert("Mohon isi Nomor Tujuan");
            }else if(!mTujuan.startsWith("0811") && !mTujuan.startsWith("0812") && !mTujuan.startsWith("0813") && !mTujuan.startsWith("0821")
                && !mTujuan.startsWith("0822") && !mTujuan.startsWith("0823") && !mTujuan.startsWith("0851") && !mTujuan.startsWith("0852")
                && !mTujuan.startsWith("0853")
                //XL
                && !mTujuan.startsWith("0817") && !mTujuan.startsWith("0818") && !mTujuan.startsWith("0819") && !mTujuan.startsWith("0859")
                && !mTujuan.startsWith("0877") && !mTujuan.startsWith("0878") && !mTujuan.startsWith("0879")){
                //not Tsel or XL!
                editNoTujuan.setIsError(true);
                Dialog.alert("Nomor Tujuan harus operator Telkomsel atau XL");
            }else if(mJumlah.length() == 0){
                editJumlah.setIsError(true);
                Dialog.alert("Mohon isi Jumlah");
            }else if(mPIN.length() == 0){
                editPIN.setIsError(true);
                Dialog.alert("Mohon isi PIN");
            }else{
            	new DoTransferTask(mTujuan, mJumlah, mPIN);
            }
		}
	}
	
	public class DoTransferTask implements Runnable{
		String username, pin, to, amount;
		
		public DoTransferTask(String to, String amount, String pin) {
			username = Constant.mPersistData.getUsername();
			this.to = to;
			this.amount = amount;
			this.pin = pin;
			PleaseWaitPopupScreen.showScreenAndWait(this, "Memproses transaksi...");
		}
		
		public void run() {
			final String entity = Connection.doTransferOperatorLain(username, pin, to, amount);
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject objs = new JSONObject(entity);
                            int status = objs.getInt("status");
                            if(status == 0){
                        		/*String trxID = objs.getString("trxid");
                                long currTimeMilis = System.currentTimeMillis();
                                String currTime = BasicUtils.formatDateFromMilliseconds(currTimeMilis, "dd/MM/yyyy HH:mm");
                                InboxData inboxData = new InboxData(trxID, currTimeMilis, editJumlah.getText(),
                                        editNoTujuan.getText(), "-", "Kirim", "Uang", "-", "-", "-");
                                Constant.mListInbox.addElement(inboxData);
                                Constant.mHashMapInbox.put(Long.toString(inboxData.getDate()), inboxData);
                                BasicUtils.saveInboxData();*/
                                
                            	String msg = "Anda akan menerima konfirmasi pop-up di handphone Anda.";
                                BasicUtils.showMessageDialog(msg, new DialogClosedListener() {
									public void dialogClosed(Dialog dialog, int choice) {
										if(choice == Dialog.OK){
											listener.onPageResult(0, Constant.RESULT_OK, null);
		                                	close();
										}
									}
								});
                            }else if(status == 1001){
                            	Dialog.alert("Saldo Anda tidak mencukupi untuk melakukan transaksi ini.");
                            }else if(status == 1014){
                            	Dialog.alert("PIN salah. Mohon periksa kembali PIN Anda.");
                            }else if(status == 23){
                            	Dialog.alert("Jumlah uang yang ditransfer kurang dari jumlah minimum  transaksi.");
                            }else if(status == 24){
                            	Dialog.alert("Jumlah terlalu besar");
                            }else{
                            	Dialog.alert("Transaksi gagal.");
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Dialog.alert(Constant.txt_error_json);
                        }
                    }else{
                    	Dialog.alert(Constant.txt_error_no_connection);
                    }
                }
			});
		}
	}
	
	class ContactDialog extends Dialog{
		
	    public ContactDialog(Vector numberList){
	        super("Pilih nomor:", null, null, Dialog.DISCARD, null, Dialog.GLOBAL_STATUS);
	        
	        VerticalFieldManager vfm = new VerticalFieldManager(FIELD_HCENTER);

	        for(int i=0;i<numberList.size();i++){
	        	final String[] msisdn = (String[]) numberList.elementAt(i);
	        	ButtonField button = new ButtonField(msisdn[0]+msisdn[1], FIELD_HCENTER);
	        	button.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						editNoTujuan.setText(msisdn[1]);
						close();
					}
				});
	        	vfm.add(button);
	        }
	        
		    this.add(vfm);
	    }
	    
	    public boolean keyChar(char key, int status, int time) {
	    	if(key==Characters.ESCAPE){
	    		this.close();
	    	}
	        return true;
	     }
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }
}
