package com.indosatapps.dompetku;

import java.util.Calendar;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TouchGesture;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.CheckboxField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.DialogClosedListener;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.RadioButtonField;
import net.rim.device.api.ui.component.RadioButtonGroup;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.picker.DateTimePicker;
import net.rim.device.api.ui.text.TextFilter;

import org.json.me.JSONObject;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.ButtonFieldCustom;
import com.indosatapps.dompetku.field.DropDownFieldCustom;
import com.indosatapps.dompetku.field.EditFieldCustom;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.LabelFieldColor;
import com.indosatapps.dompetku.field.PasswordEditFieldCustom;
import com.indosatapps.dompetku.field.PleaseWaitPopupScreen;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.Connection;
import com.thirdparty.device.api.ui.container.EvenlySpacedToolbar;

public class Register extends MainScreen implements FieldChangeListener{
	Mediator mediator;
	EditFieldCustom mEmail, mMSISDN, mFirstName, mLastName, mAddr, mDOB, mIDNumber, mMother;
	PasswordEditFieldCustom mPIN, mPINConfirm;
	RadioButtonGroup mGender;
	RadioButtonField mGenderMale, mGenderFemale;
	DropDownFieldCustom mIDType;
	CheckboxField cbTnC;
	LabelField txtTnC;
	ButtonFieldCustom btnRegis;
	DateTimePicker datePicker;
	int intMale, intFemale;
	
	public Register(Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		HeaderField header = new HeaderField("Registrasi", 0);
		add(header);
		
		//for edit field
		mMSISDN = new EditFieldCustom("No HP*", "", Constant.deviceWidth-30, 16, false, false, FIELD_HCENTER);
		mMSISDN.setFilter(TextFilter.get(TextFilter.PHONE));
		mEmail = new EditFieldCustom("Email", "", Constant.deviceWidth-30, 50, false, false, FIELD_HCENTER);
		mEmail.setFilter(TextFilter.get(TextFilter.EMAIL));
		mFirstName = new EditFieldCustom("Nama Depan*", "", Constant.deviceWidth-30, 20, false, false, FIELD_HCENTER);
		mFirstName.setFilter(TextFilter.get(TextFilter.DEFAULT));
		mLastName = new EditFieldCustom("Nama Belakang", "", Constant.deviceWidth-30, 20, false, false, FIELD_HCENTER);
		mLastName.setFilter(TextFilter.get(TextFilter.DEFAULT));
		mAddr = new EditFieldCustom("Alamat", "", Constant.deviceWidth-30, 255, false, false, FIELD_HCENTER);
		mAddr.setFilter(TextFilter.get(TextFilter.DEFAULT));
		mIDNumber = new EditFieldCustom("Nomor Identitas", "", Constant.deviceWidth-30, 16, false, false, FIELD_HCENTER);
		mIDNumber.setFilter(TextFilter.get(TextFilter.DEFAULT));
		mMother = new EditFieldCustom("Nama Ibu Kandung*", "", Constant.deviceWidth-30, 40, false, false, FIELD_HCENTER);
		mMother.setFilter(TextFilter.get(TextFilter.DEFAULT));
		mPIN = new PasswordEditFieldCustom("PIN*", "", Constant.deviceWidth-30, 6, FIELD_VCENTER);
		mPIN.setFilter(TextFilter.get(TextFilter.NUMERIC));
		mPINConfirm = new PasswordEditFieldCustom("Konfirmasi PIN*", "", Constant.deviceWidth-30, 6, FIELD_VCENTER);
		mPINConfirm.setFilter(TextFilter.get(TextFilter.NUMERIC));
		
		//for Gender (Radio Button)
		LabelFieldColor labelGender = new LabelFieldColor("Jenis Kelamin*", Constant.colorFontGeneral, 0);
		labelGender.setMargin(0, 10, 0, Assets.bgEditText[0].getWidth());
		labelGender.setFont(Assets.fontGlobal);
		
		mGender = new RadioButtonGroup();
		mGenderMale = new RadioButtonField("Pria", mGender, false) {
		    protected void layout(int width, int height) {
		        super.layout(getPreferredWidth(), height);
		    }
		};
		mGenderFemale = new RadioButtonField("Wanita", mGender, false) {
		    protected void layout(int width, int height) {
		        super.layout(getPreferredWidth(), height);
		    }
		};
		
		EvenlySpacedToolbar genderChoices = new EvenlySpacedToolbar(USE_ALL_WIDTH);
		genderChoices.setMargin(5, 0, 0, 0);
		genderChoices.add(mGenderMale);
		genderChoices.add(mGenderFemale);
		
		VerticalFieldManager genderLayout = new VerticalFieldManager(USE_ALL_WIDTH);
		genderLayout.setMargin(0, 0, 15, 0);
		genderLayout.add(labelGender);
		genderLayout.add(genderChoices);
		
		//for DOB (Calendar)
		EditField editDOB = new EditField("", "Klik untuk membuka kalender", EditField.DEFAULT_MAXCHARS, FIELD_HCENTER|EditField.NO_NEWLINE){
			protected void paint(Graphics g){
				g.setColor(Constant.colorFontFormInput);
        	    super.paint(g);
        	}
			
			protected boolean navigationClick(int status, int time) {
				UiApplication.getUiApplication().invokeLater(new Runnable() {
	                public void run() {
	                	datePicker = DateTimePicker.createInstance(datePicker.getDateTime(), "dd MMM yyyy", null);
				        datePicker.doModal();
				        
				        Calendar c = datePicker.getDateTime();
				        setDate(c);
	                }
				});
				return true;
			}
			
			protected boolean touchEvent(TouchEvent message){
				if(message.getEvent()==TouchEvent.GESTURE)
				{
					if(message.getGesture().getEvent()==TouchGesture.TAP)
					{
						UiApplication.getUiApplication().invokeLater(new Runnable() {
			                public void run() {
			                	datePicker = DateTimePicker.createInstance(datePicker.getDateTime(), "dd MMM yyyy", null);
						        datePicker.doModal();
						        
						        Calendar c = datePicker.getDateTime();
						        setDate(c);
			                }
						});
						return true;
					}
				}
				return super.touchEvent(message);
			}
		};
		editDOB.setEditable(false);
		editDOB.setFont(Assets.fontGlobal);
		datePicker = DateTimePicker.createInstance(Calendar.getInstance(), "dd MMM yyyy", null);
		mDOB = new EditFieldCustom("Tanggal Lahir*", editDOB, "", Constant.deviceWidth-30, 15, false, false, FIELD_HCENTER);
		
		//for IDType (Dropdown)
		String[] idTypeChoices = new String[]{"KTP", "SIM", "KITAS"};
		String[] idTypeValues = new String[]{"1", "2", "3"};
		mIDType = new DropDownFieldCustom("Tipe Identitas", Constant.deviceWidth-30, idTypeChoices, idTypeValues, 0, FIELD_HCENTER);
		
		//for TnC
		cbTnC = new CheckboxField("", false, FIELD_VCENTER);
		txtTnC = new LabelField("Saya setuju dengan Syarat & Ketentuan layanan Dompetku", FOCUSABLE|FIELD_HCENTER|FIELD_VCENTER){
			protected void paint(Graphics g) {
				if(isFocus()){
					g.setColor(Constant.colorBgHyperlink);
					g.drawRoundRect(0, 0, getWidth(), getHeight(), 10, 10);
					g.fillRoundRect(0, 0, getWidth(), getHeight(), 10, 10);
				}
				g.setColor(Constant.colorFontGeneral);
				super.paint(g);
			}
			
			protected void drawFocus(Graphics g, boolean bo){}
			
			protected void onFocus(int direction){
				super.onFocus(direction);
				invalidate();
			}
			
			protected void onUnfocus(){
				super.onUnfocus();
				invalidate();
			}
			
			protected boolean keyChar(char character, int status, int time)
			{
				if(character==Characters.ENTER){
					fieldChangeNotify(0);
					return true;
				}
				return super.keyChar(character, status, time);
			}
			
			protected boolean navigationClick(int status, int time)
			{
				fieldChangeNotify(0);
				return true;
			}
			
			protected boolean touchEvent(TouchEvent message)
			{
				if(message.getEvent()==TouchEvent.GESTURE)
				{
					if(message.getGesture().getEvent()==TouchGesture.TAP)
					{
						fieldChangeNotify(0);
						return true;
					}
				}
				return super.touchEvent(message);
			}
		};
		txtTnC.setFont(Assets.fontGlobal);
		txtTnC.setMargin(0, 0, 0, 10);
		txtTnC.setChangeListener(this);
		
		HorizontalFieldManager tncLayout = new HorizontalFieldManager(USE_ALL_WIDTH);
		tncLayout.setMargin(0, 0, 10, 0);
		tncLayout.add(cbTnC);
		tncLayout.add(txtTnC);
		
		//for button
		btnRegis = new ButtonFieldCustom("DAFTAR", Constant.deviceWidth/3, FIELD_HCENTER);
		btnRegis.setEditable(true);
		btnRegis.setMargin(10, 0, 10, 0);
		btnRegis.setChangeListener(this);
		
		VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 15, 15, 15);
		scr.add(mMSISDN.getField());
		scr.add(mEmail.getField());
		scr.add(mFirstName.getField());
		scr.add(mLastName.getField());
		scr.add(genderLayout);
		scr.add(mDOB.getField());
		scr.add(mAddr.getField());
		scr.add(mIDType.getField());
		scr.add(mIDNumber.getField());
		scr.add(mMother.getField());
		scr.add(mPIN.getField());
		scr.add(mPINConfirm.getField());
		scr.add(tncLayout);
		scr.add(btnRegis);
		
		//test ActiveRichTextField
		/*int offsets[] = new int[]{0, 19, 37, 54};
		Font[] fonts = new Font[]{Assets.fontGlobal, Assets.fontGlobalUnderlined};
		int bg[] = new int[]{Color.WHITE, Color.WHITE, Color.WHITE};
		int fg[] = new int[]{Constant.colorFontGeneral, Constant.colorFontGeneral, Constant.colorFontGeneral};
		byte attributes[] = new byte[]{0, 1, 0};
		
		ActiveRichTextField x = new ActiveRichTextField("Saya setuju dengan Syarat & Ketentuan layanan Dompetku", offsets, attributes, fonts, fg, bg, HIGHLIGHT_FOCUS|ActiveRichTextField.USE_TEXT_WIDTH);
		scr.add(x);*/
		
		add(scr);
		
		String username = Constant.mPersistData.getUsername();
        if(username!=""){
            mMSISDN.setText(username);
            mMSISDN.setEditable(false);
        }
	}
	
	public void setDate(Calendar c){
		String dd = Integer.toString(c.get(Calendar.DAY_OF_MONTH));
        String mm = Integer.toString(c.get(Calendar.MONTH)+1);
        String yy = Integer.toString(c.get(Calendar.YEAR));
        
        if(dd.length()==1){
        	dd = "0"+dd;
        }
        if(mm.length()==1){
        	mm = "0"+mm;
        }
        
        mDOB.setText(BasicUtils.CalendarToStandardDate(dd, mm, yy));
	}
	
	public void fieldChanged(Field field, int context) {
		if(field == btnRegis){
			String txtEmail = mEmail.getText();
            String txtMSISDN = mMSISDN.getText();
            String txtFirstName = mFirstName.getText();
            String txtLastName = mLastName.getText();
            String txtAddr = mAddr.getText();
            String txtDOB = BasicUtils.StandardToServerDate(mDOB.getText());
            String txtIDNumber = mIDNumber.getText();
            String txtMother = mMother.getText();
            String txtPIN = mPIN.getText();
            String txtPINConfirm = mPINConfirm.getText();
            String txtGender = "";
            int IDTypePos = Integer.parseInt(mIDType.getSelectedIndexValue());
            String txtIDType = Integer.toString(IDTypePos);
            
            resetFieldState();
	        if(txtMSISDN.length()==0){
	        	mMSISDN.setIsError(true);
                Dialog.alert("Mohon isi No HP");
	        }else if(txtFirstName.length()==0){
	        	mFirstName.setIsError(true);
            	Dialog.alert("Mohon isi Nama Depan");
	        }else if(mGender.getSelectedIndex() == -1){
	        	Dialog.alert("Mohon isi Jenis Kelamin");
			}else if(mDOB.getText().equals("Klik untuk membuka kalender")){
				mDOB.setIsError(true);
            	Dialog.alert("Mohon isi Tanggal Lahir");
			}else if(txtMother.length()==0){
	            mMother.setIsError(true);
	        	Dialog.alert("Mohon isi Nama Ibu Kandung");
			}else if(txtPIN.length() == 0){
				mPIN.setIsError(true);
	        	Dialog.alert("Mohon isi PIN");
			}else if(txtPIN.length() != 6){
				mPIN.setIsError(true);
	        	Dialog.alert("PIN harus terdiri dari 6 digit angka.");
			}else if(txtPINConfirm.length()==0){
				mPINConfirm.setIsError(true);
	        	Dialog.alert("Mohon isi Konfirmasi PIN");
			}else if(!txtPIN.equals(txtPINConfirm)){
				mPINConfirm.setIsError(true);
	        	Dialog.alert("PIN dan Konfirmasi PIN harus sama.");
			}else if(!cbTnC.getChecked()){
	        	Dialog.alert("Centang tanda Setuju dengan Syarat & Ketentuan");
			}else{
				if(mGender.getSelectedIndex() == mGenderMale.getIndex())
		            txtGender = "1";
				else if(mGender.getSelectedIndex() == mGenderFemale.getIndex())
		            txtGender = "2";
				
				new DoRegisterTask(txtEmail, txtPIN, txtMSISDN, txtFirstName, txtLastName, txtDOB,
						txtGender, txtMother, txtAddr, txtIDType, txtIDNumber);
			}
		}else if(field == txtTnC){
			mediator.app.pushScreen(new About(mediator, About.ABOUT_TERM_CONDITION));
		}
	}
	
	public class DoRegisterTask implements Runnable{
		String email, pin, msisdn, firstName, lastName, dob, gender, motherName, addr, idType, idNo;
		
		public DoRegisterTask(String email, String pin, String msisdn, String firstName, String lastName,
				String dob, String gender, String motherName, String addr, String idType, String idNo) {
			this.email = email;
			this.pin = pin;
			this.msisdn = msisdn;
			this.firstName = firstName;
			this.lastName = lastName;
			this.dob = dob;
			this.gender = gender;
			this.motherName = motherName;
			this.addr = addr;
			this.idType = idType;
			this.idNo = idNo;
			PleaseWaitPopupScreen.showScreenAndWait(this, "Mendaftarkan...");
		}
		
		public void run() {
			final String entity = Connection.doRegister(email, pin, msisdn, firstName, lastName, dob,
					gender, motherName, addr, idType, idNo);
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                    if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject obj = new JSONObject(entity);
                            int status = obj.getInt("status");
                        	if(status == 0){
                        		BasicUtils.saveUsernamePref(mMSISDN.getText());
                                BasicUtils.saveRegisterPending(Boolean.FALSE.toString());
                                
                        		String dMsg = "Registrasi berhasil. Silakan login kembali menggunakan No HP dan PIN yang telah Anda daftarkan.";
                        		Dialog d = new Dialog(Dialog.D_OK, dMsg, 0, Bitmap.getPredefinedBitmap(Bitmap.INFORMATION), 0);
                        		d.setEscapeEnabled(false);
                        		d.setDialogClosedListener(new DialogClosedListener() {
									public void dialogClosed(Dialog dialog, int choice) {
										if(choice == Dialog.OK){
											mediator.app.pushScreen(new Login(mediator));
											close();
										}
									}
								});
                        		d.show();
                        	}else{
                        		Dialog.alert("Registrasi gagal: "+obj.getString("msg"));
                        	}
                        }catch(Exception e){
                        	System.out.println("<< Get Promo error: "+e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }
			});
		}
	}
	
	public void resetFieldState(){
        mEmail.setIsError(false);
        mMSISDN.setIsError(false);
        mFirstName.setIsError(false);
        mLastName.setIsError(false);
        mAddr.setIsError(false);
        mDOB.setIsError(false);
        mIDNumber.setIsError(false);
        mMother.setIsError(false);
        mPIN.setIsError(false);
        mPINConfirm.setIsError(false);
    }
	
	public boolean onClose(){
    	this.close();
    	return true;
    }
}
