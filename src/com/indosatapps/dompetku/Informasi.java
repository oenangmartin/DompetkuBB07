package com.indosatapps.dompetku;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.ListItemField;

public class Informasi extends MainScreen implements FieldChangeListener{
	Mediator mediator;
	ListItemField mHistoryTrx, mAbout, mTnC, mContactUs, mDaftarMerchant, mSetor, mBeli, mKirim, mTarik;
	
	public Informasi(Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		HeaderField header = new HeaderField("Informasi", 0);
		add(header);
		
		mHistoryTrx = new ListItemField("Histori Transaksi", Constant.deviceWidth-30, 0);
		mAbout = new ListItemField("Tentang Dompetku", Constant.deviceWidth-30, 0);
		mTnC = new ListItemField("Syarat & Ketentuan", Constant.deviceWidth-30, 0);
		mContactUs = new ListItemField("Hubungi Kami", Constant.deviceWidth-30, 0);
		mDaftarMerchant = new ListItemField("Daftar Merchant", Constant.deviceWidth-30, 0);
		mSetor = new ListItemField("Setor Tunai", Constant.deviceWidth-30, 0);
		mBeli = new ListItemField("Beli / Bayar", Constant.deviceWidth-30, 0);
		mKirim = new ListItemField("Kirim Uang", Constant.deviceWidth-30, 0);
		mTarik = new ListItemField("Tarik Tunai", Constant.deviceWidth-30, 0);
		
		mHistoryTrx.setChangeListener(this);
		mAbout.setChangeListener(this);
		mTnC.setChangeListener(this);
		mContactUs.setChangeListener(this);
		mDaftarMerchant.setChangeListener(this);
		mSetor.setChangeListener(this);
		mBeli.setChangeListener(this);
		mKirim.setChangeListener(this);
		mTarik.setChangeListener(this);
		
		VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 15, 15, 15);
		scr.add(mHistoryTrx);
		scr.add(mAbout);
		scr.add(mTnC);
		scr.add(mContactUs);
		scr.add(mDaftarMerchant);
		scr.add(mSetor);
		scr.add(mBeli);
		scr.add(mKirim);
		scr.add(mTarik);
		
		add(scr);
	}

	public void fieldChanged(Field field, int context) {
		if(field == mHistoryTrx){
			mediator.app.pushScreen(new HistoriTransaksi(mediator));
		}else if(field == mAbout){
			mediator.app.pushScreen(new About(mediator, About.ABOUT));
		}else if(field == mTnC){
			mediator.app.pushScreen(new About(mediator, About.ABOUT_TERM_CONDITION));
		}else if(field == mContactUs){
			mediator.app.pushScreen(new About(mediator, About.ABOUT_CONTACT_US));
		}else if(field == mDaftarMerchant){
			mediator.app.pushScreen(new MerchantList(mediator));
		}else if(field == mSetor){
			mediator.app.pushScreen(new About(mediator, About.ABOUT_SETOR_TUNAI));
		}else if(field == mBeli){
			mediator.app.pushScreen(new About(mediator, About.ABOUT_BELI_BAYAR));
		}else if(field == mKirim){
			mediator.app.pushScreen(new About(mediator, About.ABOUT_KIRIM_UANG));
		}else if(field == mTarik){
			mediator.app.pushScreen(new About(mediator, About.ABOUT_TARIK_TUNAI));
		}
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }
}
