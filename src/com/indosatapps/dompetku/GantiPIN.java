package com.indosatapps.dompetku;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.DialogClosedListener;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.text.TextFilter;

import org.json.me.JSONObject;

import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.ButtonFieldCustom;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.PasswordEditFieldCustom;
import com.indosatapps.dompetku.field.PleaseWaitPopupScreen;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.Connection;
import com.indosatapps.dompetku.utils.PageResultListener;

public class GantiPIN extends MainScreen implements FieldChangeListener{
	Mediator mediator;
	PasswordEditFieldCustom editPINLama, editPINBaru, editPINBaruConfirm;
	ButtonFieldCustom btnSubmit;
	PageResultListener listener;
	
	public GantiPIN(Mediator mediator, PageResultListener listener) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		this.listener = listener;
		HeaderField header = new HeaderField("Ganti PIN", 0);
		add(header);
		
		editPINLama = new PasswordEditFieldCustom("PIN Lama", "", Constant.deviceWidth-30, 6, FIELD_HCENTER);
		editPINBaru = new PasswordEditFieldCustom("PIN Baru", "", Constant.deviceWidth-30, 6, FIELD_HCENTER);
		editPINBaruConfirm = new PasswordEditFieldCustom("Ulang PIN Baru", "", Constant.deviceWidth-30, 6, FIELD_HCENTER);
		editPINLama.setFilter(TextFilter.get(TextFilter.NUMERIC));
		editPINBaru.setFilter(TextFilter.get(TextFilter.NUMERIC));
		editPINBaruConfirm.setFilter(TextFilter.get(TextFilter.NUMERIC));
		
		btnSubmit = new ButtonFieldCustom("OK", Constant.deviceWidth/3, FIELD_HCENTER);
		btnSubmit.setEditable(true);
		btnSubmit.setMargin(10, 0, 10, 0);
		btnSubmit.setChangeListener(this);
		
		VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL);
		scr.setMargin(15, 15, 15, 15);
		scr.add(editPINLama.getField());
		scr.add(editPINBaru.getField());
		scr.add(editPINBaruConfirm.getField());
		scr.add(btnSubmit);
		
		add(scr);
	}
	
	public void fieldChanged(Field field, int arg1) {
		if(field == btnSubmit){
			String mPINLama = editPINLama.getText();
            String mPINBaru = editPINBaru.getText();
            String mPINBaruConfirm = editPINBaruConfirm.getText();
            
            resetFieldState();
            if(mPINLama.length() == 0){
                editPINLama.setIsError(true);
                Dialog.alert("Mohon isi PIN Lama");
            }else if(mPINBaru.length() == 0){
                editPINBaru.setIsError(true);
                Dialog.alert("Mohon isi PIN Baru");
            }else if(mPINBaru.equals(mPINLama)){
                editPINBaru.setIsError(true);
                Dialog.alert("PIN Baru tidak boleh sama dengan PIN Lama Anda.");
            }else if(mPINBaruConfirm.length() == 0){
                editPINBaruConfirm.setIsError(true);
                Dialog.alert("Mohon ulang PIN Baru");
            }else if(!mPINBaru.equals(mPINBaruConfirm)){
                editPINBaruConfirm.setIsError(true);
                Dialog.alert("PIN dan Konfirmasi PIN harus sama.");
            }else{
            	new DoChangePINTask(mPINLama, mPINBaru);
            }
		}
	}
	
	public void resetFieldState(){
        editPINLama.setIsError(false);
        editPINBaru.setIsError(false);
        editPINBaruConfirm.setIsError(false);
    }
	
	public class DoChangePINTask implements Runnable{
		String username, oldPIN, newPIN;
		
		public DoChangePINTask(String oldPIN, String newPIN) {
			username = Constant.mPersistData.getUsername();
			this.oldPIN = oldPIN;
			this.newPIN = newPIN;
			PleaseWaitPopupScreen.showScreenAndWait(this, "Mohon tunggu...");
		}
		
		public void run() {
			final String entity = Connection.doChangePIN(username, oldPIN, newPIN);
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject objs = new JSONObject(entity);
                            int status = objs.getInt("status");
                            if(status == 0){
                            	BasicUtils.showMessageDialog("PIN berhasil diganti!", new DialogClosedListener() {
									public void dialogClosed(Dialog dialog, int choice) {
										if(choice == Dialog.OK){
											mediator.app.pushScreen(new Login(mediator));
											
											Constant.mPersistData.setLastSession("");
											BasicUtils.savePersistData();
											
											listener.onPageResult(Constant.REQUEST_CODE_GANTI_PIN, Constant.RESULT_OK, null);
		                                	close();
										}
									}
								});
                            }else if(status == 1011){
                            	Dialog.alert("PIN atau agen tidak ditemukan atau salah.");
                            }else if(status == 1012){
                                Dialog.alert("PIN error, percobaan melebihi batas.");
                            }else if(status == 1013){
                                Dialog.alert("PIN sudah kadaluarsa.");
                            }else if(status == 1014){
                                Dialog.alert("PIN Lama Anda salah.");
                            }else if(status == 1015){
                                Dialog.alert("PIN Baru tidak boleh sama dengan PIN Lama Anda.");
                            }else if(status == 1016){
                                Dialog.alert("PIN sudah dipakai sebelumnya.");
                            }else{
                            	Dialog.alert("Penggantian PIN gagal.");
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Dialog.alert(Constant.txt_error_json);
                        }
                    }else{
                    	Dialog.alert(Constant.txt_error_no_connection);
                    }
                }
			});
		}
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }
}
