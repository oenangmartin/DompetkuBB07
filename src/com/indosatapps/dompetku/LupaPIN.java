package com.indosatapps.dompetku;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.LabelFieldColor;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class LupaPIN extends MainScreen {
	
	public LupaPIN(Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		HeaderField header = new HeaderField("Lupa PIN", 0);
		add(header);
		
		String txt = "Silakan hubungi 111 dari nomor Matrix dan 100 dari nomor Mentari & IM3 untuk permintaan reset PIN. " +
				"PIN baru akan dikirimkan melalui SMS setelah kami verifikasi";
		LabelFieldColor field = new LabelFieldColor(txt, Constant.colorFontGeneral, FIELD_VCENTER|DrawStyle.HCENTER);
		field.setFont(Assets.fontGlobal);
		field.setMargin(15, 10, 15, 10);
		add(field);
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }
}
