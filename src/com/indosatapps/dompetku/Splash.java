package com.indosatapps.dompetku;

import net.rim.device.api.system.Characters;
import net.rim.device.api.system.GIFEncodedImage;
import net.rim.device.api.system.KeyListener;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TouchGesture;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.AnimatedGIFField;
import com.indosatapps.dompetku.utils.BasicUtils;

public class Splash extends MainScreen implements Runnable{
	Mediator mediator;
	int counter=0;
	Thread t;
	
	public Splash(Mediator mediator) {
		super(USE_ALL_HEIGHT|NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createBitmapBackground(Assets.splash));
		
		this.mediator = mediator;
		
		GIFEncodedImage ourAnimation = (GIFEncodedImage) GIFEncodedImage.getEncodedImageResource("loading.gif");
        AnimatedGIFField _ourAnimation = new AnimatedGIFField(ourAnimation, Field.FIELD_VCENTER|Field.FIELD_HCENTER);
        int topMargin = (Constant.deviceHeight - _ourAnimation.getPreferredHeight()) / 2;
        _ourAnimation.setMargin(topMargin, 0, 0, 0);
        add(_ourAnimation);
		
		SplashScreenListener listener = new SplashScreenListener();
        addKeyListener(listener);
        
		Thread t = new Thread(this);
		t.start();
	}
		
	public void run() {
		while(counter<2){
			counter++;
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				System.out.println( e.getMessage() );
			    e.printStackTrace();
			}
		}
		
		mediator.loadRes();
		BasicUtils.loadFavoriteData();
		BasicUtils.loadInboxData();
		
		mediator.app.invokeLater(new Runnable() {
			public void run() {
				//mediator.app.pushScreen(new OTP("0816775009", mediator, null));
                
				/*if(BasicUtils.isFirstTimeInstall()){
					mediator.app.pushScreen(new VerifikasiNomor(mediator));
	            }else{
	                if(BasicUtils.isRegisterPending()){
	                	mediator.app.pushScreen(new Register(mediator));
	                }else{*/
						if(BasicUtils.isSessionActive()){
	                    	mediator.app.pushScreen(new Home(mediator));
	                    }else{
	                    	mediator.app.pushScreen(new Login(mediator));
	                    }
	                /*}
	            }*/
				close();
			}
		});
	}
	
	protected boolean navigationClick(int status, int time) {
		return false;
	}
	protected boolean navigationUnclick(int status, int time) {
	    return false;
	}
	protected boolean navigationMovement(int dx, int dy, int status, int time) {
	    return false;
	}
	
	public static class SplashScreenListener implements KeyListener {
		public SplashScreenListener() {}
		
	     public boolean keyChar(char key, int status, int time) {
	        //intercept the ESC and MENU key - exit the splash screen
	        boolean retval = false;
	        switch (key) {
	           case Characters.CONTROL_MENU:
	           case Characters.ESCAPE:
	           retval = true;
	           break;
	        }
	        return retval;
	     }
	     public boolean keyDown(int keycode, int time) {
	        return false;
	     }
	     public boolean keyRepeat(int keycode, int time) {
	        return false;
	     }
	     public boolean keyStatus(int keycode, int time) {
	        return false;
	     }
	     public boolean keyUp(int keycode, int time) {
	        return false;
	     }
	 }
	
	protected boolean touchEvent(TouchEvent message)
	{
		if(message.getEvent()==TouchEvent.GESTURE)
		{
			if(message.getGesture().getEvent()==TouchGesture.TAP)
			{
				return false;
			}
		}
		return false;
	}
}