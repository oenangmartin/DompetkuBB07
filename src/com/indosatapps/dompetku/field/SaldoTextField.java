package com.indosatapps.dompetku.field;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;

public class SaldoTextField extends Field{
	private String saldo;
	private Font font;
	private int w, h;
	
	public SaldoTextField(String saldo, long style) {
		super(style);
		this.saldo = saldo;
		font = Assets.fontSaldo;
		w = Constant.deviceWidth/2;
		h = font.getHeight()+10;
	}
	
	protected void layout(int width, int height) {
		setExtent(w, h);
	}

	protected void paint(Graphics g) {
		g.setColor(Constant.colorPrimary);
		g.drawRoundRect(0, 0, w, w, 20, 20);
		g.fillRoundRect(0, 0, w, h, 20, 20);
		
		g.setFont(font);
		g.setColor(Color.WHITE);
		g.drawText(saldo, (w-font.getAdvance(saldo)-10), (h-font.getHeight())/2);
	}
	
	public void setSaldo(String saldo){
		this.saldo = saldo;
		invalidate();
	}
}
