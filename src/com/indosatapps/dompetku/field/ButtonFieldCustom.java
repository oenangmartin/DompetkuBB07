package com.indosatapps.dompetku.field;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TouchGesture;

import com.indosatapps.dompetku.data.Assets;

public class ButtonFieldCustom extends Field{
	String label;
	int w, h;
	Bitmap[] btn, btnFocus, btnDis;
	int fontColor;
	boolean isEnabled = true;
	
	public ButtonFieldCustom(String label, int width, long style) {
		super(style | FOCUSABLE | FIELD_HCENTER );
		this.label = label;
		this.btn = Assets.btnDisable;
		this.btnFocus = Assets.btn;
		this.btnDis = Assets.btnDisable;
		w = width;
		h = btn[1].getHeight();
		fontColor = Color.WHITE;
	}
	
	protected void layout(int width, int height) {
		setExtent(w, h);
	}
	
	protected void paint(Graphics g) {
		Bitmap[] btn = isFocus()?btnFocus:this.btn;
		if(!isEnabled) btn = btnDis;
	
		//draw the left side
		g.drawBitmap(0, 0, btn[0].getWidth(), btn[0].getHeight(), btn[0], 0, 0);
		
		//draw the middle side
		int ulang = w-(btn[0].getWidth()+btn[2].getWidth());
		for(int i=0 ; i<ulang ; i+=btn[1].getWidth()){
			g.drawBitmap(btn[0].getWidth()+i, 0, btn[1].getWidth(), btn[1].getHeight(), btn[1], 0, 0);
		}
		
		//draw the right side
		g.drawBitmap(btn[0].getWidth()+ulang, 0, btn[2].getWidth(), btn[2].getHeight(), btn[2], 0, 0);
		
		g.setFont(Assets.fontGlobalBold);
		g.setColor(fontColor);
		g.drawText(label, (w-Assets.fontGlobalBold.getAdvance(label))/2, (h-Assets.fontGlobalBold.getHeight())/2);
	}
	
	public int getW(){
		return w;
	}
	
	public void setEditable(boolean editable) {
		isEnabled = editable;
		super.setEditable(editable);
	}
	
	public void setText(String text){
		label = text;
		invalidate();
	}
	
	public String getText(){
		return label;
	}
	
	public boolean isFocusable() {
	   return isEditable() && super.isFocusable();
	}
	
	public void invalidate(){
		super.invalidate();
	}
	
	protected void drawFocus(Graphics g, boolean bo){
	}
	
	protected void onFocus(int direction){
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus(){
		super.onUnfocus();
		invalidate();
	}
	
	protected boolean keyChar(char character, int status, int time)
	{
		if(character==Characters.ENTER){
			if(isEnabled) fieldChangeNotify(0);
			return true;
		}
		return super.keyChar(character, status, time);
	}
	
	protected boolean navigationClick(int status, int time)
	{
		if(isEnabled) fieldChangeNotify(0);
		return true;
	}
	
	protected boolean touchEvent(TouchEvent message)
	{
		if(message.getEvent()==TouchEvent.GESTURE)
		{
			if(message.getGesture().getEvent()==TouchGesture.TAP)
			{
				if(isEnabled) fieldChangeNotify(0);
				return true;
			}
		}
		return super.touchEvent(message);
	}
}
