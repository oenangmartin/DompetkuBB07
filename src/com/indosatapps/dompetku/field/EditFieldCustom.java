package com.indosatapps.dompetku.field;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.FocusChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.text.TextFilter;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.thirdparty.device.api.ui.container.JustifiedHorizontalFieldManager;

public class EditFieldCustom {
	EditField editField;
	VerticalFieldManager finalField;
	HorizontalFieldManager finalEditField;
	int width, height;
	Bitmap[] bg;
	ButtonBitmapField btnFav, btnSearch;
	boolean isError = false, isEnabled = true, isFavorite = false, isSearch = false;
	String label;
	
	public EditFieldCustom(String label, String initialValue, final int w, int maxNumChar, boolean isFavorite, boolean isSearch, long style) {
		this(label, null, initialValue, w, maxNumChar, isFavorite, isSearch, style);
	}
	
	public EditFieldCustom(String label, EditField eField, String initialValue, final int w, int maxNumChar, boolean isFavorite, boolean isSearch
			, long style) {
		bg = Assets.bgEditText;
		width = w;
		height = bg[1].getHeight();
		this.label = label;
		this.isFavorite = isFavorite;
		this.isSearch = isSearch;
		
		LabelFieldColor labelField = new LabelFieldColor(label, Constant.colorFontGeneral, 0);
		labelField.setMargin(0, 10, 0, Assets.bgEditText[0].getWidth());
		labelField.setFont(Assets.fontGlobal);
		
		if(eField == null){
			editField = new EditField("", initialValue, maxNumChar, style|EditField.NO_NEWLINE){
				protected void paint(Graphics g){
					if(isEnabled)g.setColor(Constant.colorFontFormInput);
					else g.setColor(Color.WHITE);
	        	    super.paint(g);
	        	}
			};
			editField.setFont(Assets.fontGlobal);
		}else {
			editField = eField;
		}
		
		HorizontalFieldManager hfm = new HorizontalFieldManager(HorizontalFieldManager.HORIZONTAL_SCROLL);
		hfm.setMargin((Assets.bgEditText[1].getHeight()-Assets.fontGlobal.getHeight())/2,
						Assets.bgEditText[0].getWidth(),
						(Assets.bgEditText[1].getHeight()-Assets.fontGlobal.getHeight())/2,
						Assets.bgEditText[2].getWidth());
		hfm.add(editField);
		
		btnFav = new ButtonBitmapField(Assets.icFavorite, Assets.icFavoriteFocus, Field.FIELD_VCENTER);
		btnSearch = new ButtonBitmapField(Assets.icSearch, Assets.icSearchFocus, Field.FIELD_VCENTER);
		btnFav.setEditable(true);
		btnSearch.setEditable(true);
		btnFav.setMargin(0, 0, 0, 5);
		btnSearch.setMargin(0, 0, 0, 5);

		if(isFavorite==true && isSearch==true){
			width = w - btnFav.getW() - btnSearch.getW() - 10;
		}else if(isFavorite==true && isSearch==false){
			width = w - btnFav.getW() - 5;
		}else if(isFavorite==false && isSearch==true){
			width = w - btnSearch.getW() - 5;
		}
		
		finalEditField = new HorizontalFieldManager(){
			protected void paint(Graphics g) {
				Bitmap[] bg;
				if(isEnabled)
					if(isError)
						bg = editField.isFocus()?Assets.bgEditTextFocus:Assets.bgEditTextError;
					else
						bg = editField.isFocus()?Assets.bgEditTextFocus:Assets.bgEditText;
				else
					bg = Assets.bgEditTextDisable;	
				
				//draw the left side
				g.drawBitmap(0, 0, bg[0].getWidth(), bg[0].getHeight(), bg[0], 0, 0);
				
				//draw the middle side
				int ulang = width-(bg[0].getWidth()+bg[2].getWidth());
				for(int i=0 ; i<ulang ; i+=bg[1].getWidth()){
					g.drawBitmap(bg[0].getWidth()+i, 0, bg[1].getWidth(), bg[1].getHeight(), bg[1], 0, 0);
				}
				
				//draw the right side
				g.drawBitmap(bg[0].getWidth()+ulang, 0, bg[2].getWidth(), bg[2].getHeight(), bg[2], 0, 0);
				
				super.paint(g);
			}
		};
		//finalField.setPadding(0, bg[2].getWidth(), 0, bg[1].getWidth());
		finalEditField.add(hfm);
		
		editField.setFocusListener(new FocusChangeListener() {
			public void focusChanged(Field field, int eventType) {
				finalEditField.invalidate();
			}
		});
		
		HorizontalFieldManager btnLayout = new HorizontalFieldManager(Field.FIELD_VCENTER);
		if(isSearch) btnLayout.add(btnSearch);
		if(isFavorite) btnLayout.add(btnFav);
			
		JustifiedHorizontalFieldManager jhfm = new JustifiedHorizontalFieldManager(null, finalEditField, btnLayout, Field.USE_ALL_WIDTH);
		
		finalField = new VerticalFieldManager();
		if(label!="")finalField.add(labelField);
		finalField.add(jhfm);
		finalField.setMargin(0, 0, 5, 0);
	}
	
	public Field getField(){
		return finalField;
	}
	
	public String getText(){
		return editField.getText();
	}
	
	public void setText(String text){
		editField.setText(text);
	}
	
	public void setFilter(TextFilter filter){
		editField.setFilter(filter);
	}
	
	public void setIsError(boolean isError){
		this.isError = isError;
		finalEditField.invalidate();
	}
	
	public void setEditable(boolean editable) {
		isEnabled = editable;
		editField.setEditable(editable);
		finalEditField.invalidate();
	}
	
	public void setBtnFavoriteListener(FieldChangeListener listener){
		btnFav.setChangeListener(listener);
	}
	
	public void setBtnSearchListener(FieldChangeListener listener){
		btnSearch.setChangeListener(listener);
	}
	
	public String getLabel(){
		return label;
	}
	
	public EditField getEditField(){
		return editField;
	}
}
