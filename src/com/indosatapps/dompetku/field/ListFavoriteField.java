package com.indosatapps.dompetku.field;

import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

import com.indosatapps.dompetku.bean.FavoriteData;
import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;

public class ListFavoriteField extends CustomField{
	FavoriteData favoriteData;
	int w, h;
	Font font1, font2, font3;
	String favName, favText, favBiller;
	
	public ListFavoriteField(FavoriteData favoriteData, long style) {
		super(style | FIELD_HCENTER );
		this.favoriteData = favoriteData;
		font1 = Assets.fontGlobalBold;
		font2 = Assets.fontGlobal;
		font3 = Assets.fontMini;
		w = Constant.deviceWidth-30;
		h = font1.getHeight()+font2.getHeight()+font3.getHeight()+30;
		
		favName = favoriteData.getFavoriteID();
        favText = favoriteData.getFavoriteText();
        favBiller = favoriteData.getMenu()+" - "+favoriteData.getCategory()+" - "+favoriteData.getMerchant();
	}
	
	protected void layout(int width, int height) {
		setExtent(w, h);
	}
	
	protected void paint(Graphics g) {
		if(isFocus()){
			g.setColor(Constant.colorPrimary);
			g.drawRoundRect(0, 0, w, h, 20, 20);
			g.fillRoundRect(0, 0, w, h, 20, 20);
		}
		
		g.setColor(isFocus()?Constant.colorPageBackground:Constant.colorFontGeneral);
		g.setFont(font1);
		g.drawText(favName, 10, 10);
		g.setFont(font2);
		g.drawText(favText, 10, 15+font1.getHeight());
		g.setFont(font3);
		g.drawText(favBiller, 10, 20+font1.getHeight()+font2.getHeight());
	}
}
