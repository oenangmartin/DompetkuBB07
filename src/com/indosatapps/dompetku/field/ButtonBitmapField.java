package com.indosatapps.dompetku.field;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TouchGesture;

public class ButtonBitmapField extends Field{
	int w, h;
	Bitmap btn, btnFocus, btnDisable, btnTemp;
	boolean isEnabled = true;
	
	public ButtonBitmapField(Bitmap btn, Bitmap btnFocus, long style) {
		this(btn, btnFocus, null, style);
	}
	
	public ButtonBitmapField(Bitmap btn, Bitmap btnFocus, Bitmap btnDisable, long style) {
		super(style | FOCUSABLE);
		this.btn = btn;
		this.btnFocus = btnFocus;
		this.btnDisable = btnDisable;
		w = btn.getWidth();
		h = btn.getHeight();
	}

	protected void layout(int width, int height) {
		setExtent(w, h);
	}

	protected void paint(Graphics g) {
		btnTemp = isFocus()?btnFocus:this.btn;
		if(!isEnabled){
			btnTemp = btnDisable;
		}
		g.drawBitmap(0, 0, w, h, btnTemp, 0, 0);
	}
	
	public int getW(){
		return w;
	}
	
	public void setBitmap(Bitmap btn, Bitmap btnFocus){
		this.btn = btn;
		this.btnFocus = btnFocus;
		invalidate();
	}
	
	public void setEditable(boolean editable) {
		isEnabled = editable;
		super.setEditable(editable);
	}
	
	public boolean isFocusable() {
	   return isEditable() && super.isFocusable();
	}
	
	public void invalidate(){
		super.invalidate();
	}
	
	protected void drawFocus(Graphics g, boolean bo){
	}
	
	protected void onFocus(int direction){
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus(){
		super.onUnfocus();
		invalidate();
	}
	
	protected boolean keyChar(char character, int status, int time)
	{
		if(character==Characters.ENTER){
			if(isEnabled) fieldChangeNotify(0);
			return true;
		}
		return super.keyChar(character, status, time);
	}
	
	protected boolean navigationClick(int status, int time)
	{
		if(isEnabled) fieldChangeNotify(0);
		return true;
	}
	
	protected boolean touchEvent(TouchEvent message)
	{
		if(message.getEvent()==TouchEvent.GESTURE)
		{
			if(message.getGesture().getEvent()==TouchGesture.TAP)
			{
				if(isEnabled) fieldChangeNotify(0);
				return true;
			}
		}
		return super.touchEvent(message);
	}
}
