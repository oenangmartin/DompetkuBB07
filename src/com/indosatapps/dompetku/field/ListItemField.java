package com.indosatapps.dompetku.field;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.DataProcessor;
import com.indosatapps.dompetku.utils.DataRetriever;

public class ListItemField extends CustomField implements DataProcessor{
	String label, imgURL;
	int w, h;
	double ratio;
	Font font;
	Bitmap logo;
	String fileName;
	
	public ListItemField(String label, int width, long style) {
		this(label, null, null, width, style);
	}
	
	public ListItemField(String label, Bitmap logo, int width, long style) {
		this(label, logo, null, width, style);
	}
	
	public ListItemField(String label, Bitmap logo, String imgURL, int width, long style) {
		super(style | FIELD_HCENTER );
		this.label = label;
		this.logo = logo;
		this.imgURL = imgURL;
		font = Assets.fontGlobalBold;
		w = width;
		h = font.getHeight()+20;
		
		if(this.logo != null){
			ratio = ((double)this.logo.getHeight())/(h-10);
			if(this.logo.getHeight() > (h-10)){
				int hTemp = (int) (this.logo.getWidth()/ratio);
				this.logo = BasicUtils.ResizeTransparentBitmap(logo, hTemp, h-10, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_TO_FIT);
			}
		}
		
		if(imgURL!=null){
			fileName = imgURL.substring(imgURL.lastIndexOf('/')+1, imgURL.lastIndexOf('.'));
			if(BasicUtils.checkFileExistance(fileName)){
				this.logo = BasicUtils.getBitmapFromFile(fileName);
			}else{
				DataRetriever ret = new DataRetriever();
				ret.addRequest(this);
			}
		}
	}
	
	protected void layout(int width, int height) {
		setExtent(w, h);
	}
	
	protected void paint(Graphics g) {
		if(isFocus()){
			g.setColor(Constant.colorPrimary);
			g.drawRoundRect(0, 0, w, h, 20, 20);
			g.fillRoundRect(0, 0, w, h, 20, 20);
		}
		
		if(logo!=null){
			g.drawBitmap(10, (h-logo.getHeight())/2, logo.getWidth(), logo.getHeight(), logo, 0, 0);
		}else{
			g.setFont(font);
			g.setColor(isFocus()?Constant.colorPageBackground:Constant.colorFontGeneral);
			g.drawText(label, 10, (h-font.getHeight())/2, DrawStyle.ELLIPSIS, w-20);
		}
	}
	
	public int getW(){
		return w;
	}
	
	public void setText(String text){
		label = text;
		invalidate();
	}
	
	public void setFont(Font font){
		this.font = font;
		invalidate();
	}
	
	public String getText(){
		return label;
	}

	public String getUrl() {
		return imgURL;
	}

	public void processData(byte[] data) {
		if(data==null){
			logo = Assets.noImage;
		}else{
			logo = Bitmap.createBitmapFromBytes(data, 0, data.length, 1);
			if(logo.getHeight() > (h-10)){
				ratio = ((double)logo.getHeight())/(h-10);
				int hTemp = (int) (logo.getWidth()/ratio);
				logo = BasicUtils.ResizeTransparentBitmap(logo, hTemp, h-10, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_TO_FIT);
				//logo = BasicUtils.ResizeTransparentBitmap(logo, w, h-10, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_TO_FIT);
			}
			BasicUtils.saveBitmapToFile(fileName, logo);
		}
		invalidate();
	}
}
