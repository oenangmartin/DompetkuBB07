package com.indosatapps.dompetku.field;

import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

import com.indosatapps.dompetku.bean.InboxData;
import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.utils.BasicUtils;


public class ListInboxField extends CustomField{
	InboxData inboxData;
	int w, h;
	Font font1, font2;
	String date, action;
	
	public ListInboxField(InboxData inboxData, int width, long style) {
		super(style | FIELD_HCENTER );
		this.inboxData = inboxData;
		font1 = Assets.fontMini;
		font2 = Assets.fontGlobalBold;
		w = width;
		h = font1.getHeight()+font2.getHeight()+25;
		
		date = BasicUtils.formatDateFromMilliseconds(inboxData.getDate(), "dd/MM/yyyy HH:mm");
		action = inboxData.getMenu()+" "+inboxData.getCategory();
	}
	
	protected void layout(int width, int height) {
		setExtent(w, h);
	}
	
	protected void paint(Graphics g) {
		if(isFocus()){
			g.setColor(Constant.colorPrimary);
			g.drawRoundRect(0, 0, w, h, 20, 20);
			g.fillRoundRect(0, 0, w, h, 20, 20);
		}
		
		g.setColor(isFocus()?Constant.colorPageBackground:Constant.colorFontGeneral);
		g.setFont(font1);
		g.drawText(date, 10, 10);
		g.setFont(font2);
		g.drawText(action, 10, 15+font1.getHeight());
	}
}
