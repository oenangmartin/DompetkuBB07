package com.indosatapps.dompetku.field;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.DataProcessor;
import com.indosatapps.dompetku.utils.DataRetriever;

public class ListMerchantField extends CustomField implements DataProcessor{
	String label, imgURL, fileName;
	int w, h;
	Font font;
	Bitmap logo = null;
	
	public ListMerchantField(String label, String imgURL, long style) {
		super(style | FIELD_HCENTER );
		this.label = label;
		this.imgURL = imgURL;
		font = Assets.fontGlobalBold;
		w = Assets.bgMerchantList.getWidth();
		h = Assets.bgMerchantList.getHeight();
		
		System.out.println("Width: "+w+" Height: "+h);
		
		if(imgURL!=null && imgURL!=""){
			fileName = imgURL.substring(imgURL.lastIndexOf('/')+1, imgURL.lastIndexOf('.'));
			if(BasicUtils.checkFileExistance(fileName)){
				this.logo = BasicUtils.getBitmapFromFile(fileName);
			}else{
				DataRetriever ret = new DataRetriever();
				ret.addRequest(this);
			}
		}
	}
	
	protected void layout(int width, int height) {
		setExtent(w, h);
	}
	
	protected void paint(Graphics g) {
		g.drawBitmap(0, 0, w, h, isFocus()?Assets.bgMerchantListFocus:Assets.bgMerchantList, 0, 0);
		
		if(logo!=null){
			g.drawBitmap((w-logo.getWidth())/2, (h-logo.getHeight())/2, logo.getWidth(), logo.getHeight(), logo, 0, 0);
		}else{
			g.setFont(font);
			g.setColor(Constant.colorPrimary);
			g.drawText(label, (w-font.getAdvance(label))/2, (h-font.getHeight())/2, DrawStyle.ELLIPSIS, w-20);
		}
	}
	
	public int getW(){
		return w;
	}
	
	public void setText(String text){
		label = text;
		invalidate();
	}
	
	public void setFont(Font font){
		this.font = font;
		invalidate();
	}
	
	public String getText(){
		return label;
	}

	public String getUrl() {
		return imgURL;
	}

	public void processData(byte[] data) {
		if(data==null){
			logo = null;
		}else{
			logo = Bitmap.createBitmapFromBytes(data, 0, data.length, 1);
			if(logo.getHeight() > h-20 || logo.getWidth() > w-20){
				logo = BasicUtils.ResizeTransparentBitmap(logo, w-20, h-20, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_TO_FIT);
			}
			BasicUtils.saveBitmapToFile(fileName, logo);
		}
		invalidate();
	}
}
