package com.indosatapps.dompetku.field;

import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TouchGesture;

public class CustomField extends Field{
	
	public CustomField(long style)
	{
		super (style | FOCUSABLE);
	}

	protected void layout(int width, int height) {
		setExtent(width, height);
	}

	protected void paint(Graphics g) {
	}
	
	public boolean isFocusable(){
		return true;
	}
	
	public void invalidate(){
		super.invalidate();
	}
	
	protected void drawFocus(Graphics g, boolean bo){
	}
	
	protected void onFocus(int direction){
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus(){
		super.onUnfocus();
		invalidate();
	}
	
	protected boolean keyChar(char character, int status, int time)
	{
		if(character==Characters.ENTER){
			fieldChangeNotify(0);
			return true;
		}
		return super.keyChar(character, status, time);
	}
	
	protected boolean navigationClick(int status, int time)
	{
		fieldChangeNotify(0);
		return true;
	}
	
	protected boolean touchEvent(TouchEvent message)
	{
		if(message.getEvent()==TouchEvent.GESTURE)
		{
			if(message.getGesture().getEvent()==TouchGesture.TAP)
			{
				fieldChangeNotify(0);
				return true;
			}
		}
		return super.touchEvent(message);
	}

}
