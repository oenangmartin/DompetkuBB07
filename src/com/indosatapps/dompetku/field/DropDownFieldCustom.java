package com.indosatapps.dompetku.field;

import java.util.Vector;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FocusChangeListener;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.VerticalFieldManager;

import com.indosatapps.dompetku.bean.DropdownItemData;
import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;

public class DropDownFieldCustom{
	ObjectChoiceFieldCustom dropdownField;
	VerticalFieldManager finalField;
	HorizontalFieldManager finalDropdownField;
	int width, height;
	Bitmap[] bg;
	Bitmap icon;
	boolean isError = false;
	Vector dropdownItemsData = new Vector();
	
	public DropDownFieldCustom(String label, final int w, Object[] choices, Object[] values, int initialIndex, long style) {
		this(label, null, w, choices, values, initialIndex, style);
	}
	
	public DropDownFieldCustom(String label, ObjectChoiceFieldCustom eField, final int w, Object[] choices, Object[] values, int initialIndex, long style) {
		bg = Assets.bgEditText;
		icon = Assets.iconDropdown;
		width = w;
		height = bg[1].getHeight();
		
		LabelFieldColor labelField = new LabelFieldColor(label, Constant.colorFontGeneral, 0);
		labelField.setMargin(0, 10, 0, Assets.bgEditText[0].getWidth());
		labelField.setFont(Assets.fontGlobal);
		
		dropdownItemsData.removeAllElements();
		for(int i=0;i<choices.length;i++){
            dropdownItemsData.addElement(new DropdownItemData(values[i].toString(), choices[i].toString()));
        }
		
		if(eField == null){
			dropdownField = new ObjectChoiceFieldCustom("", choices, initialIndex, style);
		}else {
			dropdownField = eField;
		}
		
		dropdownField.setFocusListener(new FocusChangeListener() {
			public void focusChanged(Field field, int eventType) {
				dropdownField.invalidate();
			}
		});
		
		/*HorizontalFieldManager hfm = new HorizontalFieldManager(HorizontalFieldManager.HORIZONTAL_SCROLL);
		hfm.setMargin((Assets.bgEditText[1].getHeight()-Assets.fontGlobal.getHeight())/2,
						Assets.bgEditText[0].getWidth(),
						(Assets.bgEditText[1].getHeight()-Assets.fontGlobal.getHeight())/2,
						Assets.bgEditText[2].getWidth());
		hfm.add(dropdownField);
		
		finalDropdownField = new HorizontalFieldManager(){
			protected void paint(Graphics g) {
				Bitmap[] bg;
				if(isError)
					bg = dropdownField.isFocus()?Assets.bgEditTextFocus:Assets.bgEditTextError;
				else
					bg = dropdownField.isFocus()?Assets.bgEditTextFocus:Assets.bgEditText;
				
				//draw the left side
				g.drawBitmap(0, 0, bg[0].getWidth(), bg[0].getHeight(), bg[0], 0, 0);
				
				//draw the middle side
				int ulang = width-(bg[0].getWidth()+bg[2].getWidth());
				for(int i=0 ; i<ulang ; i+=bg[1].getWidth()){
					g.drawBitmap(bg[0].getWidth()+i, 0, bg[1].getWidth(), bg[1].getHeight(), bg[1], 0, 0);
				}
				
				//draw the right side
				g.drawBitmap(bg[0].getWidth()+ulang, 0, bg[2].getWidth(), bg[2].getHeight(), bg[2], 0, 0);
				
				//draw icon
				g.drawBitmap(w-icon.getHeight()-15, (bg[0].getHeight()-icon.getHeight())/2, icon.getWidth(), icon.getHeight(), icon, 0, 0);
				
				super.paint(g);
			}
		};
		//finalField.setPadding(0, bg[2].getWidth(), 0, bg[1].getWidth());
		finalDropdownField.add(hfm);*/
		
		finalField = new VerticalFieldManager();
		if(label!="")finalField.add(labelField);
		finalField.add(dropdownField);
		finalField.setMargin(0, 0, 5, 0);
	}
	
	public Field getField(){
		return finalField;
	}
	
	public int getSelectedIndex(){
		return dropdownField.getSelectedIndex();
	}
	
	public void setSelectedIndex(int index){
		dropdownField.setSelectedIndex(index);
	}
	
	public String getSelectedIndexValue(){
		DropdownItemData data = (DropdownItemData) dropdownItemsData.elementAt(dropdownField.getSelectedIndex());
		return data.getId();
	}
	
	public String getSelectedIndexLabel(){
		DropdownItemData data = (DropdownItemData) dropdownItemsData.elementAt(dropdownField.getSelectedIndex());
		return data.getLabel();
	}
	
	public void setIsError(boolean isError){
		this.isError = isError;
		dropdownField.setIsError(isError);
	}
	
	public ObjectChoiceField getDropdown(){
		return dropdownField;
	}
}
