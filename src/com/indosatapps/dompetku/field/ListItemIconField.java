package com.indosatapps.dompetku.field;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.DataProcessor;
import com.indosatapps.dompetku.utils.DataRetriever;

public class ListItemIconField extends CustomField implements DataProcessor{
	String label, iconURL;
	int w, h;
	Font font;
	Bitmap icon;
	String fileName;
	
	public ListItemIconField(Bitmap icon, String label, int width, long style) {
		this(icon, label, null, width, style);
	}
	
	public ListItemIconField(Bitmap icon, String label, String iconURL, int width, long style) {
		super(style | FIELD_HCENTER );
		this.icon = icon;
		this.label = label;
		this.iconURL = iconURL;
		font = Assets.fontGlobalBold;
		w = width;
		h = font.getHeight()+20;
		
		if(this.icon.getWidth() > (h-10)){
			this.icon = BasicUtils.ResizeTransparentBitmap(icon, h-10, h-10, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_TO_FIT);
		}
		
		if(iconURL!=null){
			fileName = iconURL.substring(iconURL.lastIndexOf('/')+1, iconURL.lastIndexOf('.'));
			if(BasicUtils.checkFileExistance(fileName)){
				this.icon = BasicUtils.getBitmapFromFile(fileName);
			}else{
				DataRetriever ret = new DataRetriever();
				ret.addRequest(this);
			}
		}
	}
	
	protected void layout(int width, int height) {
		setExtent(w, h);
	}
	
	protected void paint(Graphics g) {
		if(isFocus()){
			g.setColor(Constant.colorPrimary);
			g.drawRoundRect(0, 0, w, h, 20, 20);
			g.fillRoundRect(0, 0, w, h, 20, 20);
		}
		
		g.drawBitmap(10, 5, icon.getWidth(), icon.getHeight(), icon, 0, 0);
		
		g.setFont(font);
		g.setColor(isFocus()?Constant.colorPageBackground:Constant.colorFontGeneral);
		g.drawText(label, 20+icon.getWidth(), (h-font.getHeight())/2);
	}
	
	public int getW(){
		return w;
	}
	
	public void setText(String text){
		label = text;
		invalidate();
	}
	
	public void setFont(Font font){
		this.font = font;
		invalidate();
	}
	
	public String getText(){
		return label;
	}

	public String getUrl() {
		return iconURL;
	}

	public void processData(byte[] data) {
		if(data==null){
			icon = Assets.noImage;
		}else{
			icon = Bitmap.createBitmapFromBytes(data, 0, data.length, 1);
			if(icon.getWidth() > (h-10)){
				icon = BasicUtils.ResizeTransparentBitmap(icon, h-10, h-10, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_TO_FIT);
			}
			BasicUtils.saveBitmapToFile(fileName, icon);
		}
		invalidate();
	}
}
