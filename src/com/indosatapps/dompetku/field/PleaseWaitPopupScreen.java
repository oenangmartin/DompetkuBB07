package com.indosatapps.dompetku.field;

import com.indosatapps.dompetku.data.Assets;

import net.rim.device.api.system.GIFEncodedImage;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;

public class PleaseWaitPopupScreen extends PopupScreen {

    //statics ------------------------------------------------------------------

    private AnimatedGIFField _ourAnimation = null;
    private LabelField _ourLabelField = null;

    public PleaseWaitPopupScreen(String text) {
        super(new HorizontalFieldManager());

        GIFEncodedImage ourAnimation = (GIFEncodedImage) GIFEncodedImage.getEncodedImageResource("loading.gif");
        _ourAnimation = new AnimatedGIFField(ourAnimation, Field.FIELD_VCENTER);
        _ourAnimation.setMargin(10, 10, 10, 0);
        this.add(_ourAnimation);
        
        _ourLabelField = new LabelField(text, Field.FIELD_VCENTER);
        _ourLabelField.setFont(Assets.fontGlobalBold);
        add(_ourLabelField);
    }

    public static void showScreenAndWait(final Runnable runThis, String text) {
        final PleaseWaitPopupScreen thisScreen = new PleaseWaitPopupScreen(text);
        Thread threadToRun = new Thread() {
            public void run() {
                // First, display this screen
                UiApplication.getUiApplication().invokeLater(new Runnable() {
                    public void run() {
                        UiApplication.getUiApplication().pushScreen(thisScreen);
                    }
                });
                // Now run the code that must be executed in the Background
                try {
                    runThis.run();
                } catch (Throwable t) {
                    t.printStackTrace();
                    throw new RuntimeException("Exception detected while waiting: " + t.toString());
                }
                // Now dismiss this screen
                UiApplication.getUiApplication().invokeLater(new Runnable() {
                    public void run() {
                        UiApplication.getUiApplication().popScreen(thisScreen);
                    }
                });
            }
        };
        threadToRun.start();
    }

}



