package com.indosatapps.dompetku.field;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;

import com.indosatapps.dompetku.bean.TransactionData;
import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.utils.BasicUtils;

public class ListHistoryTrxField extends CustomField{
	TransactionData trxData;
	int w, h;
	Font font1, font2, font3;
	Bitmap icon;
	String amount, amountRaw, type;
	
	public ListHistoryTrxField(TransactionData trxData, long style) {
		super(style | FIELD_HCENTER );
		this.trxData = trxData;
		font1 = Assets.fontGlobalBold;
		font2 = Assets.fontGlobal;
		font3 = Assets.fontMini;
		icon = Assets.basket;
		w = Constant.deviceWidth-30;
		h = font1.getHeight()+font2.getHeight()+font3.getHeight()+30;
		
		amountRaw = Integer.toString(trxData.getAmount());
		if(amountRaw.startsWith("-")){
        	amount = "("+BasicUtils.getFormattedBalance(amountRaw.substring(1), true)+")";
        }else{
        	amount = BasicUtils.getFormattedBalance(amountRaw, true);
        }
		
		type = trxData.getType()+" - ";
	}
	
	protected void layout(int width, int height) {
		setExtent(w, h);
	}
	
	protected void paint(Graphics g) {
		if(isFocus()){
			g.setColor(Constant.colorPrimary);
			g.drawRoundRect(0, 0, w, h, 20, 20);
			g.fillRoundRect(0, 0, w, h, 20, 20);
		}
		
		icon = isFocus()?Assets.basketFocus:Assets.basket;
		g.drawBitmap(0, (h-icon.getHeight())/2, icon.getWidth(), icon.getHeight(), icon, 0, 0);
		
		g.setColor(isFocus()?Constant.colorPageBackground:Constant.colorFontGeneral);
		g.setFont(font1);
		g.drawText(type, 10+icon.getWidth(), 10);
		g.setFont(font2);
		g.drawText(trxData.getAgent(), 10+icon.getWidth(), 15+font1.getHeight());
		g.setFont(font3);
		g.drawText(trxData.getDate(), 10+icon.getWidth(), 20+font1.getHeight()+font2.getHeight());
		
		g.setFont(font1);
		if(amountRaw.startsWith("-"))
			g.setColor(isFocus()?Constant.colorPageBackground:Constant.colorPrimary);
        else
        	g.setColor(isFocus()?Constant.colorPageBackground:Constant.colorFontGeneral);
		g.drawText(amount, 10+icon.getWidth()+font1.getAdvance(type), 10);
	}
}
