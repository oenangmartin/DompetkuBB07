package com.indosatapps.dompetku.field;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;

public class LabelFieldColor extends LabelField {
	int color;
	
	public LabelFieldColor(String label, int color, long style){
		super(label, style);
		this.color = color;
	}

	protected void paint(Graphics g) {
		g.setColor(color);
		super.paint(g);
	}
	
	protected void drawFocus(Graphics g, boolean bo){
	}
	
	protected void onFocus(int direction){
		
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus(){
		super.onUnfocus();
		invalidate();
	}
}
