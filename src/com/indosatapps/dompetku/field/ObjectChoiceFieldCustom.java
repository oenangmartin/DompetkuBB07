package com.indosatapps.dompetku.field;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.FocusChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.ObjectChoiceField;

public class ObjectChoiceFieldCustom extends ObjectChoiceField{
	int width, height;
	Font font;
	Bitmap icon;
	boolean isError = false;
	
	public ObjectChoiceFieldCustom(String label, Object[] choices, int initialIndex, long style) {
		super(label, choices, initialIndex, style);
		width = Constant.deviceWidth-30;
		height = Assets.bgEditText[0].getHeight();
		font = Assets.fontGlobal;
		icon = Assets.iconDropdown;
	}
	
	protected void layout(int width, int height) {
		super.layout(this.width, this.height);
		setExtent(this.width, this.height);
	}
	
	protected void paint(Graphics g) {
		Bitmap[] bg;
		if(isError)
			bg = isFocus()?Assets.bgEditTextFocus:Assets.bgEditTextError;
		else
			bg = isFocus()?Assets.bgEditTextFocus:Assets.bgEditText;
		
		//draw the left side
		g.drawBitmap(0, 0, bg[0].getWidth(), bg[0].getHeight(), bg[0], 0, 0);
		
		//draw the middle side
		int ulang = width-(bg[0].getWidth()+bg[2].getWidth());
		for(int i=0 ; i<ulang ; i+=bg[1].getWidth()){
			g.drawBitmap(bg[0].getWidth()+i, 0, bg[1].getWidth(), bg[1].getHeight(), bg[1], 0, 0);
		}
		
		//draw the right side
		g.drawBitmap(bg[0].getWidth()+ulang, 0, bg[2].getWidth(), bg[2].getHeight(), bg[2], 0, 0);
		
		//draw icon
		g.drawBitmap(width-icon.getWidth()-12, (bg[0].getHeight()-icon.getHeight())/2, icon.getWidth(), icon.getHeight(), icon, 0, 0);
		
		g.setFont(font);
		g.setColor(Constant.colorFontFormInput);
		g.drawText(getChoice(getSelectedIndex()).toString(), bg[0].getWidth(), (bg[0].getHeight()-font.getHeight())/2);
		
		//super.paint(g);
	}
	
	public void invalidate(){
		super.invalidate();
	}
	
	public void setIsError(boolean isError){
		this.isError = isError;
		invalidate();
	}
}
