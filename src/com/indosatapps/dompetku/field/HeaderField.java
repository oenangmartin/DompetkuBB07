package com.indosatapps.dompetku.field;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;

public class HeaderField extends Field{
	private String title;
	private int w, h;
	
	public HeaderField(String title, long style) {
		super(style | NON_FOCUSABLE);
		this.title = title;
		w = Constant.deviceWidth;
		h = Assets.headerLogo.getHeight();
	}
	
	protected void layout(int width, int height) {
		setExtent(w, h);
	}

	protected void paint(Graphics g) {
		/*for(int i=0 ; i<Banks.deviceWidth ; i+=Banks.bg_header.getWidth()){
			g.drawBitmap(i, 0, Banks.bg_header.getWidth(),
					Banks.bg_header.getHeight(), Banks.bg_header, 0, 0);
		}*/
		
		g.setColor(Constant.colorPageBackground);
		g.drawRect(0, 0, w, h);
		g.fillRect(0, 0, w, h);
		g.drawBitmap(10, 0, Assets.headerLogo.getWidth(), Assets.headerLogo.getHeight(), Assets.headerLogo, 0, 0);
		
		g.setFont(Assets.fontGlobalBold);
		g.setColor(Color.BLACK);
		g.drawText(title, (w-Assets.fontGlobalBold.getAdvance(title)-10), (h-Assets.fontGlobalBold.getHeight())/2);
	}

	public int getPreferredHeight() {
		return h;
	}
	
	public void setTitle(String title){
		this.title = title;
		invalidate();
	}

	public boolean isFocusable(){
		return false;
	}
	
	public void invalidate(){
		super.invalidate();
	}
	
	protected void drawFocus(Graphics g, boolean bo){
	}
	
	protected void onFocus(int direction){
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus(){
		super.onUnfocus();
		invalidate();
	}
}