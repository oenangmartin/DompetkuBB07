package com.indosatapps.dompetku;

import java.util.Vector;

import javax.microedition.pim.Contact;
import javax.microedition.pim.PIM;
import javax.microedition.pim.PIMItem;

import net.rim.blackberry.api.pdap.BlackBerryContact;
import net.rim.blackberry.api.pdap.BlackBerryContactList;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.DialogClosedListener;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.text.NumericTextFilter;
import net.rim.device.api.ui.text.TextFilter;
import net.rim.device.api.util.StringUtilities;

import org.json.me.JSONObject;

import com.indosatapps.dompetku.bean.InboxData;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.ButtonFieldCustom;
import com.indosatapps.dompetku.field.EditFieldCustom;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.PasswordEditFieldCustom;
import com.indosatapps.dompetku.field.PleaseWaitPopupScreen;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.Connection;
import com.indosatapps.dompetku.utils.PageResultListener;

public class TransferDompetku extends MainScreen implements FieldChangeListener{
	private int TASK_INQUIRY = 1;
    private int TASK_TRANSFER = 2;
    
	Mediator mediator;
	EditFieldCustom editNoTujuan, editJumlah;
	PasswordEditFieldCustom editPIN;
	ButtonFieldCustom btnSubmit;
	PageResultListener listener;
	String mTujuan, mJumlah, mPIN;
	Vector numberList = new Vector();
	
	public TransferDompetku(Mediator mediator, PageResultListener listener) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.listener = listener;
		this.mediator = mediator;
		HeaderField header = new HeaderField("Dompetku", 0);
		add(header);
		
		editNoTujuan = new EditFieldCustom("Nomor Tujuan", "", Constant.deviceWidth-30, 16, false, true, FIELD_HCENTER);
		editNoTujuan.setFilter(NumericTextFilter.get(NumericTextFilter.NUMERIC));
		editJumlah = new EditFieldCustom("Jumlah", "", Constant.deviceWidth-30, 10, false, false, FIELD_HCENTER);
		editJumlah.setFilter(NumericTextFilter.get(NumericTextFilter.NUMERIC));
		editPIN = new PasswordEditFieldCustom("PIN", "", Constant.deviceWidth-30, 6, FIELD_HCENTER);
		editPIN.setFilter(TextFilter.get(TextFilter.NUMERIC));
		
		editNoTujuan.setBtnSearchListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context) {
				try{
					BlackBerryContactList list = (BlackBerryContactList)PIM.getInstance().
												openPIMList(PIM.CONTACT_LIST, PIM.READ_ONLY);
					//Contact contact = list.choose(null, BlackBerryContactList.AddressTypes.EMAIL, false);
					PIMItem contact = list.choose();
					processContact(contact);
				}catch (Exception e) {
					System.out.println(">> Contact Picker error: "+e.getMessage());
					e.printStackTrace();
					Dialog.alert("Gagal membuka Kontak.");
				}
			}
		});
		
		btnSubmit = new ButtonFieldCustom("KIRIM", Constant.deviceWidth/3, FIELD_HCENTER);
		btnSubmit.setEditable(true);
		btnSubmit.setMargin(10, 0, 10, 0);
		btnSubmit.setChangeListener(this);
		
		VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL);
		scr.setMargin(15, 15, 15, 15);
		scr.add(editNoTujuan.getField());
		scr.add(editJumlah.getField());
		scr.add(editPIN.getField());
		scr.add(btnSubmit);
		
		add(scr);
	}
	
	public void resetFieldState(){
        editNoTujuan.setIsError(false);
        editJumlah.setIsError(false);
        editPIN.setIsError(false);
    }
	
	public void processContact(PIMItem contact){
		if(contact!=null && contact instanceof BlackBerryContact){
			numberList.removeAllElements();
			int telephonesCount = contact.countValues(Contact.TEL);
			if(telephonesCount > 0){
			    for(int i=0; i< telephonesCount; ++i) {
			    	String number = contact.getString(Contact.TEL, i);
			    	if(number.indexOf(' ') > -1)
			    		number = StringUtilities.removeChars(number, " ");
			        if(number.indexOf("-") > -1)
			        	number = StringUtilities.removeChars(number, "-");
			        if(number.startsWith("+62"))
			        	number = "0"+number.substring(3);
			        
			        int attribute = contact.getAttributes(BlackBerryContact.TEL, i);
	                if(attribute == Contact.ATTR_MOBILE){
	                	numberList.addElement(new String[]{"Mobile: ", number});
	                }else if (attribute == Contact.ATTR_HOME){
	                	numberList.addElement(new String[]{"Home: ", number});
			    	}else if (attribute == Contact.ATTR_WORK){
			    		numberList.addElement(new String[]{"Work: ", number});
			    	}
	            }
			    
			    if(numberList.size()>0){
			    	if(numberList.size() == 1){
			    		String[] msisdn = (String[]) numberList.elementAt(0);
				    	editNoTujuan.setText(msisdn[1]);
			    	}else{
			    		ContactDialog d = new ContactDialog(numberList);
			    		d.doModal();
			    	}
			    }else{
			    	Dialog.alert("Nomor tidak ditemukan.");
			    }
			}else{
				//no number
				Dialog.alert("Nomor tidak ditemukan.");
			}
        }
	}
	
	public void fieldChanged(Field field, int context) {
		if(field == btnSubmit){
			mTujuan = editNoTujuan.getText().toString();
            mJumlah = editJumlah.getText().toString();
            mPIN = editPIN.getText().toString();
            
            resetFieldState();
            if(mTujuan.length() == 0){
                editNoTujuan.setIsError(true);
                Dialog.alert("Mohon isi Nomor Tujuan");
            }else if(!mTujuan.startsWith("0814") && !mTujuan.startsWith("0815") && !mTujuan.startsWith("0816") && !mTujuan.startsWith("0855")
                    && !mTujuan.startsWith("0856") && !mTujuan.startsWith("0857") && !mTujuan.startsWith("0858")){
                //not Indosat!
                editNoTujuan.setIsError(true);
                Dialog.alert("Nomor Tujuan harus operator Indosat");
            }else if(mJumlah.length() == 0){
                editJumlah.setIsError(true);
                Dialog.alert("Mohon isi Jumlah");
            }else if(mPIN.length() == 0){
                editPIN.setIsError(true);
                Dialog.alert("Mohon isi PIN");
            }else{
            	new DoTransferTask(TASK_INQUIRY, mTujuan, mJumlah, mPIN);
            }
		}
	}
	
	public class DoTransferTask implements Runnable{
		int taskID;
		String username, pin, to, amount;
		
		public DoTransferTask(int taskID, String to, String amount, String pin) {
			username = Constant.mPersistData.getUsername();
			this.to = to;
			this.amount = amount;
			this.pin = pin;
			this.taskID = taskID;
			PleaseWaitPopupScreen.showScreenAndWait(this, "Memproses transaksi...");
		}
		
		public void run() {
			String response = "false";
			if(taskID == TASK_TRANSFER)
                response = Connection.doTransferDompetku(username, pin, to, amount);
            else if(taskID == TASK_INQUIRY)
                response = Connection.doAccountInquiry(username, pin, to);
            
			final String entity = response;
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject objs = new JSONObject(entity);
                            int status = objs.getInt("status");
                            if(status == 0){
                            	if(taskID == TASK_INQUIRY){
                            		String jumlah = editJumlah.getText();
                                    try{
                                        int hargaInt = Integer.parseInt(jumlah);
                                        hargaInt = hargaInt + 500;
                                        jumlah = Integer.toString(hargaInt);
                                    }catch(Exception e){
                                        jumlah = editJumlah.getText();
                                        e.printStackTrace();
                                    }
                                    String nama = objs.getString("name");
                                    String msg = "Anda akan melakukan Kirim Uang ke\nNomor: "+editNoTujuan.getText()
                                                +"\nNama: "+nama
                                                +"\nJumlah: "+editJumlah.getText()
                                                +"\nFee: 500"
                                                +"\nTotal: "+jumlah;
                                    Dialog d = new Dialog(msg, new String[]{"Setuju", "Batal"}, new int[]{Dialog.OK, Dialog.CANCEL}, Dialog.OK, null);
                            		d.setEscapeEnabled(false);
                            		d.setDialogClosedListener(new DialogClosedListener() {
    									public void dialogClosed(Dialog dialog, int choice) {
    										if(choice == Dialog.OK){
    											new DoTransferTask(TASK_TRANSFER, mTujuan, mJumlah, mPIN);
    										}
    									}
    								});
                            		d.show();
                            	}else if(taskID == TASK_TRANSFER){
                            		String trxID = objs.getString("trxid");
                                    long currTimeMilis = System.currentTimeMillis();
                                    String currTime = BasicUtils.formatDateFromMilliseconds(currTimeMilis, "dd/MM/yyyy HH:mm");
                                    InboxData inboxData = new InboxData(trxID, currTimeMilis, editJumlah.getText(),
                                            editNoTujuan.getText(), "-", "Kirim", "Uang", "-", "-", "-");
                                    Constant.mListInbox.addElement(inboxData);
                                    Constant.mHashMapInbox.put(Long.toString(inboxData.getDate()), inboxData);
                                    BasicUtils.saveInboxData();
                                    
                                    String msg = "Anda telah melakukan Kirim Uang pada tanggal "+currTime.substring(0, currTime.indexOf(" "))+
                                            " ke "+editNoTujuan.getText().toString()+" sebesar "+editJumlah.getText().toString()+" dengan biaya sebesar 500."+
                                            "\nRefID: "+trxID;
                                    BasicUtils.showMessageDialog(msg, new DialogClosedListener() {
    									public void dialogClosed(Dialog dialog, int choice) {
    										if(choice == Dialog.OK){
    											listener.onPageResult(0, Constant.RESULT_OK, null);
    		                                	close();
    										}
    									}
    								});
                            	}
                            }else if(status == 1001){
                            	Dialog.alert("Saldo Anda tidak mencukupi untuk melakukan transaksi ini.");
                            }else if(status == 1014){
                            	Dialog.alert("PIN salah. Mohon periksa kembali PIN Anda.");
                            }else if(status == 23){
                            	Dialog.alert("Jumlah uang yang ditransfer kurang dari jumlah minimum  transaksi.");
                            }else if(status == 24){
                            	Dialog.alert("Jumlah terlalu besar");
                            }else{
                            	Dialog.alert("Transaksi gagal.");
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Dialog.alert(Constant.txt_error_json);
                        }
                    }else{
                    	Dialog.alert(Constant.txt_error_no_connection);
                    }
                }
			});
		}
	}
	
	class ContactDialog extends Dialog{
		
	    public ContactDialog(Vector numberList){
	        super("Pilih nomor:", null, null, Dialog.DISCARD, null, Dialog.GLOBAL_STATUS);
	        
	        VerticalFieldManager vfm = new VerticalFieldManager(FIELD_HCENTER);

	        for(int i=0;i<numberList.size();i++){
	        	final String[] msisdn = (String[]) numberList.elementAt(i);
	        	ButtonField button = new ButtonField(msisdn[0]+msisdn[1], FIELD_HCENTER);
	        	button.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						editNoTujuan.setText(msisdn[1]);
						close();
					}
				});
	        	vfm.add(button);
	        }
	        
		    this.add(vfm);
	    }
	    
	    public boolean keyChar(char key, int status, int time) {
	    	if(key==Characters.ESCAPE){
	    		this.close();
	    	}
	        return true;
	     }
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }
}
