package com.indosatapps.dompetku;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.ListItemField;
import com.indosatapps.dompetku.utils.PageResultListener;

public class Administrasi extends MainScreen implements FieldChangeListener, PageResultListener{
	Mediator mediator;
	ListItemField mGantiPIN;
	PageResultListener listener;
	
	public Administrasi(Mediator mediator, PageResultListener listener) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		this.listener = listener;
		HeaderField header = new HeaderField("Administrasi", 0);
		add(header);
		
		mGantiPIN = new ListItemField("Ganti PIN", Constant.deviceWidth-30, 0);
		mGantiPIN.setChangeListener(this);
		
		VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 15, 15, 15);
		scr.add(mGantiPIN);
		
		add(scr);
	}

	public void fieldChanged(Field field, int context) {
		if(field == mGantiPIN){
			mediator.app.pushScreen(new GantiPIN(mediator, this));
		}
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }

	public void onPageResult(int requestCode, int resultCode, Object data) {
		if(requestCode == Constant.REQUEST_CODE_GANTI_PIN && resultCode == Constant.RESULT_OK){
			listener.onPageResult(Constant.REQUEST_CODE_GANTI_PIN, Constant.RESULT_OK, null);
        	close();
		}
	}

}
