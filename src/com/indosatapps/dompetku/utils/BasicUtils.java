package com.indosatapps.dompetku.utils;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.microedition.global.Formatter;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.io.file.FileConnection;

import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.io.IOUtilities;
import net.rim.device.api.io.SocketConnectionEnhanced;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.PNGEncodedImage;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.DialogClosedListener;

import com.indosatapps.dompetku.bean.DompetkuPersistData;
import com.indosatapps.dompetku.bean.FavoriteData;
import com.indosatapps.dompetku.bean.InboxData;
import com.indosatapps.dompetku.bean.KodeBankData;
import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;

/**
 * The class that contains functional operation such as resizing bitmap, split string, etc.
 * 
 * @author Rhio
 *
 */
public class BasicUtils {
	
	/**
	 * create a simple connection using available network
	 * 
	 * @param url the url
	 * @return the response data
	 */
	public static byte[] getHttpData(String url){
		BlackberryUtils.setBlackBerryConnectionParams();
		
		byte[] data=null;
		if(BlackberryUtils.currentConnection==-1){
            return data;
        }
		
		try{
			url+=BlackberryUtils.currentConnectionString;
			
			HttpConnection conn=(HttpConnection)Connector.open(url);
			InputStream is=conn.openInputStream();
			
			ByteArrayOutputStream baos=new ByteArrayOutputStream();
			byte[] buffer=new byte[1024];
			int size;
			while((size=is.read(buffer,0,buffer.length))!=-1){
				baos.write(buffer, 0, size);
			}
			baos.flush();
			data=baos.toByteArray();
			
			baos.close();
			is.close();
			conn.close();
		}catch(Exception e){			
			System.out.println("error http nih:"+e.getMessage());
		}
		return data;
		
	}
	
	public static byte[] getHttpDataViaWiFi(String url){
		byte[] data=null;
		try{
			/*MyConnectionFactory connFact = new MyConnectionFactory();
			ConnectionDescriptor connDesc;
			connDesc = connFact.getConnection(url);
			if (connDesc != null) {
				HttpConnection conn = (HttpConnection)connDesc.getConnection();*/
				HttpConnection conn=(HttpConnection)Connector.open(url+";interface=wifi;EndToEndDesired");
				SocketConnectionEnhanced sce = (SocketConnectionEnhanced) conn;
                sce.setSocketOptionEx(SocketConnectionEnhanced.READ_TIMEOUT, 30000);
				
				InputStream is=conn.openInputStream();

	  			ByteArrayOutputStream baos=new ByteArrayOutputStream();
	  			byte[] buffer=new byte[1024];
	  			int size;
	  			while((size=is.read(buffer,0,buffer.length))!=-1){
	  				baos.write(buffer, 0, size);
	  			}
	  			baos.flush();
	  			data=baos.toByteArray();
	  			
	  			baos.close();
	  			is.close();
	  			conn.close();
			//}
		}catch(Exception e){			
			System.out.println(">> getHttpDataViaWiFi error: "+e.getMessage());
			e.printStackTrace();
		}
		return data;
		
	}
	
	/*HttpConnection conn=(HttpConnection)Connector.open(url);
	conn.setRequestMethod(HttpConnection.POST);
	conn.setRequestProperty("Content-Type", CONTENT_TYPE_FORM_POST);
    conn.setRequestProperty("Content-length", String.valueOf(postData.length));*/
	
	/**
	 * 
	 * @param bmp the bitmap
	 * @param width the new width
	 * @param height the new height
	 * @return the bitmap after resized
	 */
	public static Bitmap resizeBitmap(Bitmap bmp, int width, int height, int aspectRatio)
	{
		Bitmap result = new Bitmap(width, height);
		bmp.scaleInto(result, Bitmap.FILTER_LANCZOS, aspectRatio);
		return result;
	}

	/**
	 *
	 * @param bmp the bitmap
	 * @param width the new width
	 * @param height the new height
	 * @return the bitmap after resized
	 */
	public static Bitmap resizeBitmapNoScale(Bitmap bmp, int width, int height)
	{
		Bitmap result = new Bitmap(width, height);
		bmp.scaleInto(result, Bitmap.FILTER_LANCZOS);
		return result;
	}
	
	/**
	 * Resize the transparent bitmap
	 * 
	 * @param bmpSrc the transparent bitmap (.png)
	 * @param nWidth the new width
	 * @param nHeight the new height
	 * @param nFilterType Interpolation filter type. May be one of: 
				Bitmap.FILTER_LANCZOS 
				Bitmap.FILTER_BOX 
				Bitmap.FILTER_BILINEAR 
	 * @param nAspectRatio preserveAspectRatio - - preserve aspect ratio option. May be one of: 
				Bitmap.SCALE_STRETCH - aspect ratio is not preserved 
				Bitmap.SCALE_TO_FIT � preserve aspect ratio and fit the source bitmap to the destination bitmap 
				Bitmap.SCALE_TO_FILL � preserve aspect ratio and fill the destination bitmap with the source bitmap 

	 * @return the new bitmap after resized
	 */
	public static Bitmap ResizeTransparentBitmap(Bitmap bmpSrc, int nWidth, int nHeight, int nFilterType, int nAspectRatio)
    {
        if(bmpSrc== null)
            return null;

        //Get the original dimensions of the bitmap
        int nOriginWidth = bmpSrc.getWidth();
        int nOriginHeight = bmpSrc.getHeight();
        if(nWidth == nOriginWidth && nHeight == nOriginHeight)
            return bmpSrc;

        //Prepare a drawing bitmap and graphic object
        Bitmap bmpOrigin = new Bitmap(nOriginWidth, nOriginHeight);
        Graphics graph = Graphics.create(bmpOrigin);

        //Create a line of transparent pixels for later use
        int[] aEmptyLine = new int[nWidth];
        for(int x = 0; x < nWidth; x++)
            aEmptyLine[x] = 0x00000000;
        //Create two scaled bitmaps
        Bitmap[] bmpScaled = new Bitmap[2];
        for(int i = 0; i < 2; i++)
        {
            //Draw the bitmap on a white background first, then on a black background
            graph.setColor((i == 0) ? Color.WHITE : Color.BLACK);
            graph.fillRect(0, 0, nOriginWidth, nOriginHeight);
            graph.drawBitmap(0, 0, nOriginWidth, nOriginHeight, bmpSrc, 0, 0);

            //Create a new bitmap with the desired size
            bmpScaled[i] = new Bitmap(nWidth, nHeight);
            if(nAspectRatio == Bitmap.SCALE_TO_FIT)
            {
                //Set the alpha channel of all pixels to 0 to ensure transparency is
                //applied around the picture, if needed by the transformation
                for(int y = 0; y < nHeight; y++)
                    bmpScaled[i].setARGB(aEmptyLine, 0, nWidth, 0, y, nWidth, 1);
            }

            //Scale the bitmap
            bmpOrigin.scaleInto(bmpScaled[i], nFilterType, nAspectRatio);
        }

        //Prepare objects for final iteration
        Bitmap bmpFinal = bmpScaled[0];
        int[][] aPixelLine = new int[2][nWidth];

        //Iterate every line of the two scaled bitmaps
        for(int y = 0; y < nHeight; y++)
        {
            bmpScaled[0].getARGB(aPixelLine[0], 0, nWidth, 0, y, nWidth, 1);
            bmpScaled[1].getARGB(aPixelLine[1], 0, nWidth, 0, y, nWidth, 1);

            //Check every pixel one by one
            for(int x = 0; x < nWidth; x++)
            {
                //If the pixel was untouched (alpha channel still at 0), keep it transparent
                if(((aPixelLine[0][x] >> 24) & 0xff) == 0)
                    aPixelLine[0][x] = 0x00000000;
                else
                {
                    //Compute the alpha value based on the difference of intensity
                    //in the red channel
                    int nAlpha = ((aPixelLine[1][x] >> 16) & 0xff) -
                                    ((aPixelLine[0][x] >> 16) & 0xff) + 255;
                    if(nAlpha == 0)
                        aPixelLine[0][x] = 0x00000000; //Completely transparent
                    else if(nAlpha >= 255)
                        aPixelLine[0][x] |= 0xff000000; //Completely opaque
                    else
                    {
                        //Compute the value of the each channel one by one
                        int nRed = ((aPixelLine[0][x] >> 16 ) & 0xff);
                        int nGreen = ((aPixelLine[0][x] >> 8 ) & 0xff);
                        int nBlue = (aPixelLine[0][x] & 0xff);

                        nRed = (int)(255 + (255.0 * ((double)(nRed-255)/(double)nAlpha)));
                        nGreen = (int)(255 + (255.0 * ((double)(nGreen-255)/(double)nAlpha)));
                        nBlue = (int)(255 + (255.0 * ((double)(nBlue-255)/(double)nAlpha)));

                        if(nRed < 0) nRed = 0;
                        if(nGreen < 0) nGreen = 0;
                        if(nBlue < 0) nBlue = 0;
                        aPixelLine[0][x] = nBlue | (nGreen<<8) | (nRed<<16) | (nAlpha<<24);
                    }
                }
            }

            //Change the pixels of this line to their final value
            bmpFinal.setARGB(aPixelLine[0], 0, nWidth, 0, y, nWidth, 1);
        }
        return bmpFinal;
    }
	
	/**
	 * Split the text based on the delimeter
	 * 
	 * @param strString the text
	 * @param strDelimiter the delimeter
	 * @return
	 */
	public static String[] split(String strString, String strDelimiter) {
	    String[] strArray;
	    int iOccurrences = 0;
	    int iIndexOfInnerString = 0;
	    int iIndexOfDelimiter = 0;
	    int iCounter = 0;

	    //Check for null input strings.
	    if (strString == null) {
	        throw new IllegalArgumentException("Input string cannot be null.");
	    }
	    //Check for null or empty delimiter strings.
	    if (strDelimiter.length() <= 0 || strDelimiter == null) {
	        throw new IllegalArgumentException("Delimeter cannot be null or empty.");
	    }

	    //strString must be in this format: (without {} )
	    //"{str[0]}{delimiter}str[1]}{delimiter} ...
	    // {str[n-1]}{delimiter}{str[n]}{delimiter}"

	    //If strString begins with delimiter then remove it in order
	    //to comply with the desired format.

	    if (strString.startsWith(strDelimiter)) {
	        strString = strString.substring(strDelimiter.length());
	    }

	    //If strString does not end with the delimiter then add it
	    //to the string in order to comply with the desired format.
	    if (!strString.endsWith(strDelimiter)) {
	        strString += strDelimiter;
	    }

	    //Count occurrences of the delimiter in the string.
	    //Occurrences should be the same amount of inner strings.
	    while((iIndexOfDelimiter = strString.indexOf(strDelimiter,
	           iIndexOfInnerString)) != -1) {
	        iOccurrences += 1;
	        iIndexOfInnerString = iIndexOfDelimiter +
	            strDelimiter.length();
	    }

	    //Declare the array with the correct size.
	    strArray = new String[iOccurrences];

	    //Reset the indices.
	    iIndexOfInnerString = 0;
	    iIndexOfDelimiter = 0;

	    //Walk across the string again and this time add the
	    //strings to the array.
	    while((iIndexOfDelimiter = strString.indexOf(strDelimiter,
	           iIndexOfInnerString)) != -1) {

	        //Add string to array.
	        strArray[iCounter] = strString.substring(iIndexOfInnerString,iIndexOfDelimiter);

	        //Increment the index to the next character after
	        //the next delimiter.
	        iIndexOfInnerString = iIndexOfDelimiter +
	            strDelimiter.length();

	        //Inc the counter.
	        iCounter += 1;
	    }

	    return strArray;
	}
	
	/**
	 * Check the file existance on the device
	 * 
	 * @param fName the file name
	 * @return
	 */
	public static boolean checkFileExistance(String fName){
		FileConnection fconn = null;
		try {
			fconn = (FileConnection) Connector.open(Constant.TXT_URI+fName, Connector.READ_WRITE);
			if (fconn.exists()){
				return true;
			}else{
				return false;
			}
		}catch (Exception e) {
			System.out.println(">> Check file existance error: "+e.getMessage());
			return false;
		}
	}
	
	public static String readTextFile(String fName) {
		String result = null;
		FileConnection fconn = null;
		DataInputStream is = null;
		createSubFolder();
		try {
			fconn = (FileConnection) Connector.open(Constant.TXT_URI+fName, Connector.READ_WRITE);
			is = fconn.openDataInputStream();
			byte[] data = IOUtilities.streamToBytes(is);
			result = new String(data);
			System.out.println(">> Read file "+fName+" : "+result);
		}catch (IOException e) {
			System.out.println(">> Read txt file error1: "+e.getMessage());
			result = "false";
		}finally {
			try {
				if (null != is)
				is.close();
				if (null != fconn)
				fconn.close();
			}catch (IOException e) {
				System.out.println(">> Read txt file error2: "+e.getMessage());
				result = "false";
			}
		}
		return result;
	}
	
	public static void writeTextFile(String fName, String text) {
		DataOutputStream os = null;
		FileConnection fconn = null;
		createSubFolder();
		try {
			fconn = (FileConnection) Connector.open(Constant.TXT_URI+fName, Connector.READ_WRITE);
			if (!fconn.exists())
				fconn.create();
				
			os = fconn.openDataOutputStream();
			os.write(text.getBytes());
		}catch (IOException e) {
			System.out.println(">> Write txt file error1: "+e.getMessage());
		}finally {
			try {
				if (null != os)
				os.close();
				if (null != fconn)
				fconn.close();
			}catch (IOException e) {
				System.out.println(">> Write txt file error2: "+e.getMessage());
			}
		}
	}
	
	public static void deleteTextFile(String fName){
		FileConnection fconn = null;
		try {
			fconn = (FileConnection) Connector.open(Constant.TXT_URI+fName, Connector.READ_WRITE);
			if (fconn.exists())
				fconn.delete();
		}catch (Exception e) {
			System.out.println(">> Delete txt file error: "+e.getMessage());
		}
	}
	
	public static void createSubFolder(){
		FileConnection fconn = null;
		FileConnection fconn2 = null;
		try {
			fconn = (FileConnection) Connector.open("file:///store/home/user/Databases/", Connector.READ_WRITE);
			if (!fconn.exists()){
				fconn.mkdir();
			}
			
			fconn2 = (FileConnection) Connector.open(Constant.TXT_URI, Connector.READ_WRITE);
			if (!fconn2.exists()){
				fconn2.mkdir();
			}
		}catch (Exception e) {
			System.out.println(">> Create subFolder error: "+e.getMessage());
		}
	}
	
	public static String readFileToString(Class mClass, String fileName) {
		byte[] content = "false".getBytes();
		try{
		    InputStream is = mClass.getResourceAsStream("/"+fileName);
		    content = IOUtilities.streamToBytes(is);
		    is.close();
		}catch (Exception e) {
			e.printStackTrace();
			content = "false".getBytes();
		}
	    return new String(content);
	}
	
	public static void disableOrientationChange(){
	    // force app to use only portrait mode
	    int directions = Display.DIRECTION_NORTH;
	    UiEngineInstance engineInstance = Ui.getUiEngineInstance();
	    if (engineInstance != null){
	        engineInstance.setAcceptableDirections(directions);
	    }
	}
	
	public static void saveBitmapToFile(String fName, Bitmap bmp){
		createSubFolder();
        try {
            FileConnection fconn = (FileConnection) Connector.open(Constant.TXT_URI+fName, Connector.READ_WRITE);
            if (!fconn.exists())
                fconn.create();
            OutputStream out = fconn.openOutputStream();
            //PNGEncodedImage encodedImage = PNGEncodedImage.encode(bmp);
            //byte[] imageBytes = encodedImage.getData();
            PNGEncoder encoder = new PNGEncoder(bmp, true);
            byte[] imageBytes = encoder.encode(true);
            out.write(imageBytes);
            out.close();
            fconn.close();
        } catch (Exception e) {
            System.out.println(">> Exception while saving Bitmap: "+ e.toString());
            System.out.println(">> File Name: "+ fName);
            e.getMessage();
        }
        System.out.println(">> File has been saved succesfully in " + Constant.TXT_URI+fName );
	}
	
	public static Bitmap getBitmapFromFile(String fName){
	    Bitmap bitmap=null, scaleBitmap=null;
	    InputStream inputStream=null;
	    FileConnection fileConnection=null;     
	    try{
	        fileConnection=(FileConnection) Connector.open(Constant.TXT_URI+fName);
	        if(fileConnection.exists()){
	            inputStream = fileConnection.openInputStream();           
	            byte[] data = new byte[(int)fileConnection.fileSize()];           
	            data = IOUtilities.streamToBytes(inputStream);
	            inputStream.close();
	            fileConnection.close();
	            bitmap = Bitmap.createBitmapFromBytes(data, 0, data.length, 1);

	            //You can return this bitmap otherwise, after this you can scale it according to your requirement; like...
	            scaleBitmap = bitmap;
	            //scaleBitmap = BasicUtils.ResizeTransparentBitmap(bitmap, Assets.fontGlobalBold.getHeight()+10, Assets.fontGlobalBold.getHeight()+10, Bitmap.FILTER_LANCZOS, 1);
	        }else{
	        	System.out.println(">> Image file not exist");
	            scaleBitmap = Assets.noImage;//Otherwise, Give a Dialog here;
	        }
	    }catch (Exception e) {
	    	System.out.println(">> getBitmapFromFile error: "+e.getMessage());
	    	e.printStackTrace();
	        try{
	            if(inputStream!=null){
	                inputStream.close();                
	            }
	            if(fileConnection!=null){
	                fileConnection.close();
	            }
	        } catch (Exception exp) {}
	        scaleBitmap = Assets.noImage;//Your known Image;     
	    }
	    return scaleBitmap;//return the scale Bitmap not the original bitmap;
	}
	
	public static void loadPersistData(){
		PersistentObject persist = PersistentStore.getPersistentObject(Constant.PREFERENCE_FILE_KEY);
		if(persist.getContents() != null){
			Constant.mPersistData = (DompetkuPersistData) persist.getContents();
		}else{
			Constant.mPersistData.setUsername("");
			Constant.mPersistData.setLastSession("");
			Constant.mPersistData.setIsFirstInstall(Boolean.TRUE.toString());
			Constant.mPersistData.setIsShowTutorial(Boolean.TRUE.toString());
			Constant.mPersistData.setIsRegisPending(Boolean.FALSE.toString());
			Constant.mPersistData.setImageCache(new Hashtable());
			savePersistData();
		}
	}
	
	public static void savePersistData(){
		PersistentObject persist = PersistentStore.getPersistentObject(Constant.PREFERENCE_FILE_KEY);
		persist.setContents(Constant.mPersistData);
        persist.commit();
	}
	
	public static String createSignature(String username, String pin){
        String time = Long.toString(System.currentTimeMillis());
        String signA = time + pin;
        String signB = reverseString(pin) + "|" + username;
        String signC = signA+"|"+signB;
        
        System.out.println(">> SignC: "+signC);
        
        String encrypt = null;
        try {
            encrypt = new String(TripleDES.encrypt(signC));
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        System.out.println(">> Encrypt: "+encrypt);
        return encrypt;
    }
	
	public static String  reverseString(String input){
		String s = "";
		for (int i= input.length(); i > 0 ; --i ) {
		    s = s + input.charAt(i-1);
		}
		return s;
	}
	
    public static void saveUsernamePref(String username){
    	Constant.mPersistData.setUsername(username);
    	savePersistData();
    }
    
    public static void deleteUsernamePref(){
    	Constant.mPersistData.setUsername("");
    	savePersistData();
    }
    
    public static void saveLastSession(){
    	Constant.mPersistData.setLastSession(Long.toString(System.currentTimeMillis()));
    	savePersistData();
    }
    
    public static boolean isSessionActive(){
    	if(!Constant.mPersistData.getLastSession().equals("")){
			long lastSession = Long.parseLong(Constant.mPersistData.getLastSession());
		    System.out.println(">> Last session: "+formatDateFromMilliseconds(lastSession, "dd/MM/yyyy HH:mm"));
		    if(lastSession != 0){
		        long diffTime = System.currentTimeMillis() - lastSession;
		        int minutes = (int) (diffTime / (1000*60));
		        //if(minutes<0) minutes = minutes * -1;
		        System.out.println("Minutes Different = "+minutes+" minute(s)");
		        
		        if(minutes > 30)
		            return false;
		        else
		            return true;
		    }else{
		        return false;
		    }
    	}else
    		return false;
    }
    
    public static boolean isFirstTimeInstall(){
    	if(Constant.mPersistData.getIsFirstInstall().equals(Boolean.TRUE.toString()))
    		return true;
    	else
    		return false;
    }
    
    public static boolean isShowTutorial(){
    	if(Constant.mPersistData.getIsShowTutorial().equals(Boolean.TRUE.toString()))
			return true;
		else
			return false;
    }
    
    public static boolean isRegisterPending(){
        if(Constant.mPersistData.getIsRegisPending().equals(Boolean.TRUE.toString()))
			return true;
		else
			return false;
    }
    
    public static void saveFirstTimeInstall(){
    	Constant.mPersistData.setIsFirstInstall(Boolean.FALSE.toString());
    	savePersistData();
    }
    
    public static void saveShowTutorial(){
    	Constant.mPersistData.setIsShowTutorial(Boolean.FALSE.toString());
    	savePersistData();
    }
    
    public static void saveRegisterPending(String isRegisPending){
        Constant.mPersistData.setIsRegisPending(isRegisPending);
    	savePersistData();
    }
    
    public static Vector readListKodeBank(Class mClass) {
    	Vector list = new Vector();
    	String result = readFileToString(mClass, "list_kode_bank");
    	if(!result.equals("false")){
    		String[] bank = split(result, "#");
    		for(int i=0;i<bank.length;i++){
    			list.addElement(new KodeBankData(bank[i].substring(bank[i].length()-3), bank[i].substring(0, bank[i].indexOf(";"))));
    		}
    	}else{
    		System.out.println(">> Read List Kode Bank error!");
    	}
        return list;
    }
    
    public static Vector readListStasiun(Class mClass) {
    	Vector list = new Vector();
    	String result = readFileToString(mClass, "list_stasiun_krl");
    	if(!result.equals("false")){
    		String[] stasiun = split(result, ";");
    		for(int i=0;i<stasiun.length;i++){
    			list.addElement(stasiun[i]);
    		}
    	}else{
    		System.out.println(">> Read List Stasiun error!");
    	}
        return list;
    }
    
    public static String getFormattedBalance(String balance, boolean isUsingCurrency){
        double harga = Double.parseDouble(balance);
        balance = formatNumber(harga, 0, ".");
        System.out.println("ID: " + balance);
        
        if(isUsingCurrency){
            balance = "Rp "+balance;
            System.out.println("ID after curr: " + balance);
        }
        
        return balance;
    }
    
    public static String formatNumber(double number, int decimals, String digitGrouping){
		Formatter f = new Formatter("en");
		String rawNumber = f.formatNumber(number, decimals+1);
		
		String rawIntString = rawNumber.substring(0, rawNumber.indexOf(".")); //Basically intString without digit grouping
		StringBuffer intString = new StringBuffer();
		StringBuffer decString = new StringBuffer(rawNumber.substring(rawNumber.indexOf(".")+1));
		StringBuffer formattedNumber = new StringBuffer();
		int workingVal = 0;
		int newNum = 0;
		boolean roundNext;

		//Add digit grouping
		int grouplen = 0;
		int firstDigit;
		if(rawIntString.charAt(0) == '-'){
			firstDigit = 1;
		}else{
			firstDigit = 0;
		}
		for(int n=rawIntString.length()-1;n>=firstDigit;n--){
			intString.insert(0, rawIntString.substring(n, n+1));
			grouplen++;
			if(grouplen == 3 && n>firstDigit){
				intString.insert(0, digitGrouping);
				grouplen = 0;
			}
		}

		//First, check the last digit
		workingVal = Integer.parseInt(String.valueOf(decString.charAt(decString.length()-1)));
		if(workingVal>=5){
			roundNext = true;
		}else{
			roundNext = false;
		}
		//Get the decimal values, round if needed, and add to formatted string buffer
		for(int n=decString.length()-2;n>=0;n--){
			workingVal = Integer.parseInt(String.valueOf(decString.charAt(n)));
			if(roundNext == true){
				newNum = workingVal + 1;
				if(newNum == 10){
					roundNext = true;
					newNum = 0;
				}else{
					roundNext = false;
				}
				formattedNumber.insert(0, newNum);
			}else{
				formattedNumber.insert(0, workingVal);
			}
		}
		//Now get the integer values, round if needed, and add to formatted string buffer
		formattedNumber.insert(0, ".");
		for(int n=intString.length()-1;n>=0;n--){
			try{
				workingVal = Integer.parseInt(String.valueOf(intString.charAt(n)));
			}catch(Exception e){
				formattedNumber.insert(0, intString.charAt(n));
				continue;
			}
			if(roundNext == true){
				newNum = workingVal + 1;
				if(newNum == 10){
					roundNext = true;
					newNum = 0;
				}else{
					roundNext = false;
				}
				formattedNumber.insert(0, newNum);
			}else{
				formattedNumber.insert(0, workingVal);
			}	
		}
		
		//Just in case its a number like 9999.99999 (if it rounds right to the end
		if(roundNext == true){
			formattedNumber.insert(0, 1);
			
		}	
		
		//re-add the minus sign if needed
		if(firstDigit == 1) formattedNumber.insert(0, rawIntString.charAt(0));
		
		if(digitGrouping.length() > 0){
			if(formattedNumber.toString().indexOf(".") == -1){
				//no decimal
				if(formattedNumber.toString().indexOf(digitGrouping) > 3+firstDigit){
					formattedNumber.insert(1+firstDigit, digitGrouping);
				}
				
				if(formattedNumber.toString().length() == 4+firstDigit){
					formattedNumber.insert(1+firstDigit, digitGrouping);
				}
			}else{
				//no decimal
				if(formattedNumber.toString().indexOf(digitGrouping) > 3+firstDigit){
					formattedNumber.insert(1+firstDigit, digitGrouping);
				}
				
				String intportion = formattedNumber.toString().substring(0, formattedNumber.toString().indexOf("."));
				if(intportion.length() == 4+firstDigit){
					formattedNumber.insert(1+firstDigit, digitGrouping);
				}
			}
		}

		//now remove trailing zeros
		String tmp = formattedNumber.toString();
		int newLength = tmp.length();
		for(int n=tmp.length()-1;n>=0;n--){
			if(tmp.substring(n, n+1).equalsIgnoreCase("0")){
				newLength--;
			}else{
				if(tmp.substring(n, n+1).equalsIgnoreCase(".")) newLength--;
				break;
			}
		}
		formattedNumber.setLength(newLength);

		return formattedNumber.toString();
	}
    
    public static String formatDateFromMilliseconds(long milliSeconds, String dateFormat){
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date. 
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(milliSeconds));
        return formatter.format(calendar.getTime());
    }
    
    public static String CalendarToStandardDate(String dd, String mm, String yy) {
        if (mm.equalsIgnoreCase("01")) {
            mm = "Januari";
        } else if (mm.equalsIgnoreCase("02")) {
            mm = "Februari";
        } else if (mm.equalsIgnoreCase("03")) {
            mm = "Maret";
        } else if (mm.equalsIgnoreCase("04")) {
            mm = "April";
        } else if (mm.equalsIgnoreCase("05")) {
            mm = "Mei";
        } else if (mm.equalsIgnoreCase("06")) {
            mm = "Juni";
        } else if (mm.equalsIgnoreCase("07")) {
            mm = "Juli";
        } else if (mm.equalsIgnoreCase("08")) {
            mm = "Agustus";
        } else if (mm.equalsIgnoreCase("09")) {
            mm = "September";
        } else if (mm.equalsIgnoreCase("10")) {
            mm = "Oktober";
        } else if (mm.equalsIgnoreCase("11")) {
            mm = "November";
        } else if (mm.equalsIgnoreCase("12")) {
            mm = "Desember";
        } else {
            mm = "";
        }

        String newDate = dd + " " + mm + " " + yy;
        return newDate;
    }

    public static String StandardToServerDate(String date) {
        if(date.length() == 0)
            return "";
        
        String yy = date.substring(date.length() - 4, date.length());
        String mm = date.substring(3, date.length() - 5);
        String dd = date.substring(0, 2);

        if (mm.equalsIgnoreCase("januari")) {
            mm = "01";
        } else if (mm.equalsIgnoreCase("februari")) {
            mm = "02";
        } else if (mm.equalsIgnoreCase("maret")) {
            mm = "03";
        } else if (mm.equalsIgnoreCase("april")) {
            mm = "04";
        } else if (mm.equalsIgnoreCase("mei")) {
            mm = "05";
        } else if (mm.equalsIgnoreCase("juni")) {
            mm = "06";
        } else if (mm.equalsIgnoreCase("juli")) {
            mm = "07";
        } else if (mm.equalsIgnoreCase("agustus")) {
            mm = "08";
        } else if (mm.equalsIgnoreCase("september")) {
            mm = "09";
        } else if (mm.equalsIgnoreCase("oktober")) {
            mm = "10";
        } else if (mm.equalsIgnoreCase("november")) {
            mm = "11";
        } else if (mm.equalsIgnoreCase("desember")) {
            mm = "12";
        } else {
            mm = "00";
        }
        String newDate = dd + mm + yy;
        return newDate;
    }

    public static void showMessageDialog(String message, DialogClosedListener listener){
        Dialog d = new Dialog(Dialog.D_OK, message, 0, Bitmap.getPredefinedBitmap(Bitmap.INFORMATION), 0);
		d.setEscapeEnabled(false);
		d.setDialogClosedListener(listener);
		d.show();
    }
    
    /*public static void showConfirmDialog(String message, OnClickListener listener){
        AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setMessage(message).setCancelable(false).setPositiveButton("OK", listener)
        .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog alert = dialog.create();
        alert.show();
    }*/
    
    public static void loadFavoriteData() {
    	Constant.mListFavorite.removeAllElements();
        Constant.mHashMapFavorite.clear();
    	try {
			String strFavorite = BasicUtils.readTextFile(Constant.PATH_FAVORITE+"_"+Constant.mPersistData.getUsername());
			if(!strFavorite.equals("false") || !strFavorite.equals("")){
				if(strFavorite.indexOf("**") > 0){
					String[] splitFav = BasicUtils.split(strFavorite, "**");
					for(int i=0;i<splitFav.length;i++){
						String[] temp = BasicUtils.split(splitFav[i], "##");
						FavoriteData favData = new FavoriteData(temp[0], temp[1], temp[2], temp[3], temp[4], temp[5], temp[6], temp[7]);
		                Constant.mListFavorite.addElement(favData);
		                Constant.mHashMapFavorite.put(favData.getFavoriteID(), favData);
					}
				}else{
					String[] temp = BasicUtils.split(strFavorite, "##");
					FavoriteData favData = new FavoriteData(temp[0], temp[1], temp[2], temp[3], temp[4], temp[5], temp[6], temp[7]);
	                Constant.mListFavorite.addElement(favData);
	                Constant.mHashMapFavorite.put(favData.getFavoriteID(), favData);
				}
			}
			System.out.println(">> Load Favorite TXT success. Size: "+Constant.mListFavorite.size());
		} catch (Exception e) {
			System.out.println(">> Read Favorite Exception: "+e.getMessage());
			e.printStackTrace();
		}
    }

    public static void saveFavoriteData() {
    	String temp = "";
		for(int i=0;i<Constant.mListFavorite.size();i++){
			if(i==0){
				temp = temp + getFavoriteString((FavoriteData) Constant.mListFavorite.elementAt(i));
			}else{
				temp = temp + "**" + getFavoriteString((FavoriteData) Constant.mListFavorite.elementAt(i));
			}
		}
		deleteTextFile(Constant.PATH_FAVORITE+"_"+Constant.mPersistData.getUsername());
		writeTextFile(Constant.PATH_FAVORITE+"_"+Constant.mPersistData.getUsername(), temp);
		System.out.println(">> Favorite saved: "+temp);
    }
    
    public static String getFavoriteString(FavoriteData data){
    	return data.getFavoriteID()+"##"+data.getFavoriteText()+"##"+data.getMenu()+"##"+data.getCategory()
        		+"##"+data.getCategoryID()+"##"+data.getMerchant()+"##"+data.getMerchantID()+"##"+data.getParamName();
    }
    
    public static void deleteFavoriteData(String favoriteID){
        if(Constant.mHashMapFavorite.containsKey(favoriteID)){
            Constant.mHashMapFavorite.remove(favoriteID);
            Constant.mListFavorite.removeAllElements();
            
            Enumeration e = Constant.mHashMapFavorite.keys();
            while (e.hasMoreElements()) {
            	String key = (String) e.nextElement();
            	Constant.mListFavorite.addElement(Constant.mHashMapFavorite.get(key));
            }
            saveFavoriteData();
        }else{
        	Dialog.alert("Favorite Item tidak ditemukan");
        }
    }
    
    public static void loadInboxData() {
    	Constant.mListInbox.removeAllElements();
        Constant.mHashMapInbox.clear();
    	try {
			String strInbox = BasicUtils.readTextFile(Constant.PATH_INBOX+"_"+Constant.mPersistData.getUsername());
			if(!strInbox.equals("false") || !strInbox.equals("")){
				if(strInbox.indexOf("**") > 0){
					String[] splitInbox = BasicUtils.split(strInbox, "**");
					for(int i=0;i<splitInbox.length;i++){
						String[] temp = BasicUtils.split(splitInbox[i], "##");
						InboxData inboxData = new InboxData(temp[0], Long.parseLong(temp[1]), temp[2], temp[3], temp[4], temp[5], temp[6], temp[7], temp[8], temp[9]);
		                Constant.mListInbox.addElement(inboxData);
		                Constant.mHashMapInbox.put(Long.toString(inboxData.getDate()), inboxData);
					}
				}else{
					String[] temp = BasicUtils.split(strInbox, "##");
					InboxData inboxData = new InboxData(temp[0], Long.parseLong(temp[1]), temp[2], temp[3], temp[4], temp[5], temp[6], temp[7], temp[8], temp[9]);
	                Constant.mListInbox.addElement(inboxData);
	                Constant.mHashMapInbox.put(Long.toString(inboxData.getDate()), inboxData);
				}
			}
			System.out.println(">> Load Inbox TXT success. Size: "+Constant.mListInbox.size());
		} catch (Exception e) {
			System.out.println(">> Read Inbox Exception: "+e.getMessage());
			e.printStackTrace();
		}
    }

    public static void saveInboxData() {
    	String temp = "";
		for(int i=0;i<Constant.mListInbox.size();i++){
			if(i==0){
				temp = temp + getInboxString((InboxData) Constant.mListInbox.elementAt(i));
			}else{
				temp = temp + "**" + getInboxString((InboxData) Constant.mListInbox.elementAt(i));
			}
		}
		deleteTextFile(Constant.PATH_INBOX+"_"+Constant.mPersistData.getUsername());
		writeTextFile(Constant.PATH_INBOX+"_"+Constant.mPersistData.getUsername(), temp);
		System.out.println(">> Inbox saved: "+temp);
    }
    
    public static String getInboxString(InboxData data){
    	return data.getTrxID()+"##"+Long.toString(data.getDate())+"##"+data.getAmount()+"##"+data.getTo()+"##"+data.getAdditional()
        		+"##"+data.getMenu()+"##"+data.getCategory()+"##"+data.getCategoryID()+"##"+data.getMerchant()+"##"+data.getMerchantID();
    }
    
    public static void deleteInboxData(String date){
        if(Constant.mHashMapInbox.containsKey(date)){
            Constant.mHashMapInbox.remove(date);
            Constant.mListInbox.removeAllElements();
            
            Enumeration e = Constant.mHashMapInbox.keys();
            while (e.hasMoreElements()) {
            	String key = (String) e.nextElement();
            	Constant.mListInbox.addElement(Constant.mHashMapInbox.get(key));
            }
            saveInboxData();
        }else{
        	Dialog.alert("Inbox Item tidak ditemukan");
        }
    }
}
