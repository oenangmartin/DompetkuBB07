package com.indosatapps.dompetku.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import net.rim.device.api.crypto.BlockDecryptor;
import net.rim.device.api.crypto.BlockEncryptor;
import net.rim.device.api.crypto.CryptoException;
import net.rim.device.api.crypto.PKCS5FormatterEngine;
import net.rim.device.api.crypto.PKCS5UnformatterEngine;
import net.rim.device.api.crypto.SHA1Digest;
import net.rim.device.api.crypto.TripleDESDecryptorEngine;
import net.rim.device.api.crypto.TripleDESEncryptorEngine;
import net.rim.device.api.crypto.TripleDESKey;
import net.rim.device.api.io.Base64InputStream;
import net.rim.device.api.io.Base64OutputStream;
import net.rim.device.api.util.Arrays;
import net.rim.device.api.util.DataBuffer;

public class TripleDES {
	//private static String KEY = "EL_@51t4s_D0mp3Tku__REqV";   //production
    private static String KEY = "Th1s_0nLy_F0uR_t3sT1nG__";     //development
    
    public static byte[] encrypt(String message) throws CryptoException, IOException {
    	TripleDESKey key = new TripleDESKey(KEY.getBytes());
    	TripleDESEncryptorEngine engine = new TripleDESEncryptorEngine(key);
    	PKCS5FormatterEngine fengine = new PKCS5FormatterEngine(engine);
    	ByteArrayOutputStream output = new ByteArrayOutputStream();
        BlockEncryptor encryptor = new BlockEncryptor( fengine, output );
        //SHA1Digest digest = new SHA1Digest();
        //digest.update( message.getBytes() );
        //byte[] hash = digest.getDigest();
        encryptor.write( message.getBytes() );
        //encryptor.write( hash );
        encryptor.close();
        output.close();
        byte[] temp = output.toByteArray();
        return Base64OutputStream.encode(temp, 0, temp.length, false, false);
        
        /*SecretKeySpec keyspec = new SecretKeySpec(KEY.getBytes(), "DESede");
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        
        byte[] encrypted = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, keyspec);
            encrypted = cipher.doFinal(message.getBytes());
        } catch (Exception e){                       
            throw new Exception("[encrypt] " + e.getMessage());
        }
        
        return Base64.encode(encrypted, Base64.DEFAULT);*/
    }

    public static String decrypt(String message) throws CryptoException, IOException {
    	byte[] chiper = Base64InputStream.decode(message);
    	TripleDESKey key = new TripleDESKey(KEY.getBytes());
    	TripleDESDecryptorEngine engine = new TripleDESDecryptorEngine(key);
    	PKCS5UnformatterEngine uengine = new PKCS5UnformatterEngine(engine);
    	ByteArrayInputStream input = new ByteArrayInputStream( chiper );
        BlockDecryptor decryptor = new BlockDecryptor( uengine, input );
        
        byte[] temp = new byte[ 100 ];
        DataBuffer buffer = new DataBuffer();

        for( ;; ) {
            int bytesRead = decryptor.read( temp );
            buffer.write( temp, 0, bytesRead );
            if( bytesRead < 100 ) {
                // We ran out of data.
                break;
            }
        }

        byte[] plaintextAndHash = buffer.getArray();
        int plaintextLength = plaintextAndHash.length - SHA1Digest.DIGEST_LENGTH;
        byte[] plaintext = new byte[ plaintextLength ];
        byte[] hash = new byte[ SHA1Digest.DIGEST_LENGTH ];

        System.arraycopy( plaintextAndHash, 0, plaintext, 0, plaintextLength );
        System.arraycopy( plaintextAndHash, plaintextLength, hash, 0, SHA1Digest.DIGEST_LENGTH );

        // Now, hash the plaintext and compare against the hash
        // that we found in the decrypted data.
        SHA1Digest digest = new SHA1Digest();
        digest.update( plaintext );
        byte[] hash2 = digest.getDigest();

        /*if( !Arrays.equals( hash, hash2 )) {
            throw new RuntimeException();
        }*/

        return new String(plaintext, "UTF-8");
    	
        /*byte[] code = Base64.decode(message, Base64.DEFAULT);
        //IvParameterSpec ivspec = new IvParameterSpec(new byte[8]);
        SecretKeySpec keyspec = new SecretKeySpec(KEY.getBytes(), "DESede");
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        
        byte[] decrypted = null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, keyspec);
            decrypted = cipher.doFinal(code);
        } catch (Exception e){
            throw new Exception("[decrypt] " + e.getMessage());
        }
        return new String(decrypted, "UTF-8");*/
    }
    
}