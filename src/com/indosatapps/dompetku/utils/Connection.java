package com.indosatapps.dompetku.utils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.io.HttpsConnection;

public class Connection {
    private final static String CONTENT_TYPE_FORM_POST="application/x-www-form-urlencoded";
    
    private static String END_POINT_ELA = "http://fb.elasitas.com/jason/api_dompetku/";       //development
    //private static String END_POINT_ELA = "https://mapi.dompetku.com:8300/";         //production
    private static String TOKEN = "204a217e5873d4a00868f3224cebd607";
    private static String URL_GET_CATEGORY_MERCHANT_LIST = END_POINT_ELA+"api_c/list_merchant/"+TOKEN;
    private static String URL_GET_FIELD_LIST = END_POINT_ELA+"api_c/list_field/"+TOKEN+"/";
    private static String URL_GET_PROMO_LIST = END_POINT_ELA+"api_c/list_promo/"+TOKEN+"/";
    private static String URL_GET_MERCHANT_LIST = END_POINT_ELA+"api_c/list_listmerchant/"+TOKEN+"/";
            
    private static String END_POINT = "http://114.4.68.110:14567/mfsmw/";       //development
    //private static String END_POINT = "https://mapi.dompetku.com/";         //production
    private static String URL_LOGIN = END_POINT+"webapi/login";
    private static String URL_CHECK_BALANCE = END_POINT+"webapi/balance_check";
    private static String URL_HISTORY_TRX = END_POINT+"webapi/history_transaction";
    private static String URL_TRANSFER_DOMPETKU = END_POINT+"webapi/money_transfer";
    private static String URL_TRANSFER_BANK = END_POINT+"webapi/bank_transfer";
    private static String URL_TRANSFER_OP_LAIN = END_POINT+"webapi/p2p_transfer";
    private static String URL_ACCOUNT_INQUIRY = END_POINT+"webapi/user_inquiry";
    private static String URL_GENERATE_TOKEN = END_POINT+"webapi/create_coupon";
    private static String URL_REGISTER = END_POINT+"webapi/register";
    private static String URL_GANTI_PIN = END_POINT+"webapi/change_pin";
    //private static String URL_OTP = END_POINT_ELA+"otp/";
    private static String URL_OTP = "http://114.4.68.110/otp/";
    
    private static String URL_QRCODE_USER_KEY = "http://114.4.68.110/flashiz/apis/signon.php";
    private static String URL_QRCODE_INQUIRY = "http://114.4.68.110/flashiz/apis/inquiry.php";
    private static String URL_QRCODE_PAYMENT = "http://114.4.68.110/flashiz/apis/payment.php";
    
    public static String getCategoryMerchantList() {
        BasicUtils.saveLastSession();
        return getHttpData(URL_GET_CATEGORY_MERCHANT_LIST, "".getBytes());
    }
    
    public static String getFieldList(String merchantID) {
        System.out.println(">> getFieldList "+merchantID);
        BasicUtils.saveLastSession();
        String url = URL_GET_FIELD_LIST+merchantID;
        return getHttpData(url, "".getBytes());
    }
    
    public static String getMerchantList(String limit, String page) {
    	BasicUtils.saveLastSession();
        String url = URL_GET_MERCHANT_LIST+limit+"/"+page;
        return getHttpData(url, "".getBytes());
    }
    
    public static String getPromoList() {
    	BasicUtils.saveLastSession();
        return getHttpData(URL_GET_PROMO_LIST, "".getBytes());
    }
    
    public static String doRequestOTP(String msisdn) {
        String param = "?mode="+URLUTF8Encoder.encode("request")+
                    "&msisdn="+URLUTF8Encoder.encode(msisdn);
        System.out.println(">> doRequestOTP "+param);
        return getHttpData(URL_OTP+param, "".getBytes());
    }
    
    public static String doConfirmOTP(String msisdn, String code) {
        String param = "?mode="+URLUTF8Encoder.encode("confirm")+
                    "&msisdn="+URLUTF8Encoder.encode(msisdn)+
                    "&code="+URLUTF8Encoder.encode(code);
        System.out.println(">> doConfirmOTP "+param);
        return getHttpData(URL_OTP+param, "".getBytes());
    }
	
    public static String doRegister(String email, String pin, String msisdn, String firstname, String lastname, String dob, String gender, String motherName, String addr, String idType, String idNo) {
        String param = "signature="+URLUTF8Encoder.encode(BasicUtils.createSignature(msisdn, pin))+
                    "&userid="+URLUTF8Encoder.encode("web_api_test")+
                    "&firstname="+URLUTF8Encoder.encode(firstname)+
                    "&lastname="+URLUTF8Encoder.encode(lastname)+
                    "&dob="+URLUTF8Encoder.encode(dob)+
                    "&gender="+URLUTF8Encoder.encode(gender)+
                    "&mothername="+URLUTF8Encoder.encode(motherName)+
                    "&address="+URLUTF8Encoder.encode(addr)+
                    "&idtype="+URLUTF8Encoder.encode(idType)+
                    "&idnumber="+URLUTF8Encoder.encode(idNo)+
                    "&msisdn="+URLUTF8Encoder.encode(msisdn);
        System.out.println("doRegister "+param);
        return getHttpData(URL_REGISTER, param.getBytes());
    }
    
    public static String doLogin(String username, String pin) {
        String param = "signature="+URLUTF8Encoder.encode(BasicUtils.createSignature(username, pin))+
                    "&userid="+URLUTF8Encoder.encode("web_api_test");
        System.out.println("doLogin "+param);
        return getHttpData(URL_LOGIN, param.getBytes());
    }
    
    public static String doCheckBalance(String username) {
        String param = "to="+URLUTF8Encoder.encode(username)+
                    "&userid="+URLUTF8Encoder.encode("web_api_test");
        System.out.println("doCheckBalance "+param);
        
        BasicUtils.saveLastSession();
        return getHttpData(URL_CHECK_BALANCE, param.getBytes());
    }
    
    public static String doTransaction(String username, String pin, String url, String param) {
        param = param+"&signature="+URLUTF8Encoder.encode(BasicUtils.createSignature(username, pin));
        System.out.println("doTransaction "+url+" "+param);
        
        BasicUtils.saveLastSession();
        return getHttpData(url, param.getBytes());
    }

    public static String doGetHistoryTrx(String username, String count) {
        String param = "userid="+URLUTF8Encoder.encode("web_api_test")+
                    "&to="+URLUTF8Encoder.encode(username)+
                    "&count="+URLUTF8Encoder.encode(count);
        System.out.println("doGetHistoryTrx "+param);
        
        BasicUtils.saveLastSession();
        return getHttpData(URL_HISTORY_TRX, param.getBytes());
    }
    
    public static String doAccountInquiry(String username, String pin, String account) {
        String param = "signature="+URLUTF8Encoder.encode(BasicUtils.createSignature(username, pin))+
                    "&userid="+URLUTF8Encoder.encode("web_api_test")+
                    "&to="+URLUTF8Encoder.encode(account);
        System.out.println("doAccountInquiry "+param);
        
        BasicUtils.saveLastSession();
        return getHttpData(URL_ACCOUNT_INQUIRY, param.getBytes());
    }
    
    public static String doGenerateToken(String username, String pin) {
        String param = "signature="+URLUTF8Encoder.encode(BasicUtils.createSignature(username, pin))+
                    "&userid="+URLUTF8Encoder.encode("web_api_test");
        System.out.println("doGenerateToken "+param);
        
        BasicUtils.saveLastSession();
        return getHttpData(URL_GENERATE_TOKEN, param.getBytes());
    }
    
    public static String doTransferDompetku(String username, String pin, String to, String amount) {
        String param = "signature="+URLUTF8Encoder.encode(BasicUtils.createSignature(username, pin))+
                    "&userid="+URLUTF8Encoder.encode("web_api_test")+
                    "&to="+URLUTF8Encoder.encode(to)+
                    "&amount="+URLUTF8Encoder.encode(amount);
        System.out.println("doTransferDompetku "+param);
        
        BasicUtils.saveLastSession();
        return getHttpData(URL_TRANSFER_DOMPETKU, param.getBytes());
    }
    
    public static String doTransferOperatorLain(String username, String pin, String to, String amount) {
        String param = "signature="+URLUTF8Encoder.encode(BasicUtils.createSignature(username, pin))+
                    "&userid="+URLUTF8Encoder.encode("web_api_test")+
                    "&to="+URLUTF8Encoder.encode(to)+
                    "&amount="+URLUTF8Encoder.encode(amount);
        System.out.println("doTransferOperatorLain "+param);
        
        BasicUtils.saveLastSession();
        return getHttpData(URL_TRANSFER_OP_LAIN, param.getBytes());
    }
    
    public static String doTransferBank(String username, String pin, String bankCode, String bankAccNo, String amount) {
    	String param = "signature="+URLUTF8Encoder.encode(BasicUtils.createSignature(username, pin))+
		        "&userid="+URLUTF8Encoder.encode("web_api_test")+
		        "&bankCode="+URLUTF8Encoder.encode(bankCode)+
		        "&bankAccNo="+URLUTF8Encoder.encode(bankAccNo)+
		        "&amount="+URLUTF8Encoder.encode(amount);
		System.out.println("doTransferBank "+param);
		
		BasicUtils.saveLastSession();
        return getHttpData(URL_TRANSFER_BANK, param.getBytes());
    }
    
    public static String doChangePIN(String username, String oldPIN, String newPIN) {
        String param = "signature="+URLUTF8Encoder.encode(BasicUtils.createSignature(username, oldPIN))+
                    "&userid="+URLUTF8Encoder.encode("web_api_test")+
                    "&pin="+URLUTF8Encoder.encode(newPIN);
        System.out.println("doChangePIN "+param);
        
        BasicUtils.saveLastSession();
        return getHttpData(URL_GANTI_PIN, param.getBytes());
    }
    
	public static String getHttpData(String url, byte[] postData){
		BlackberryUtils.setBlackBerryConnectionParams();
		byte[] data=null;
		if(BlackberryUtils.currentConnection==-1){
            return "false";
        }else{
			try{
				url+=BlackberryUtils.currentConnectionString;
				System.out.println("URL: "+url);
				
				HttpConnection conn=(HttpConnection)Connector.open(url);
				conn.setRequestMethod(HttpConnection.POST);
				conn.setRequestProperty("Content-Type", CONTENT_TYPE_FORM_POST);
	            conn.setRequestProperty("Content-length", String.valueOf(postData.length));
	            
				OutputStream os = null;
				os = conn.openOutputStream();
	            os.write(postData);
	            os.flush();
	            
				InputStream is=conn.openInputStream();
				
				ByteArrayOutputStream baos=new ByteArrayOutputStream();
				byte[] buffer=new byte[1024];
				int size;
				while((size=is.read(buffer,0,buffer.length))!=-1){
					baos.write(buffer, 0, size);
				}
				baos.flush();
				data=baos.toByteArray();
				
				int resCode = conn.getResponseCode();
				System.out.println("Conn response code: "+resCode);
				if(resCode!=200){
					data = "false".getBytes();
				}
				
				baos.close();
				is.close();
				conn.close();
				
			}catch(Exception e){
				System.out.println("error http nih:"+e.getMessage());
				data="false".getBytes();
			}
			
			System.out.println("Conn response: "+ new String(data));
			
			return new String(data);
        }
	}
	
	public static String getHttpsData(String url, byte[] postData){
		BlackberryUtils.setBlackBerryConnectionParams();
		byte[] data=null;
		if(BlackberryUtils.currentConnection==-1){
            return "false";
        }else{
			try{
				url+=BlackberryUtils.currentConnectionString;
				url+=";EndToEndDesired";
				System.out.println("URL: "+url);
				
				HttpsConnection conn=(HttpsConnection)Connector.open(url);
				conn.setRequestMethod(HttpConnection.POST);
				conn.setRequestProperty("Content-Type", CONTENT_TYPE_FORM_POST);
	            conn.setRequestProperty("Content-length", String.valueOf(postData.length));
	            
				OutputStream os = null;
				os = conn.openOutputStream();
	            os.write(postData);
	            os.flush();
	            
				InputStream is=conn.openInputStream();
				
				ByteArrayOutputStream baos=new ByteArrayOutputStream();
				byte[] buffer=new byte[1024];
				int size;
				while((size=is.read(buffer,0,buffer.length))!=-1){
					baos.write(buffer, 0, size);
				}
				baos.flush();
				data=baos.toByteArray();
				
				int resCode = conn.getResponseCode();
				System.out.println("Conn response code: "+resCode);
				if(resCode!=200){
					data = "false".getBytes();
				}
				
				baos.close();
				is.close();
				conn.close();
				
			}catch(Exception e){
				System.out.println("error http nih:"+e.getMessage());
				data="false".getBytes();
			}
			
			System.out.println("Conn response: "+ new String(data));
			return new String(data);
        }
	}
}
