package com.indosatapps.dompetku.utils;


import java.util.Enumeration;
import java.util.Hashtable;

import net.rim.device.api.browser.field.ContentReadEvent;

/**
 * BrowserFieldLoadProgressTracker 
 * - Keeps track of browser field page-load progress 
 * 
 * Challenge:
 * - Number of resources (e.g., images, css, javascript) to load is not know beforehand
 * 
 */
public class BrowserFieldProgressTracker {
 
	 // The fraction used to split the remaining load amount in the progress bar after a new resource is found (e.g., 2=half, 3=one third, 5=one fifth)
	 // Bar progress typically moves fast at the beginning and slows down progressively as we don't know how many resources still need to be loaded
	 private float progressFactor;
	 
	 // The percentage value left until the progress bar is fully filled out (initial value=1 or 100%) 
	 private float percentageLeft;     
	 
	 // Stores info about the resources being loaded
	 private Hashtable resourcesTable;    // Map: Resource (Connection) ---> ProgressTrackerEntry 
	 
	 // Stores info about a resource being loaded
	 static class ProgressTrackerEntry {
	     ProgressTrackerEntry(int bytesRead, int bytesToRead, float percentage) {
	         this.bytesRead = bytesRead;
	         this.bytesToRead = bytesToRead;
	         this.percentage = percentage;
	     }
	     int bytesRead;     // bytes read so far for this resource
	     int bytesToRead;   // total number of bytes that need to be read for this resource
	     float percentage;  // the amount (in percentage) this resource represents in the progress bar (e.g., 50%, 25%, 12.5%, 6.25%)
	     
	     public void updateBytesRead(int bytesRead) {
	         bytesRead += bytesRead;
	         if ( bytesRead > bytesToRead ) {  // this can happen when the final size of a resource cannot be anticipated 
	             bytesToRead = bytesRead;
	         }
	     }
	 }
	 
	 public BrowserFieldProgressTracker(float progressFactor) {
	     this.progressFactor = progressFactor;
	     reset();
	 }
	 
	 public synchronized void reset() {
	     resourcesTable = null;
	     percentageLeft = 1f;
	 }
	 	 
	 public synchronized float updateProgress(ContentReadEvent event) {
		
		 if ( resourcesTable == null ) {
	         resourcesTable = new Hashtable();
	     }
	
	     Object resourceBeingLoaded = event.getSource();
	
	     ProgressTrackerEntry entry = (ProgressTrackerEntry)resourcesTable.get(resourceBeingLoaded);
	     if ( entry == null ) {
	         float progressPercentage = percentageLeft / progressFactor;
	         percentageLeft -= progressPercentage;
	         resourcesTable.put( resourceBeingLoaded, new ProgressTrackerEntry(event.getItemsRead(), event.getItemsToRead(), progressPercentage ) );
	     }
	     else {
	         entry.updateBytesRead( event.getItemsRead() );
	     }
	
	     return getProgressPercentage();
	 }
	    	    
	 /**
	 * Returns the amount of items read so far in percentage so that the progress bar can be updated.
	 * @return the amount of items read so far in percentage (0.0-1.0)
	 */
	 public synchronized float getProgressPercentage() {
	     float percentage = 0f;
	     for( Enumeration e = resourcesTable.elements() ; e.hasMoreElements() ; ) {
	         ProgressTrackerEntry entry = (ProgressTrackerEntry)e.nextElement();
	         percentage += ((entry.bytesRead/entry.bytesToRead)*entry.percentage);
	     }        
	     return percentage;
	 }    

}

