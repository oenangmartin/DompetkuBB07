package com.indosatapps.dompetku.utils;

public interface PageResultListener {

	public void onPageResult(int requestCode, int resultCode, Object data);
}
