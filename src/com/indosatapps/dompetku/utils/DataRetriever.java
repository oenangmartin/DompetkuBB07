package com.indosatapps.dompetku.utils;

import java.util.Vector;

public class DataRetriever implements Runnable
{	
	private Thread thread;
	private Vector processors;
	
	public DataRetriever(){
		processors=new Vector();
		thread=null;
	}
	
	public void addRequest(DataProcessor processor){
		processors.addElement(processor);
		if(thread==null){
			thread=new Thread(this);
			thread.start();
		}
	}

	public void run() {
		
		while(processors.size()!=0){
			DataProcessor processor=(DataProcessor)processors.elementAt(0);
			//tarik Data
			byte[] data=null;
			String url = processor.getUrl();
			data = BasicUtils.getHttpData(url);
			processor.processData(data);
			
			processors.removeElementAt(0);
		}
		
		thread=null;
	}

}
