package com.indosatapps.dompetku.utils;

public interface DataProcessor {
	public String getUrl();
	public void processData(byte[] data);

}
