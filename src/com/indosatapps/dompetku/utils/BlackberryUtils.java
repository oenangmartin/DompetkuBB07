package com.indosatapps.dompetku.utils;

import java.io.IOException;

import javax.microedition.io.Connection;
import javax.microedition.io.Connector;
import javax.wireless.messaging.Message;
import javax.wireless.messaging.MessageConnection;
import javax.wireless.messaging.TextMessage;

import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.invoke.Invoke;
import net.rim.blackberry.api.invoke.MapsArguments;
import net.rim.blackberry.api.invoke.MessageArguments;
import net.rim.blackberry.api.invoke.PhoneArguments;
import net.rim.blackberry.api.maps.MapView;
import net.rim.blackberry.api.messagelist.ApplicationIcon;
import net.rim.blackberry.api.messagelist.ApplicationIndicator;
import net.rim.blackberry.api.messagelist.ApplicationIndicatorRegistry;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Clipboard;
import net.rim.device.api.system.CoverageInfo;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.EncodedImage;
import net.rim.device.api.system.WLANInfo;

import net.rim.device.api.servicebook.ServiceBook;
import net.rim.device.api.servicebook.ServiceRecord;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;


public class BlackberryUtils{
	
	private static boolean isNoInternet=false;

	/**
	 * Get PIN number of current device.
	 * 
	 * @return PIN number
	 */
    public static String getPIN(){
        return Integer.toHexString(DeviceInfo.getDeviceId()).toUpperCase();
    }

    /**
     * Copy object to clipboard.
     * 
     * @param o object
     */
    public static void copyToClipboard(Object o){
        Clipboard clipboard = Clipboard.getClipboard();
        clipboard.put(o);
    }

    /**
     * Open the Message application on device.
     * 
     * @param message the message to send
     */
    public static void invokeNativeMessaging(String message){
        try {
            /*MessageConnection conn = (MessageConnection)Connector.open("sms://:0");//+contact.getString(Contact.TEL,0));
            TextMessage text = (TextMessage)conn.newMessage(MessageConnection.TEXT_MESSAGE);
            text.setPayloadText(message);*/
            copyToClipboard(message);
            Invoke.invokeApplication(Invoke.APP_TYPE_MESSAGES, new MessageArguments(MessageArguments.ARG_NEW_SMS));//text));
            //conn.close();
        } catch (Exception e) {//IOException e) {
            //Dialog.alert("Exception: " + e);
            System.out.println(e.getMessage());
        }
    }
    
    public static void invokePhone(String numbers){
    	try{
    		Invoke.invokeApplication(Invoke.APP_TYPE_PHONE, new PhoneArguments (PhoneArguments.ARG_CALL, numbers));
    	}catch (Exception e) {            
            System.out.println(e.getMessage());
        }
    }
    
    public static void invokeMap(double longitude,double latitude,int zoom){
    	MapView mapView = new MapView();
        mapView.setLatitude((int)(latitude*100000));
        mapView.setLongitude((int)(longitude*100000));
        mapView.setZoom(zoom);
        MapsArguments mapsArgs = new MapsArguments(mapView);
        Invoke.invokeApplication(Invoke.APP_TYPE_MAPS, mapsArgs);
    }
    public static void invokeMap(double longitude,double latitude,String label,String description){
    	String document="<lbs><location y='"+((int)(latitude*100000))+"' x='"+((int)(longitude*100000))+"' label='"+label+"' description='"+description+"'/></lbs>";
    	Invoke.invokeApplication(Invoke.APP_TYPE_MAPS, new MapsArguments( MapsArguments.ARG_LOCATION_DOCUMENT, document));
    }

    public static void changeApplicationIcon(String newIcon){

        Bitmap bm = Bitmap.getBitmapResource(newIcon);
        net.rim.blackberry.api.homescreen.HomeScreen.updateIcon(bm, 1);
    }

    public static void setApplicationIndicator(String indicator,boolean visible){
        ApplicationIndicatorRegistry reg = ApplicationIndicatorRegistry.getInstance();
        EncodedImage mImage = EncodedImage.getEncodedImageResource(indicator);
        ApplicationIcon mIcon = new ApplicationIcon(mImage);
        if (reg.getApplicationIndicator()==null) {
            reg.register(mIcon, true, false);
            System.out.println("App Indicator registered!");
        }
        ApplicationIndicator appIndicator = reg.getApplicationIndicator();
        appIndicator.setVisible(visible);
        System.out.println("App Indicator set visible: "+visible);
    }
    
    public static boolean invokeBrowser(String url){
    	boolean retval = true;
    	
    	Browser.getDefaultSession().displayPage(url);

    	/*int handle =  CodeModuleManager.getModuleHandle("net_rim_bb_browser_daemon");

    	if (handle <=0 ) {
    		retval = false;
	    }else {
    	    ApplicationDescriptor[] browserDescriptors =  CodeModuleManager.getApplicationDescriptors(handle);

    	    if (browserDescriptors == null ) {
    	    	retval = false;
	        }else{
	        	if ( browserDescriptors.length <=0 ) {
	        		retval = false;
	            } else {
	                String[] args = {"url", url};
	
	                ApplicationDescriptor descriptor = new ApplicationDescriptor
	                   (
	                       browserDescriptors[0],"url invocation", 
	                       args,-1, null, -1,
	                       ApplicationDescriptor.FLAG_SYSTEM
	                   );
	
	                try{
	                       ApplicationManager.getApplicationManager().runApplication(descriptor);
	                }catch(ApplicationManagerException e){
	                       retval = false;
	                }
	            }
	        }
    	}*/
    	return retval;
    }

    public static final String WIFI_CONNECTION_STRING=";interface=wifi";
    public static final String BIS_CONNECTION_STRING=";deviceside=false;ConnectionType=mds-public";
    public static String WAP_CONNECTION_STRING="";
    public static final int WIFI_CONN=0;
    public static final int BIS_CONN=1;
    public static final int GPRS_CONN=2;
    public static boolean[] connectionStatus={false,false,false};
    public static int currentConnection=-1;
    public static String currentConnectionString="";

    public static String getConnectionString(int connectionType){
        if(connectionType==BIS_CONN){
            return BIS_CONNECTION_STRING;
        }else if(connectionType==WIFI_CONN){
            return WIFI_CONNECTION_STRING;
        }else{
            return getWAPConnectionString();
        }
    }

    public static String getWAPConnectionString(){


        if(WAP_CONNECTION_STRING.equals("")){
            if(net.rim.device.api.system.DeviceInfo.isSimulator()){
                WAP_CONNECTION_STRING=";deviceside=true";
            }else{
                ServiceBook sb = ServiceBook.getSB();
                ServiceRecord[] records = sb.findRecordsByCid("WPTCP");

                for(int i=0; i < records.length; i++)
                {
                    //Search through all service records to find the
                    //valid non-Wi-Fi and non-MMS
                    //WAP 2.0 Gateway Service Record.
                    if (records[i].isValid() && !records[i].isDisabled()) {

                        if (records[i].getUid() != null && records[i].getUid().length() != 0)
                        {
                            if ((records[i].getUid().toLowerCase().indexOf("wifi") == -1) &&
                            (records[i].getUid().toLowerCase().indexOf("mms") == -1))
                            {
                                WAP_CONNECTION_STRING = ";deviceside=true;ConnectionUID="+records[i].getUid();
                                break;
                            }
                        }
                    }
                }
            }
            System.out.println("gprs conn:'"+WAP_CONNECTION_STRING+"'");
        }
        return WAP_CONNECTION_STRING;
    }

    public static void setHttpConnection(int connectionType){

        connectionStatus[connectionType]=true;

        /*if(currentConnection!=-1)
            return;*/
        currentConnection=connectionType;
        currentConnectionString=getConnectionString(connectionType);
        isNoInternet=false;

        System.out.println("setHttpConnection: "+currentConnectionString);
    }
/*
    public static boolean checkHttpConnection(String url,int connectionType){
        try {
            HttpConnection con = null;
            connectionStatus[connectionType]=false;

            String connString=BlackberryUtils.getConnectionString(connectionType);
            url+=connString;
            if(connectionType==GPRS_CONN)
                System.out.println("start checking:"+url);

            con = (HttpConnection) Connector.open(url, Connector.READ_WRITE, true);

            byte[] data = Utils.getByteArrayFromHttpConnection(con);

            con.close();
            setHttpConnection(connectionType);
        } catch (Exception ex) {
            if(connectionType==GPRS_CONN)
                System.out.println("failed checking:"+url);
            return false;
        }
        return true;
    }

    public static void recheckHTTPConnection(){
        int curr=currentConnection;
        do{
            curr=(curr+1)%3;
            if(connectionStatus[curr]){
                currentConnection=curr;
                currentConnectionString=getConnectionString(curr);
            }
        }while(curr!=currentConnection);
    }
*/    
    public static final String getBlackBerryConnectionParams() {
        String connParams = "";
        //
        if(net.rim.device.api.system.DeviceInfo.isSimulator()){
        	connParams=";deviceside=true";
        }else if (WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED) {
            connParams = ";interface=wifi"; //Connected to a WiFi access point.
        } else {
            int coverageStatus = CoverageInfo.getCoverageStatus();
            ServiceRecord record = getWAP2ServiceRecord();
            //
            if (record != null && (coverageStatus & CoverageInfo.COVERAGE_DIRECT) == CoverageInfo.COVERAGE_DIRECT) {
                    // Have network coverage and a WAP 2.0 service book record
                    connParams = ";deviceside=true;ConnectionUID=" + record.getUid();
            } else if ((coverageStatus & CoverageInfo.COVERAGE_MDS) == CoverageInfo.COVERAGE_MDS) {
                    // Have an MDS service book and network coverage
                    connParams = ";deviceside=false";
            } else if ((coverageStatus & CoverageInfo.COVERAGE_DIRECT) == CoverageInfo.COVERAGE_DIRECT) {
                    // Have network coverage but no WAP 2.0 service book record
                    connParams = ";deviceside=true";
            } else if ((coverageStatus & CoverageInfo.COVERAGE_BIS_B) == CoverageInfo.COVERAGE_BIS_B) {
                    connParams = ";deviceside=false;ConnectionType=mds-public";
            }
        }
        //
        return connParams;
    }
    public static final void setBlackBerryConnectionParams() {
        //
    	if(net.rim.device.api.system.DeviceInfo.isSimulator()){
    		setHttpConnection(GPRS_CONN);
        }else if (WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED) {            
            setHttpConnection(WIFI_CONN);
        } else {
            int coverageStatus = CoverageInfo.getCoverageStatus();
            ServiceRecord record = getWAP2ServiceRecord();
            //
            if ((coverageStatus & CoverageInfo.COVERAGE_BIS_B) == CoverageInfo.COVERAGE_BIS_B) {
                setHttpConnection(BIS_CONN);    
            }else if (record != null && (coverageStatus & CoverageInfo.COVERAGE_DIRECT) == CoverageInfo.COVERAGE_DIRECT) {
                    // Have network coverage and a WAP 2.0 service book record
            	setHttpConnection(GPRS_CONN);
            } /*else if ((coverageStatus & CoverageInfo.COVERAGE_MDS) == CoverageInfo.COVERAGE_MDS) {
                    // Have an MDS service book and network coverage
                    connParams = ";deviceside=false";
            } else if ((coverageStatus & CoverageInfo.COVERAGE_DIRECT) == CoverageInfo.COVERAGE_DIRECT) {
                    // Have network coverage but no WAP 2.0 service book record
                    connParams = ";deviceside=true";
            } */ else{
            	if(!isNoInternet){
	            	isNoInternet=true;
	            	currentConnection=-1;
//	            	UiApplication.getApplication().invokeLater(new Runnable(){
//						public void run() {
//							Dialog.alert("No Internet Connection available!!\n Please check your Internet Connection");						
//						}
//	            	});
            	}
            }
        }
        
    }
    private static final ServiceRecord getWAP2ServiceRecord() {
        String cid;
        String uid;
            ServiceBook sb = ServiceBook.getSB();
        ServiceRecord[] records = sb.getRecords();
        //
        for (int i = records.length -1; i >= 0; i--) {
            cid = records[i].getCid().toLowerCase();
            uid = records[i].getUid().toLowerCase();
            //
            if (cid.indexOf("wptcp") != -1 &&
                            uid.indexOf("wifi") == -1 &&
                            uid.indexOf("mms") == -1) {
                return records[i];
            }
        }
        //
        return null;
    }    
    
    public static void sendSMS(final String number, final String message){
    	Thread t = new Thread(new Runnable() {
			public void run() {
		    	MessageConnection msgConn = null;
		    	try{
			    	msgConn = (MessageConnection)Connector.open("sms://"+number);
			    	Message msg = msgConn.newMessage(MessageConnection.TEXT_MESSAGE);
			    	TextMessage txtMsg = (TextMessage)msg;
			    	txtMsg.setPayloadText(message);
			    	msgConn.send(txtMsg);
		    	}catch (Exception e) {
					System.out.println(">> Send SMS error: "+e.getMessage());
					e.printStackTrace();
				}
			}
    	});
    	t.start();
    }
}
