package com.indosatapps.dompetku;

import net.rim.device.api.browser.field.ContentReadEvent;
import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.browser.field2.BrowserFieldConfig;
import net.rim.device.api.browser.field2.BrowserFieldListener;
import net.rim.device.api.script.ScriptEngine;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.GaugeField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import org.w3c.dom.Document;

import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.utils.BrowserFieldProgressTracker;

public class WebView extends MainScreen{
	Mediator mediator;
	GaugeField progressBar;
	BrowserFieldProgressTracker progressTracker;
	VerticalFieldManager footer;
	
	public WebView(String url, Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		HeaderField header = new HeaderField("Promo", 0);
		add(header);
		
		footer = new VerticalFieldManager(USE_ALL_WIDTH);
    	footer.add(new LabelField(""));
    	
    	setStatus(footer);
    	
		progressBar = new GaugeField("Loading...", 0, 100, 0, GaugeField.LABEL_AS_PROGRESS);
		progressTracker = new BrowserFieldProgressTracker(10f);
		
		BrowserFieldConfig myBrowserFieldConfig = new BrowserFieldConfig();
        myBrowserFieldConfig.setProperty(BrowserFieldConfig.NAVIGATION_MODE, BrowserFieldConfig.NAVIGATION_MODE_POINTER);
        BrowserField browserField = new BrowserField(myBrowserFieldConfig);
        BrowserFieldListener listener = new BrowserFieldListener() {
        	public void documentError(BrowserField browserField, Document document) throws Exception {
        		System.out.println("Document Error: "+document.getDocumentURI());
        		if(footer.getFieldCount()>1){
        			UiApplication.getUiApplication().invokeLater(new Runnable() {
    					public void run() {
    						progressTracker.reset();
		        			footer.delete(progressBar);
		        			progressBar.setValue(0);
    					}
        			});
        		}
        		super.documentError(browserField, document);
        	}
        	public void documentAborted(BrowserField browserField, Document document) throws Exception {
        		System.out.println("Document Aborted: "+document.getDocumentURI());
        		UiApplication.getUiApplication().invokeLater(new Runnable() {
					public void run() {
						progressTracker.reset();
						progressBar.reset("Loading...", 0, 100, 0);
					}
        		});
        		super.documentAborted(browserField, document);
        	}
        	public void documentCreated(BrowserField browserField, ScriptEngine scriptEngine, Document document)
        			throws Exception {
        		System.out.println("Document Created: "+document.getDocumentURI());
        		
        		if(footer.getFieldCount()==1){
        			UiApplication.getUiApplication().invokeLater(new Runnable() {
    					public void run() {
    						footer.insert(progressBar, 0);
    						progressBar.reset("Loading...", 0, 100, 0);
    					}
        			});
        		}
        		super.documentCreated(browserField, scriptEngine, document);
        	}
        	public void documentLoaded(BrowserField browserField, Document document) throws Exception {
        		System.out.println("Document Loaded: "+document.getDocumentURI());
        		if(footer.getFieldCount()>1){
        			UiApplication.getUiApplication().invokeLater(new Runnable() {
    					public void run() {
    						progressTracker.reset();
    		                progressBar.setValue(100);
		        			footer.delete(progressBar);
    					}
        			});
        		}
        		super.documentLoaded(browserField, document);
        	}
        	public void downloadProgress(BrowserField browserField, final ContentReadEvent event) throws Exception {
        		System.out.println("Download Progress");
        		UiApplication.getUiApplication().invokeLater(new Runnable() {
					public void run() {
						int value = (int)(100*progressTracker.updateProgress(event));
						progressBar.setValue(value);
					}
				});
        		
        		super.downloadProgress(browserField, event);
        	}
        	public void documentUnloading(BrowserField browserField, Document document) throws Exception {
        		System.out.println("Document Unloading: "+document.getDocumentURI());
        		super.documentUnloading(browserField, document);
        	}
		};
        browserField.addListener(listener);
        add(browserField);
        browserField.requestContent(url);
	}
}
