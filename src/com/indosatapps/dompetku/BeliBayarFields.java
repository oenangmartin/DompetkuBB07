package com.indosatapps.dompetku;

import java.util.Calendar;
import java.util.Vector;

import javax.microedition.pim.Contact;
import javax.microedition.pim.PIM;
import javax.microedition.pim.PIMItem;

import net.rim.blackberry.api.pdap.BlackBerryContact;
import net.rim.blackberry.api.pdap.BlackBerryContactList;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.GIFEncodedImage;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TouchGesture;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.DialogClosedListener;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.picker.DateTimePicker;
import net.rim.device.api.ui.text.NumericTextFilter;
import net.rim.device.api.util.StringUtilities;

import org.json.me.JSONArray;
import org.json.me.JSONObject;

import com.indosatapps.dompetku.bean.FavoriteData;
import com.indosatapps.dompetku.bean.FieldData;
import com.indosatapps.dompetku.bean.InboxData;
import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.AnimatedGIFField;
import com.indosatapps.dompetku.field.ButtonFieldCustom;
import com.indosatapps.dompetku.field.DropDownFieldCustom;
import com.indosatapps.dompetku.field.EditFieldCustom;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.LabelFieldColor;
import com.indosatapps.dompetku.field.PasswordEditFieldCustom;
import com.indosatapps.dompetku.field.PleaseWaitPopupScreen;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.Connection;
import com.indosatapps.dompetku.utils.PageResultListener;

public class BeliBayarFields extends MainScreen implements FieldChangeListener, PageResultListener{
    private int TASK_INQUIRY = 1;
    private int TASK_CONFIRM = 2;
    
    Mediator mediator;
    String menuType, urlAction1, urlAction2, categoryName, merchantName, categoryID, merchantID;
    AnimatedGIFField _ourAnimation;
	VerticalFieldManager scr, layoutError;
	ButtonFieldCustom btnRetry, btnAction;
	HeaderField header;
	LabelFieldColor txtError;
	Vector mFieldData = new Vector();
    Vector mViewData = new Vector();
    Vector mInputData = new Vector();
    PasswordEditFieldCustom PINView;
    EditFieldCustom favoriteView;
    String favoriteText=null, favoriteParam, mAmount=null , mTo=null, mDenom=null;
    DateTimePicker datePicker;
    PageResultListener listener;
    Vector numberList = new Vector();
	
	public BeliBayarFields(String type, String categoryName, String categoryID, final String merchantID,
			String merchantName, String favoriteText, PageResultListener listener, Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.listener = listener;
		this.mediator = mediator;
        header = new HeaderField(merchantName, 0);
		add(header);
		
		this.menuType = type;
		this.categoryName = categoryName;
		this.categoryID = categoryID;
		this.merchantID = merchantID;
		this.merchantName = merchantName;
		this.favoriteText = favoriteText;
		
		GIFEncodedImage ourAnimation = (GIFEncodedImage) GIFEncodedImage.getEncodedImageResource("loading_orange.gif");
        _ourAnimation = new AnimatedGIFField(ourAnimation, FIELD_VCENTER|FIELD_HCENTER);
        int topMargin = ((Constant.deviceHeight-header.getPreferredHeight()) - _ourAnimation.getPreferredHeight()) / 2;
        _ourAnimation.setMargin(topMargin-15, 0, 0, 0);
        
        txtError = new LabelFieldColor("", Constant.colorFontGeneral, USE_ALL_WIDTH|DrawStyle.HCENTER);
        txtError.setFont(Assets.fontGlobal);
        btnRetry = new ButtonFieldCustom("COBA LAGI", Constant.deviceWidth/3, FIELD_HCENTER);
        btnRetry.setEditable(true);
        btnRetry.setMargin(5, 0, 0, 0);
        btnRetry.setChangeListener(this);
        layoutError = new VerticalFieldManager(USE_ALL_WIDTH);
        
        btnAction = new ButtonFieldCustom(menuType.toUpperCase(), Constant.deviceWidth/3, FIELD_HCENTER);
		btnAction.setEditable(true);
		btnAction.setMargin(10, 0, 10, 0);
        
        scr = new VerticalFieldManager(USE_ALL_WIDTH|USE_ALL_HEIGHT|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 15, 15, 15);
		scr.add(_ourAnimation);
		
		add(scr);
		
		mediator.app.invokeLater(new Runnable() {
			public void run() {
				new GetFieldListTask(merchantID).run();
			}
		});
	}
	
	public class GetFieldListTask implements Runnable{
		String merchantID;
		
		public GetFieldListTask(String merchantID) {
			this.merchantID = merchantID;
		}
		
		public void run() {
			final String entity = Connection.getFieldList(merchantID);
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONArray jsonArray = new JSONArray(entity);
                        	JSONObject obj = jsonArray.getJSONObject(0);
                            urlAction1 = obj.getString("url_action1");
                            urlAction2 = obj.getString("url_action2");
                            JSONArray arrayFieldData = obj.getJSONArray("detail_field");
                            if(arrayFieldData.length()>0){
                            	scr.deleteAll();
                            	mFieldData.removeAllElements();
                                for (int i = 0; i < arrayFieldData.length(); i++) {
									JSONObject data = arrayFieldData.getJSONObject(i);
									FieldData fieldData = new FieldData();
									fieldData.setKategori_id(data.getString("kategori_id"));
									fieldData.setKategori_nama(data.getString("kategori_nama"));
									fieldData.setSub_merchant_id(data.getString("sub_merchant_id"));
									fieldData.setSub_merchant_nama(data.getString("sub_merchant_nama"));
									fieldData.setField_id(data.getString("field_id"));
									fieldData.setParam_name(data.getString("param_name"));
									fieldData.setLabel(data.getString("label"));
									fieldData.setField_tipe(data.getString("field_tipe"));
									fieldData.setId_isi_dropdown(data.getString("id_isi_dropdown"));
									fieldData.setIsi_dropdown(data.getString("isi_dropdown"));
									fieldData.setTextview_type(data.getString("textview_type"));
									fieldData.setDropdown_src(data.getString("dropdown_src"));
									fieldData.setIs_favorite(data.getString("is_favorite"));
									fieldData.setOrder(data.getString("field_order"));
									mFieldData.addElement(fieldData);
									
									//add field to mViewData
									if(fieldData.getField_tipe().equals(Constant.FIELD_TYPE_TEXTVIEW)){
										if(fieldData.getTextview_type().equals(Constant.FIELD_TEXTVIEW_TYPE_PIN)){
											PasswordEditFieldCustom field = createPassEditFieldView(fieldData);
											mViewData.addElement(field);
											scr.add(field.getField());
										}else{
											EditFieldCustom field = createEditFieldView(fieldData, i);
											mViewData.addElement(field);
											scr.add(field.getField());
										}
									}else{
										DropDownFieldCustom field = createDropdownFieldView(fieldData);
										mViewData.addElement(field);
										scr.add(field.getField());
									}
									
									if(fieldData.getParam_name().equals("pin")){
                                        PINView = (PasswordEditFieldCustom) mViewData.elementAt(i);
                                    }
                                    if(fieldData.getIs_favorite().equals("1")){
                                        favoriteView = (EditFieldCustom) mViewData.elementAt(i);
                                        favoriteParam = fieldData.getParam_name();
                                    }
                            	}
                                
                                btnAction.setChangeListener(null);
	                            btnAction.setChangeListener(new FieldChangeListener() {
									public void fieldChanged(Field field, int context) {
										boolean isComplete = true;
                                        mInputData.removeAllElements();
                                        
                                        for(int i=0;i<mViewData.size();i++){
                                            FieldData fieldData = (FieldData) mFieldData.elementAt(i);
                                            Object view = mViewData.elementAt(i);
                                            
                                            if(fieldData.getField_tipe().equals(Constant.FIELD_TYPE_TEXTVIEW)){
    											if(fieldData.getTextview_type().equals(Constant.FIELD_TEXTVIEW_TYPE_PIN)){
    												PasswordEditFieldCustom editPIN = (PasswordEditFieldCustom) view;
    												String txtInput = editPIN.getText();
    												
    												editPIN.setIsError(false);
    												if(txtInput.length() == 0){
    													editPIN.setIsError(true);
    													Dialog.alert("Mohon isi "+fieldData.getLabel());
	                                                    isComplete = false;
	                                                }
    											}else{
    												EditFieldCustom editField = (EditFieldCustom) view;
    												String txtInput = editField.getText();
    												
    												editField.setIsError(false);
    												if(txtInput.length() == 0){
    													editField.setIsError(true);
    													Dialog.alert("Mohon isi "+fieldData.getLabel());
	                                                    isComplete = false;
	                                                }
    												
    												if(fieldData.getTextview_type().equals(Constant.FIELD_TEXTVIEW_TYPE_CALENDAR)){
                                                        mInputData.addElement(new String[]{fieldData.getParam_name(), BasicUtils.StandardToServerDate(editField.getText())});
                                                    }else{
                                                        if(fieldData.getParam_name().equals("denom")){
                                                            mInputData.addElement(new String[]{fieldData.getParam_name(), editField.getText()});
                                                            mInputData.addElement(new String[]{"amount", editField.getText()});
                                                        }else
                                                            mInputData.addElement(new String[]{fieldData.getParam_name(), editField.getText()});
                                                    }
    												if(fieldData.getParam_name().equals("amount")){
	                                                    mAmount = editField.getText();
	                                                }
	                                                if(fieldData.getParam_name().equals("to")){
	                                                    mTo = editField.getText();
	                                                }
	                                                if(fieldData.getParam_name().equals("denom")){
	                                                    mDenom= editField.getText();
	                                                }
    											}
    										}else{
    											DropDownFieldCustom dropdownField = (DropDownFieldCustom) view;
    											/*String[] itemLabel = fieldData.getIsi_dropdown().split(";");
                                                String[] itemID = fieldData.getId_isi_dropdown().split(";");*/
                                                if(fieldData.getParam_name().equals("denom")){
                                                    mInputData.addElement(new String[]{fieldData.getParam_name(), dropdownField.getSelectedIndexValue()});
                                                    mInputData.addElement(new String[]{"amount", dropdownField.getSelectedIndexValue()});
                                                }else
                                                    mInputData.addElement(new String[]{fieldData.getParam_name(), dropdownField.getSelectedIndexValue()});
                                                
                                                if(fieldData.getParam_name().equals("amount")){
                                                    mAmount = dropdownField.getSelectedIndexValue();
                                                }
                                                if(fieldData.getParam_name().equals("denom")){
                                                    mDenom = dropdownField.getSelectedIndexLabel();
                                                }
    										}
                                        }
                                        
                                        if(isComplete){
                                        	String temp = "";
                                        	for(int i=0;i<mInputData.size();i++){
                                                String[] inputData = (String[]) mInputData.elementAt(i);
                                                if(i==0)
                                                	temp = temp + inputData[0]+"="+inputData[1];
                                                else
                                                	temp = temp + "&" + inputData[0]+"="+inputData[1];
                                            }
                                        	final String params = temp;
                                        	
                                            if(PINView!=null){
                                                if(!urlAction2.equals("")){
                                                	new DoTransaction(TASK_INQUIRY, urlAction1, params, PINView.getText());
                                                }else{
                                                    String msg = "Anda akan mem"+menuType.toLowerCase()+" "+categoryName+"-"+merchantName+" dengan detail sebagai berikut:";
                                                    for (int i = 0;i<mFieldData.size();i++){
                                                        FieldData data = (FieldData) mFieldData.elementAt(i);
                                                        Object view = mViewData.elementAt(i);
                                                        if(!data.getParam_name().equals("pin")){
                                                            if(data.getField_tipe().equals(Constant.FIELD_TYPE_TEXTVIEW)){
                                                                EditFieldCustom editText = (EditFieldCustom) view;
                                                                String txtInput = editText.getText();
                                                                
                                                                msg = msg + "\n"+data.getLabel()+" : "+txtInput;
                                                            }else if(data.getField_tipe().equals(Constant.FIELD_TYPE_DROPDOWN)){
                                                                DropDownFieldCustom spinner = (DropDownFieldCustom) view;
                                                                msg = msg + "\n"+data.getLabel()+" : "+spinner.getSelectedIndexLabel();
                                                            }
                                                        }
                                                    }
                                                    
                                                    Dialog d = new Dialog(msg, new String[]{""+menuType, "Batal"}, new int[]{Dialog.OK, Dialog.CANCEL}, Dialog.OK, null);
                                            		d.setEscapeEnabled(false);
                                            		d.setDialogClosedListener(new DialogClosedListener() {
                    									public void dialogClosed(Dialog dialog, int choice) {
                    										if(choice == Dialog.OK){
                    											new DoTransaction(TASK_CONFIRM, urlAction1, params, PINView.getText());
                    										}
                    									}
                    								});
                                            		d.show();
                                                }
                                            }else{
                                                Dialog.alert("PIN not found!");
                                            }
                                            
                                        }
									}
								});
                                
                                scr.add(btnAction);
                                scr.invalidate();
                            }
                        }catch (Exception e){
                        	System.out.println(">> BeliBayarField Task_Load_Field error: "+e.getMessage());
                            e.printStackTrace();
                            setupLayoutError(Constant.txt_error_json, true);
                        }
                    }else{
                    	setupLayoutError(Constant.txt_error_no_connection, true);
                    }
                }
			});
		}
	}
	
	public class DoTransaction implements Runnable{
		int taskID;
		String username, pin, url, param;
		
		public DoTransaction(int taskID, String url, String param, String pin) {
			this.taskID = taskID;
			this.url = url;
			this.param = param;
			this.pin = pin;
			username = Constant.mPersistData.getUsername();
			
			if(taskID == TASK_INQUIRY) PleaseWaitPopupScreen.showScreenAndWait(this, "Processing inquiry...");
            else if(taskID == TASK_CONFIRM) PleaseWaitPopupScreen.showScreenAndWait(this, "Processing payment...");
		}
		
		public void run() {
			final String entity = Connection.doTransaction(username, pin, url, param);
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject objs = new JSONObject(entity);
                            int status = objs.getInt("status");
                            if(status == 0){
                            	if(taskID == TASK_INQUIRY){
                            		//{"status":0,"trxid":"21583346","msg":"Success","price":"21000","billpayQueryState":false,"billpayState":false}
                                    final String trxID = objs.optString("trxid", null);
                                    final String amount = objs.optString("price", null);
                                    String msg = "Anda akan mem"+menuType.toLowerCase()+" "+categoryName+"-"+merchantName+" dengan detail sebagai berikut:"
                                                + checkDetailResponse(objs) ;
                                    
                                    Dialog d = new Dialog(msg, new String[]{""+menuType, "Batal"}, new int[]{Dialog.OK, Dialog.CANCEL}, Dialog.OK, null);
                            		d.setEscapeEnabled(false);
                            		d.setDialogClosedListener(new DialogClosedListener() {
    									public void dialogClosed(Dialog dialog, int choice) {
    										if(choice == Dialog.OK){
    											//do confirm payment
                                                String tempParam = "";
                                                tempParam = tempParam + "transid="+trxID;
                                                if(amount!=null)
                                                	tempParam = tempParam + "&amount="+amount;
                                                else
                                                	tempParam = tempParam + "&amount="+mAmount;
                                                
                                                for(int i=0;i<mInputData.size();i++){
                                                    if(!tempParam.startsWith("amount"))
                                                    	tempParam += "&"+mInputData.elementAt(i);
                                                }
                                                
                                                new DoTransaction(TASK_CONFIRM, urlAction2, tempParam, PINView.getText());
    										}
    									}
    								});
                            		d.show();
                            	}else if(taskID == TASK_CONFIRM){
                            		long currTimeMilis = System.currentTimeMillis();
                                    String currTime = BasicUtils.formatDateFromMilliseconds(currTimeMilis, "dd/MM/yyyy HH:mm");
                                    String voucherCode = objs.optString("voucherCode", null);
                                    String trxID = objs.getString("trxid");
                                    String additional = "-", msg = "";
                                    
                                    if(voucherCode!=null && !voucherCode.equals("")){
                                        msg = "Anda telah melakukan Pem"+menuType.toLowerCase()+"an "+categoryName+" "+merchantName+
                                                " pada tanggal "+currTime.substring(0, currTime.indexOf(" "))+" sebesar "+mAmount+". Voucher: "+voucherCode+".\nRefID: "+trxID;
                                        additional = voucherCode;
                                    }else{
                                        String ke = "";
                                        if(mTo != null && !mTo.equals("")){
                                            ke = " ke "+mTo;
                                        }
                                        msg = "Anda telah melakukan Pem"+menuType.toLowerCase()+"an "+categoryName+" "+merchantName+
                                                " pada tanggal "+currTime.substring(0, currTime.indexOf(" "))+ke+" sebesar "+mAmount+".\nRefID: "+trxID;
                                    }
                                    
                                    //save to inbox
                                    InboxData inboxData = new InboxData(trxID, currTimeMilis, mAmount, mTo, additional, menuType, categoryName, ""+categoryID, merchantName, ""+merchantID);
                                    Constant.mListInbox.addElement(inboxData);
                                    Constant.mHashMapInbox.put(Long.toString(inboxData.getDate()), inboxData);
                                    BasicUtils.saveInboxData();
                                    
                                    BasicUtils.showMessageDialog(msg, new DialogClosedListener() {
    									public void dialogClosed(Dialog dialog, int choice) {
    										if(choice == Dialog.OK){
    											if(listener!=null) listener.onPageResult(Constant.REQUEST_CODE_REFRESH_HOME, Constant.RESULT_OK, null);
    		                                	close();
    										}
    									}
    								});
                            	}
                            }else if(status == 1001){
                            	Dialog.alert("Saldo Anda tidak mencukupi untuk melakukan transaksi ini.");
                            }else if(status == 1014){
                            	Dialog.alert("PIN salah. Mohon periksa kembali PIN Anda.");
                            }else if(status == 1099){
                            	Dialog.alert("Transaksi gagal.");
                            }else if(status == 23){
                            	Dialog.alert("Jumlah terlalu kecil.");
                            }else if(status == 24){
                            	Dialog.alert("Jumlah terlalu besar.");
                            }else{
                            	Dialog.alert("Transaksi gagal.");  //+objs.getMsg());
                            }
                        }catch (Exception e){
                        	System.out.println(">> BeliBayarField Transaction error: "+e.getMessage());
                            e.printStackTrace();
                            Dialog.alert(Constant.txt_error_json);
                        }
                    }else{
                    	Dialog.alert(Constant.txt_error_no_connection);
                    }
                }
			});
		}
	}
	
	public EditFieldCustom createEditFieldView(FieldData fieldData, final int pos){
		EditFieldCustom field = null;
		if(fieldData.getField_tipe().equals(Constant.FIELD_TYPE_TEXTVIEW)){
			//set is favorite
			boolean isFav = false;
            if(fieldData.getIs_favorite().equals("1")){
            	if(favoriteText != null) isFav = false;
            	else isFav = true;
            }
            
            if(fieldData.getTextview_type().equals(Constant.FIELD_TEXTVIEW_TYPE_NORMAL)){
            	field = new EditFieldCustom(fieldData.getLabel(), "", Constant.deviceWidth-30, 50, isFav, false, FIELD_HCENTER);
            }else if(fieldData.getTextview_type().equals(Constant.FIELD_TEXTVIEW_TYPE_CONTACT)){
                field = new EditFieldCustom(fieldData.getLabel(), "", Constant.deviceWidth-30, 16, isFav, true, FIELD_HCENTER);
                field.setFilter(NumericTextFilter.get(NumericTextFilter.NUMERIC));
                
                field.setBtnSearchListener(new FieldChangeListener() {
					public void fieldChanged(Field f, int context) {
						//do get Contact
		                Constant.REQUEST_CODE_CONTACT_PICKER = pos;
		                try{
							BlackBerryContactList list = (BlackBerryContactList)PIM.getInstance().
														openPIMList(PIM.CONTACT_LIST, PIM.READ_ONLY);
							PIMItem contact = list.choose();
							processContact(contact);
						}catch (Exception e) {
							System.out.println(">> Contact Picker error: "+e.getMessage());
							e.printStackTrace();
							Dialog.alert("Gagal membuka Kontak.");
						}
					}
				});
            }else if(fieldData.getTextview_type().equals(Constant.FIELD_TEXTVIEW_TYPE_CALENDAR)){
            	EditField editCalendar = new EditField("", "Klik untuk membuka kalender", EditField.DEFAULT_MAXCHARS, FIELD_HCENTER|EditField.NO_NEWLINE){
        			protected void paint(Graphics g){
        				g.setColor(Constant.colorFontFormInput);
                	    super.paint(g);
                	}
        			
        			protected boolean navigationClick(int status, int time) {
        				UiApplication.getUiApplication().invokeLater(new Runnable() {
        	                public void run() {
        	                	datePicker = DateTimePicker.createInstance(datePicker.getDateTime(), "dd MMM yyyy", null);
        				        datePicker.doModal();
        				        
        				        Calendar c = datePicker.getDateTime();
        				        String dd = Integer.toString(c.get(Calendar.DAY_OF_MONTH));
        				        String mm = Integer.toString(c.get(Calendar.MONTH)+1);
        				        String yy = Integer.toString(c.get(Calendar.YEAR));
        				        
        				        if(dd.length()==1){
        				        	dd = "0"+dd;
        				        }
        				        if(mm.length()==1){
        				        	mm = "0"+mm;
        				        }
        				        
        				        setText(BasicUtils.CalendarToStandardDate(dd, mm, yy));
        	                }
        				});
        				return true;
        			}
        			
        			protected boolean touchEvent(TouchEvent message){
        				if(message.getEvent()==TouchEvent.GESTURE){
        					if(message.getGesture().getEvent()==TouchGesture.TAP){
        						UiApplication.getUiApplication().invokeLater(new Runnable() {
                	                public void run() {
                	                	datePicker = DateTimePicker.createInstance(datePicker.getDateTime(), "dd MMM yyyy", null);
                				        datePicker.doModal();
                				        
                				        Calendar c = datePicker.getDateTime();
                				        String dd = Integer.toString(c.get(Calendar.DAY_OF_MONTH));
                				        String mm = Integer.toString(c.get(Calendar.MONTH)+1);
                				        String yy = Integer.toString(c.get(Calendar.YEAR));
                				        
                				        if(dd.length()==1){
                				        	dd = "0"+dd;
                				        }
                				        if(mm.length()==1){
                				        	mm = "0"+mm;
                				        }
                				        
                				        setText(BasicUtils.CalendarToStandardDate(dd, mm, yy));
                	                }
                				});
        						return true;
        					}
        				}
        				return super.touchEvent(message);
        			}
        		};
        		editCalendar.setEditable(false);
        		editCalendar.setFont(Assets.fontGlobal);
        		datePicker = DateTimePicker.createInstance(Calendar.getInstance(), "dd MMM yyyy", null);
        		field = new EditFieldCustom(fieldData.getLabel(), editCalendar, "", Constant.deviceWidth-30, 15, isFav, false, FIELD_HCENTER);
            }else if(fieldData.getTextview_type().equals(Constant.FIELD_TEXTVIEW_TYPE_NUMBER)){
            	field = new EditFieldCustom(fieldData.getLabel(), "", Constant.deviceWidth-30, 20, isFav, false, FIELD_HCENTER);
                field.setFilter(NumericTextFilter.get(NumericTextFilter.NUMERIC));
            }else if(fieldData.getTextview_type().equals(Constant.FIELD_TEXTVIEW_TYPE_STATION)){
            	EditField editField = new EditField("", "Klik untuk membuka dftar Stasiun", EditField.DEFAULT_MAXCHARS, FIELD_HCENTER|EditField.NO_NEWLINE){
        			protected void paint(Graphics g){
        				g.setColor(Constant.colorFontFormInput);
                	    super.paint(g);
                	}
        			
        			protected boolean navigationClick(int status, int time) {
        				UiApplication.getUiApplication().invokeLater(new Runnable() {
        	                public void run() {
        	                	//go to Bank Code List
        	                	mediator.app.pushScreen(new ListDropdown(ListDropdown.STASIUN, 0, BeliBayarFields.this));
        	                }
        				});
        				return true;
        			}
        			
        			protected boolean touchEvent(TouchEvent message){
        				if(message.getEvent()==TouchEvent.GESTURE)
        				{
        					if(message.getGesture().getEvent()==TouchGesture.TAP)
        					{
        						mediator.app.pushScreen(new ListDropdown(ListDropdown.STASIUN, 0, BeliBayarFields.this));
        						return true;
        					}
        				}
        				return super.touchEvent(message);
        			}
        		};
        		editField.setEditable(false);
        		editField.setFont(Assets.fontGlobal);
        		field = new EditFieldCustom(fieldData.getLabel(), editField, "", Constant.deviceWidth-30, EditField.DEFAULT_MAXCHARS, isFav, false, FIELD_HCENTER);
            }
            
            //set is favorite
            if(fieldData.getIs_favorite().equals("1")){
                if(favoriteText != null){
                    field.setText(favoriteText);
                    field.setEditable(false);
                }else{
                	field.setBtnFavoriteListener(new FieldChangeListener() {
						public void fieldChanged(Field field, int context) {
							if(favoriteView!=null){
                                String favText = favoriteView.getText();
                                favoriteView.setIsError(false);
                                if(favText.length() == 0){
                                	favoriteView.setIsError(true);
                                	Dialog.alert("Mohon isi "+favoriteView.getLabel());
                                }else{
                                    DialogAddFavorite favDialog = new DialogAddFavorite(categoryName+"_"+merchantName+"_"+favText, favText, favoriteParam);
                                    favDialog.doModal();
                                }
                            }else{
                            	Dialog.alert("Favorite not found!");
                            }
						}
					});
                }
            }
        }
        return field;
    }
	
	public PasswordEditFieldCustom createPassEditFieldView(FieldData fieldData){
		PasswordEditFieldCustom editPIN = new PasswordEditFieldCustom(fieldData.getLabel(), "", Constant.deviceWidth-30, 6, FIELD_HCENTER);
		editPIN.setFilter(NumericTextFilter.get(NumericTextFilter.NUMERIC));
		return editPIN;
	}
	
	public DropDownFieldCustom createDropdownFieldView(FieldData fieldData){
		String[] itemLabel = BasicUtils.split(fieldData.getIsi_dropdown(), ";");
        String[] itemID = BasicUtils.split(fieldData.getId_isi_dropdown(), ";");
        
		DropDownFieldCustom dropdownField = new DropDownFieldCustom(fieldData.getLabel(), Constant.deviceWidth-30, itemLabel, itemID, 0, FIELD_HCENTER);
		return dropdownField;
	}
	
	public String checkDetailResponse(JSONObject objs){
		String msisdn = objs.optString("msisdn", null);
    	String name = objs.optString("name", null);
    	String price = objs.optString("price", null);
    	String fee = objs.optString("fee", null);
        
		String detail = "";
        if(mTo != null){
            if(menuType.equalsIgnoreCase("Beli"))
                detail = detail + "\nNo. Tujuan: "+mTo;
            else
                detail = detail + "\nNo. Pelanggan: "+mTo;
        }else{
        	if(msisdn!=null) mTo = msisdn;
            else if(name!=null) mTo = name;
            else mTo = "-";
        }
        if(name!=null){
            detail = detail + "\nNama: "+name;
        }
        if(mDenom != null){
            detail = detail + "\nNominal: "+mDenom;
        }
        if(price!=null){
            String harga = price;
            try{
                if(fee!=null){
                    int hargaInt = Integer.parseInt(harga);
                    int feeInt = Integer.parseInt(fee);
                    hargaInt = hargaInt - feeInt;
                    harga = Integer.toString(hargaInt);
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            
            if(menuType.equalsIgnoreCase("Beli"))
                detail = detail + "\nHarga: "+harga;
            else
                detail = detail + "\nJumlah: "+harga;
            
            if(mAmount == null) mAmount = price;
        }
        if(fee!=null){
            detail = detail + "\nFee: "+fee;
            detail = detail + "\nTotal: "+price;
        }
        
        return detail;
    }
	
	class DialogAddFavorite extends Dialog{
		EditField editNama;
		ButtonField btnAddFav;
		String favoriteText, paramName;
		
	    public DialogAddFavorite(String hint, String favoriteText, String paramName){
	        super("Masukkan nama favorit:", null, null, Dialog.DISCARD, null, Dialog.GLOBAL_STATUS);
	        this.favoriteText = favoriteText;
            this.paramName = paramName;
            
	        VerticalFieldManager vfm = new VerticalFieldManager(FIELD_HCENTER);
	        if(hint.length()>50) hint = hint.substring(0, 50);
	        editNama = new EditField("", hint, 50, 0);
	        editNama.setEditable(true);
	        
	        btnAddFav = new ButtonField("Submit", FIELD_HCENTER);
	        btnAddFav.setMargin(10, 0, 0, 0);
	        btnAddFav.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					String favName = editNama.getText();
                    if(favName.length() == 0){
                    	Dialog.alert("Mohon isi Nama Favorit");
                    }else{
                        if(Constant.mHashMapFavorite.containsKey(editNama.getText())){
                        	Dialog.alert("Favorit dengan Nama ini sudah terdaftar. Silakan pilih nama lain.");
                        }else{
                            //do add to favorite
                            FavoriteData favoriteData = new FavoriteData(editNama.getText(), DialogAddFavorite.this.favoriteText, menuType, categoryName, ""+categoryID, merchantName, ""+merchantID, DialogAddFavorite.this.paramName);
                            Constant.mListFavorite.addElement(favoriteData);
                            Constant.mHashMapFavorite.put(favoriteData.getFavoriteID(), favoriteData);
                            BasicUtils.saveFavoriteData();
                            BasicUtils.showMessageDialog("Berhasil menambahkan ke favorit.", new DialogClosedListener() {
								public void dialogClosed(Dialog dialog, int choice) {
									close();
								}
							});
                        }
                    }
				}
			});
	        
	        vfm.add(editNama);
		    vfm.add(btnAddFav);
	
		    this.add(vfm);
	    }
	    
	    public boolean keyChar(char key, int status, int time) {
	    	if(key==Characters.ESCAPE){
	    		this.close();
	    	}
	        return super.keyChar(key, status, time);
	     }
	}
	
	public void setMSISDNFromContact(String msisdn){
		EditFieldCustom view = (EditFieldCustom) mViewData.elementAt(Constant.REQUEST_CODE_CONTACT_PICKER);
        view.setText(msisdn);
	}
	
	public void processContact(PIMItem contact){
		if(contact!=null && contact instanceof BlackBerryContact){
			numberList.removeAllElements();
			int telephonesCount = contact.countValues(Contact.TEL);
			if(telephonesCount > 0){
			    for(int i=0; i< telephonesCount; ++i) {
			    	String number = contact.getString(Contact.TEL, i);
			    	if(number.indexOf(' ') > -1)
			    		number = StringUtilities.removeChars(number, " ");
			        if(number.indexOf("-") > -1)
			        	number = StringUtilities.removeChars(number, "-");
			        if(number.startsWith("+62"))
			        	number = "0"+number.substring(3);
			        
			        int attribute = contact.getAttributes(BlackBerryContact.TEL, i);
	                if(attribute == Contact.ATTR_MOBILE){
	                	numberList.addElement(new String[]{"Mobile: ", number});
	                }else if (attribute == Contact.ATTR_HOME){
	                	numberList.addElement(new String[]{"Home: ", number});
			    	}else if (attribute == Contact.ATTR_WORK){
			    		numberList.addElement(new String[]{"Work: ", number});
			    	}
	            }
			    
			    if(numberList.size()>0){
			    	if(numberList.size() == 1){
			    		String[] msisdn = (String[]) numberList.elementAt(0);
			    		setMSISDNFromContact(msisdn[1]);
			    	}else{
			    		ContactDialog d = new ContactDialog(numberList);
			    		d.doModal();
			    	}
			    }else{
			    	Dialog.alert("Nomor tidak ditemukan.");
			    }
			}else{
				//no number
				Dialog.alert("Nomor tidak ditemukan.");
			}
        }
	}
	
	class ContactDialog extends Dialog{
		
	    public ContactDialog(Vector numberList){
	        super("Pilih nomor:", null, null, Dialog.DISCARD, null, Dialog.GLOBAL_STATUS);
	        
	        VerticalFieldManager vfm = new VerticalFieldManager(FIELD_HCENTER);

	        for(int i=0;i<numberList.size();i++){
	        	final String[] msisdn = (String[]) numberList.elementAt(i);
	        	ButtonField button = new ButtonField(msisdn[0]+msisdn[1], FIELD_HCENTER);
	        	button.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						setMSISDNFromContact(msisdn[1]);
						close();
					}
				});
	        	vfm.add(button);
	        }
	        
		    this.add(vfm);
	    }
	    
	    public boolean keyChar(char key, int status, int time) {
	    	if(key==Characters.ESCAPE){
	    		this.close();
	    	}
	        return true;
	     }
	}
	
	public void fieldChanged(Field field, int context) {
		if(field == btnRetry){
			scr.deleteAll();
        	scr.add(_ourAnimation);
        	scr.invalidate();
        	mediator.app.invokeLater(new Runnable() {
    			public void run() {
    				new GetFieldListTask(merchantID).run();
    			}
    		});
		}
	}
	
	public void setupLayoutError(String error, boolean isShowButton){
		layoutError.deleteAll();
		txtError.setText(error);
		layoutError.add(txtError);
		if(isShowButton) layoutError.add(btnRetry);
        int marginError = ((Constant.deviceHeight-header.getPreferredHeight()) - layoutError.getPreferredHeight()) / 2;
        layoutError.setMargin(marginError-15, 0, 0, 0);
        
        scr.deleteAll();
    	scr.add(layoutError);
    	scr.invalidate();
	}

	public boolean onClose() {
		this.close();
		return true;
	}

	public void onPageResult(int requestCode, int resultCode, Object data) {
		if(requestCode == Constant.REQUEST_CODE_DROPDOWN_LIST){
			if(resultCode == Constant.RESULT_OK){
				//set Station to edittext
				String[] result = (String[]) data;
				EditFieldCustom field = (EditFieldCustom) mViewData.elementAt(Integer.parseInt(result[1]));
				field.setText(result[0]);
			}
		}
	}
}
