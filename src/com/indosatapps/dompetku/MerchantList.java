package com.indosatapps.dompetku;

import net.rim.device.api.system.GIFEncodedImage;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.FlowFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import org.json.me.JSONArray;
import org.json.me.JSONObject;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.AnimatedGIFField;
import com.indosatapps.dompetku.field.ButtonFieldCustom;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.LabelFieldColor;
import com.indosatapps.dompetku.field.ListMerchantField;
import com.indosatapps.dompetku.utils.BlackberryUtils;
import com.indosatapps.dompetku.utils.Connection;

public class MerchantList extends MainScreen implements FieldChangeListener{
	Mediator mediator;
	AnimatedGIFField _ourAnimation;
	VerticalFieldManager scr, layoutError;
	FlowFieldManager holder;
	ButtonFieldCustom btnRetry, btnMore;
	HeaderField header;
	LabelFieldColor txtError, txtLoading;
	int currPage, totalPage;
	
	public MerchantList(Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		header = new HeaderField("Daftar Merchant", 0);
		add(header);
		
		GIFEncodedImage ourAnimation = (GIFEncodedImage) GIFEncodedImage.getEncodedImageResource("loading_orange.gif");
        _ourAnimation = new AnimatedGIFField(ourAnimation, FIELD_VCENTER|FIELD_HCENTER);
        int topMargin = ((Constant.deviceHeight-header.getPreferredHeight()) - _ourAnimation.getPreferredHeight()) / 2;
        _ourAnimation.setMargin(topMargin-15, 0, 0, 0);
        
        txtError = new LabelFieldColor("", Constant.colorFontGeneral, USE_ALL_WIDTH|DrawStyle.HCENTER);
        txtError.setFont(Assets.fontGlobal);
        btnRetry = new ButtonFieldCustom("COBA LAGI", Constant.deviceWidth/3, FIELD_HCENTER);
        btnRetry.setEditable(true);
        btnRetry.setMargin(5, 0, 0, 0);
        btnRetry.setChangeListener(this);
        layoutError = new VerticalFieldManager(USE_ALL_WIDTH);
        
        holder = new FlowFieldManager(VERTICAL_SCROLLBAR);
        
        btnMore = new ButtonFieldCustom("MORE", Constant.deviceWidth/3, FIELD_HCENTER);
        btnMore.setEditable(true);
        btnMore.setMargin(10, 0, 10, (Constant.deviceWidth-20-btnMore.getW())/2);
		btnMore.setChangeListener(this);
		txtLoading = new LabelFieldColor("Loading...", Constant.colorFontGeneral, USE_ALL_WIDTH|DrawStyle.HCENTER);
		txtLoading.setPadding(10, 0, 10, 0);
        
        scr = new VerticalFieldManager(USE_ALL_WIDTH|USE_ALL_HEIGHT|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 10, 15, 10);
		scr.add(_ourAnimation);
		
		add(scr);
		
		mediator.app.invokeLater(new Runnable() {
			public void run() {
				new DoGetMerchantListTask("1").run();
			}
		});
	}
	
	public class DoGetMerchantListTask implements Runnable{
		String page;
		
		public DoGetMerchantListTask(String page) {
			this.page = page;
		}
		
		public void run() {
			final String entity = Connection.getMerchantList("10", page);
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject objs = new JSONObject(entity);
                        	currPage = Integer.parseInt(objs.optString("current_page", "1"));
                        	totalPage = Integer.parseInt(objs.optString("total_page", "1"));
                        	JSONArray array = objs.getJSONArray("detail");
                        	if(array.length()>0){
                        		scr.deleteAll();
                        		for(int i=0;i<array.length();i++){
                        			//{"list_merchant_nama":"Alfamart","list_merchant_img":"","list_merchant_url":"http:\/\/www.alfamartku.com"}
                        			JSONObject data = array.getJSONObject(i);
                        			String name = data.optString("list_merchant_nama", "");
                        			String imgUrl = data.optString("list_merchant_img", "");
                        			final String url = data.optString("list_merchant_url", "");
                        			ListMerchantField field = new ListMerchantField(name, imgUrl, 0);
                        			field.setMargin(10, 5, 10, 5);
                        			if(!url.equals("")){
	                        			field.setChangeListener(new FieldChangeListener() {
											public void fieldChanged(Field field, int context) {
												BlackberryUtils.invokeBrowser(url);
											}
										});
                        			}
                        			holder.add(field);
                        		}
                        		if(currPage<totalPage)holder.add(btnMore);
                        		scr.add(holder);
                        		scr.invalidate();
                        	}else{
                        		//No Merchant
                        		setupLayoutError("Tidak ada Merchant.", false);
                        	}
                        }catch (Exception e){
                        	System.out.println("Get Merchant Error: "+e.getMessage());
                            e.printStackTrace();
                            setupLayoutError(Constant.txt_error_json, true);
                        }
                    }else{
                    	setupLayoutError(Constant.txt_error_no_connection, true);
                    }
                }
			});
		}
	}
	
	public void fieldChanged(Field field, int context) {
		if(field == btnRetry){
			scr.deleteAll();
        	scr.add(_ourAnimation);
        	scr.invalidate();
        	mediator.app.invokeLater(new Runnable() {
    			public void run() {
    				new DoGetMerchantListTask("1").run();
    			}
    		});
		}else if(field==btnMore){
			currPage++;
			holder.replace(btnMore, txtLoading);
			holder.invalidate();
			
			Thread load = new Thread(loadMore);
			load.start();
		}
	}
	
	public Runnable loadMore = new Runnable() {
		public void run() {
			final String entity = Connection.getMerchantList("10", Integer.toString(currPage));
			UiApplication.getUiApplication().invokeLater(new Runnable() {
				public void run() {
					if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject objs = new JSONObject(entity);
                        	currPage = Integer.parseInt(objs.optString("current_page", "1"));
                        	totalPage = Integer.parseInt(objs.optString("total_page", "1"));
                        	JSONArray array = objs.getJSONArray("detail");
                        	if(array.length()>0){
                        		for(int i=0;i<array.length();i++){
                        			//{"list_merchant_nama":"Alfamart","list_merchant_img":"","list_merchant_url":"http:\/\/www.alfamartku.com"}
                        			JSONObject data = array.getJSONObject(i);
                        			String name = data.optString("list_merchant_nama", "");
                        			String imgUrl = data.optString("list_merchant_img", "");
                        			final String url = data.optString("list_merchant_url", "");
                        			ListMerchantField field = new ListMerchantField(name, imgUrl, 0);
                        			field.setMargin(10, 5, 10, 5);
                        			if(!url.equals("")){
	                        			field.setChangeListener(new FieldChangeListener() {
											public void fieldChanged(Field field, int context) {
												BlackberryUtils.invokeBrowser(url);
											}
										});
                        			}
                        			holder.add(field);
                        		}
                        		holder.delete(txtLoading);
                        		if(currPage<totalPage)holder.add(btnMore);
                        	}else{
                        		holder.replace(txtLoading, btnMore);
                        		Dialog.alert("Tidak ada merchant.");
                        	}
                        }catch (Exception e){
                        	System.out.println("Get Merchant Error: "+e.getMessage());
                            e.printStackTrace();
                            holder.replace(txtLoading, btnMore);
                    		Dialog.alert(Constant.txt_error_json);
                        }
                    }else{
                    	holder.replace(txtLoading, btnMore);
                		Dialog.alert(Constant.txt_error_no_connection);
                    }
					holder.invalidate();
				}
			});
		}
	};
	
	public void setupLayoutError(String error, boolean isShowButton){
		layoutError.deleteAll();
		txtError.setText(error);
		layoutError.add(txtError);
		if(isShowButton) layoutError.add(btnRetry);
        int marginError = ((Constant.deviceHeight-header.getPreferredHeight()) - layoutError.getPreferredHeight()) / 2;
        layoutError.setMargin(marginError-15, 0, 0, 0);
        
        scr.deleteAll();
    	scr.add(layoutError);
    	scr.invalidate();
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }
}
