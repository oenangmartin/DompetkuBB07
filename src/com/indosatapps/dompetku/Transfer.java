package com.indosatapps.dompetku;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.ListItemIconField;
import com.indosatapps.dompetku.utils.PageResultListener;

public class Transfer extends MainScreen implements FieldChangeListener, PageResultListener{
	Mediator mediator;
	ListItemIconField mTransferDompetku, mTransferOpLain, mTransferBank;
	PageResultListener listener;
	
	public Transfer(Mediator mediator, PageResultListener listener) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.listener = listener;
		this.mediator = mediator;
		HeaderField header = new HeaderField("Transfer", 0);
		add(header);
		
		mTransferBank = new ListItemIconField(Assets.menuTransferBank, "Rekening Bank", Constant.deviceWidth-30, 0);
		mTransferDompetku = new ListItemIconField(Assets.menuTransferMoney, "Dompetku", Constant.deviceWidth-30, 0);
		mTransferOpLain = new ListItemIconField(Assets.menuTransferMoney, "Operator Lain", Constant.deviceWidth-30, 0);
		
		mTransferBank.setChangeListener(this);
		mTransferDompetku.setChangeListener(this);
		mTransferOpLain.setChangeListener(this);
		
		VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 15, 15, 15);
		scr.add(mTransferDompetku);
		scr.add(mTransferOpLain);
		scr.add(mTransferBank);
		
		add(scr);
	}
	
	public void fieldChanged(Field field, int context) {
		if(field == mTransferDompetku){
			mediator.app.pushScreen(new TransferDompetku(mediator, this));
		}else if(field == mTransferOpLain){
			mediator.app.pushScreen(new TransferOpLain(mediator, this));
		}else if(field == mTransferBank){
			mediator.app.pushScreen(new TransferBank(mediator, this));
		}
	}

	public void onPageResult(int requestCode, int resultCode, Object data) {
		if(resultCode == Constant.RESULT_OK){
			listener.onPageResult(0, resultCode, data);
        	close();
        }
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }

}