package com.indosatapps.dompetku;

import java.util.Random;
import java.util.Vector;

import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.FocusChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TouchGesture;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import org.json.me.JSONArray;
import org.json.me.JSONObject;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.ButtonBitmapField;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.LabelFieldColor;
import com.indosatapps.dompetku.field.PleaseWaitPopupScreen;
import com.indosatapps.dompetku.field.SaldoTextField;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.Connection;
import com.indosatapps.dompetku.utils.PageResultListener;
import com.thirdparty.device.api.ui.container.EvenlySpacedToolbar;
import com.thirdparty.device.api.ui.container.JustifiedHorizontalFieldManager;
import com.thirdparty.device.api.ui.container.JustifiedVerticalFieldManager;

public class Home extends MainScreen implements FieldChangeListener, FocusChangeListener, PageResultListener{
	Mediator mediator;
	SaldoTextField txtSaldo;
	LabelFieldColor txtDateSaldo, txtMenuTitle, txtPromo;
	ButtonBitmapField menuNav, menuBeli, menuBayar, menuToken, menuTransfer;
	VerticalFieldManager topField, middleField, promoField;
	JustifiedVerticalFieldManager scr;
	JustifiedHorizontalFieldManager bottomField;
	ButtonBitmapField btnNext, btnPrev;
	Random rnd = new Random();
	int currPromo, totalPromo;
	Vector listPromo = new Vector();
	
	public Home(final Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		HeaderField header = new HeaderField("", 0);
		add(header);
		
		LabelFieldColor labelSaldo = new LabelFieldColor("Saldo", Constant.colorFontGeneral, FIELD_LEFT|FIELD_VCENTER);
		txtSaldo = new SaldoTextField("Rp 0,-", FIELD_RIGHT|NON_FOCUSABLE);
		JustifiedHorizontalFieldManager lineSaldo = new JustifiedHorizontalFieldManager(labelSaldo, null, txtSaldo, USE_ALL_WIDTH|NON_FOCUSABLE);
		lineSaldo.setMargin(0, 0, 5, 0);
		
		txtDateSaldo = new LabelFieldColor("Per", Constant.colorFontGeneral, FIELD_RIGHT);
		txtDateSaldo.setFont(Assets.fontMini);
		txtDateSaldo.setMargin(5, 0, 10, 0);
		
		topField = new VerticalFieldManager(NON_FOCUSABLE);
		topField.add(lineSaldo);
		topField.add(txtDateSaldo);
		
		txtMenuTitle = new LabelFieldColor("", Constant.colorPrimary, FIELD_HCENTER|NON_FOCUSABLE);
		txtMenuTitle.setFont(Assets.fontGlobalBold);
		txtMenuTitle.setMargin(10, 0, 5, 0);
		
		menuNav = new ButtonBitmapField(Assets.menuNav, Assets.menuNavFocus, FIELD_HCENTER|FIELD_VCENTER);
		menuBeli = new ButtonBitmapField(Assets.menuBeli, Assets.menuBeliFocus, FIELD_HCENTER|FIELD_VCENTER);
		menuBayar = new ButtonBitmapField(Assets.menuBayar, Assets.menuBayarFocus, FIELD_HCENTER|FIELD_VCENTER);
		menuToken = new ButtonBitmapField(Assets.menuToken, Assets.menuTokenFocus, FIELD_HCENTER|FIELD_VCENTER);
		menuTransfer = new ButtonBitmapField(Assets.menuTransfer, Assets.menuTransferFocus, FIELD_HCENTER|FIELD_VCENTER);
		
		menuNav.setEditable(true);
		menuBeli.setEditable(true);
		menuBayar.setEditable(true);
		menuToken.setEditable(true);
		menuTransfer.setEditable(true);
		
		menuNav.setFocusListener(this);
		menuBeli.setFocusListener(this);
		menuBayar.setFocusListener(this);
		menuToken.setFocusListener(this);
		menuTransfer.setFocusListener(this);
		
		menuNav.setChangeListener(this);
		menuBeli.setChangeListener(this);
		menuBayar.setChangeListener(this);
		menuToken.setChangeListener(this);
		menuTransfer.setChangeListener(this);
		
		EvenlySpacedToolbar lineMenu = new EvenlySpacedToolbar(USE_ALL_WIDTH);
		lineMenu.add(menuNav);
		lineMenu.add(menuBeli);
		lineMenu.add(menuBayar);
		lineMenu.add(menuToken);
		lineMenu.add(menuTransfer);
		
		middleField = new VerticalFieldManager(FIELD_BOTTOM);
		middleField.add(txtMenuTitle);
		middleField.add(lineMenu);
		
		/*VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL);
		scr.setPadding(15, 15, 15, 15);
		scr.add(lineSaldo);
		scr.add(txtDateSaldo);
		scr.add(txtMenuTitle);
		scr.add(lineMenu);*/
		
		txtPromo = new LabelFieldColor("", Constant.colorFontGeneral, FOCUSABLE);
		txtPromo.setMargin(10, 10, 10, 10);
		txtPromo.setFont(Assets.fontMini);
		
		promoField = new VerticalFieldManager(USE_ALL_WIDTH){
			protected void paint(Graphics g) {
				g.setColor(txtPromo.isFocus()?Constant.colorBgHyperlink:Color.WHITE);
				g.drawRoundRect(0, 0, getWidth(), getHeight(), 15, 15);
				g.fillRoundRect(0, 0, getWidth(), getHeight(), 15, 15);
				super.paint(g);
			}
			
			protected void drawFocus(Graphics g, boolean bo){}
			
			protected void onFocus(int direction){
				super.onFocus(direction);
				invalidate();
			}
			
			protected void onUnfocus(){
				super.onUnfocus();
				invalidate();
			}
			
			protected boolean keyChar(char character, int status, int time){
				if(character==Characters.ENTER){
					fieldChangeNotify(0);
					return true;
				}
				return super.keyChar(character, status, time);
			}
			
			protected boolean navigationClick(int status, int time){
				fieldChangeNotify(0);
				return true;
			}
			
			protected boolean touchEvent(TouchEvent message){
				if(message.getEvent()==TouchEvent.GESTURE)
				{
					if(message.getGesture().getEvent()==TouchGesture.TAP)
					{
						fieldChangeNotify(0);
						return true;
					}
				}
				return super.touchEvent(message);
			}
		};
		promoField.add(txtPromo);
		
		btnNext = new ButtonBitmapField(Assets.icNext, Assets.icNextFocus, FIELD_VCENTER);
		btnNext.setEditable(true);
		btnPrev = new ButtonBitmapField(Assets.icPrev, Assets.icPrevFocus, FIELD_VCENTER);
		btnPrev.setEditable(true);
		btnNext.setMargin(0, 0, 0, 5);
		btnPrev.setMargin(0, 5, 0, 0);
		
		bottomField = new JustifiedHorizontalFieldManager(null, null, null, USE_ALL_WIDTH|FIELD_VCENTER);
		bottomField.setMargin(10, 0, 0, 0);
		
		scr = new JustifiedVerticalFieldManager(topField, middleField, null, USE_ALL_WIDTH|USE_ALL_HEIGHT);
		scr.setPadding(15, 15, 15, 15);
		
		add(scr);
		
		if(BasicUtils.isShowTutorial()){
			mediator.app.invokeLater(new Runnable() {
				public void run() {
					mediator.app.pushScreen(new Tutorial(Home.this));
				}
			});
		}else{
			new DoFetchHomeData();
		}
	}
	
	protected void makeMenu(Menu menu, int instance) {
		menu.add(new MenuItem("Refresh", 1, 10){
			public void run() {
				new DoFetchHomeData();
			}
		});
		
		menu.add(new MenuItem("Exit", 10000, 15){
			public void run() {
				onClose();
			}
		});
	}
	
	public class DoFetchHomeData implements Runnable{
		String username;
		
		public DoFetchHomeData() {
			username = Constant.mPersistData.getUsername();
			PleaseWaitPopupScreen.showScreenAndWait(this, "Loading user info...");
		}
		
		public void run() {
			String response1 = Connection.doCheckBalance(username);
            String response2 = Connection.getPromoList();
            final String entity = response1+"##"+response2;
            
            UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	String[] entityArray = BasicUtils.split(entity, "##");
                    System.out.println(">> Entity Array 0: "+entityArray[0]);
                    System.out.println(">> Entity Array 1: "+entityArray[1]);
                    
                    //for User Balance
                    if (!entityArray[0].equalsIgnoreCase("false")) {
                        try{
                        	JSONObject objs = new JSONObject(entityArray[0]);
                            String balance = objs.getString("balance");
                            
                            txtSaldo.setSaldo(BasicUtils.getFormattedBalance(balance, true)+",-");
                            txtDateSaldo.setText("Per "+BasicUtils.formatDateFromMilliseconds(System.currentTimeMillis(), "dd/MM/yyyy HH:mm"));
                        }catch(Exception e){
                            e.printStackTrace();
                            Dialog.alert(Constant.txt_error_json);
                        }
                    }else{
                    	Dialog.alert(Constant.txt_error_no_connection);
                    }
                    
                    //for Promo
                    if (!entityArray[1].equalsIgnoreCase("false")) {
                        try{
                        	JSONArray objs = new JSONArray(entityArray[1]);
                            if(objs.length()>0){
                            	//int pos = rnd.nextInt(objs.length());
                            	listPromo.removeAllElements();
                            	for (int i = 0; i < objs.length(); i++) {
                            		JSONObject obj = objs.getJSONObject(i);
                                	String promoText = obj.getString("promo_text");
                                	String promoURL = obj.getString("promo_url");
                                	listPromo.addElement(new String[]{promoText, promoURL});
								}
                            	
                            	for(int j=0;j<2;j++){
	                            	currPromo = 1;
	                            	totalPromo = listPromo.size();
	                            	setPromoText(0);
	                            	
	                            	if(totalPromo>1){
	                            		bottomField.replace(bottomField.getLeftField(), btnPrev);
	                            		bottomField.replace(bottomField.getCenterField(), promoField);
	                            		bottomField.replace(bottomField.getRightField(), btnNext);
	                            		
	                            		double margin = ((double) (promoField.getHeight()-btnPrev.getHeight()))/2;
	                            		int marginInt = (int) margin;
	                            		System.out.println(">> Margin panah: "+margin+" -> "+marginInt);
	                            		btnPrev.setMargin(marginInt, 5, 0, 0);
	                            		btnNext.setMargin(marginInt, 0, 0, 5);
	                            		btnPrev.setChangeListener(null);
	                            		btnPrev.setChangeListener(new FieldChangeListener() {
											public void fieldChanged(Field field, int context) {
												if(currPromo==1) currPromo=totalPromo;
												else currPromo--;
												
												setPromoText(currPromo-1);
											}
										});
	                            		btnNext.setChangeListener(null);
	                            		btnNext.setChangeListener(new FieldChangeListener() {
											public void fieldChanged(Field field, int context) {
												if(currPromo==totalPromo) currPromo=1;
												else currPromo++;
												
												setPromoText(currPromo-1);
											}
										});
	                            	}else{
	                            		bottomField.replace(bottomField.getCenterField(), promoField);
	                            	}
	                            	bottomField.invalidate();
	                            	scr.replace(scr._bottomField, new NullField(NON_FOCUSABLE));
	                            	scr.replace(scr._bottomField, bottomField);
                            	}
                            }else{
                            	scr.replace(scr._bottomField, new NullField(NON_FOCUSABLE));
                            }
                        }catch(Exception e){
                        	System.out.println("<< Get Promo error: "+e.getMessage());
                            e.printStackTrace();
                            scr.replace(scr._bottomField, new NullField(NON_FOCUSABLE));
                        }
                    }else{
                    	scr.replace(scr._bottomField, new NullField(NON_FOCUSABLE));
                    }
                }
            });
		}
	}
	
	public void setPromoText(int pos){
		final String[] promoData = (String[]) listPromo.elementAt(pos);
    	txtPromo.setText(promoData[0]);
    	promoField.setChangeListener(null);
    	promoField.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context) {
				mediator.app.pushScreen(new WebView(promoData[1], mediator));
			}
		});
	}
	
	public boolean onClose(){
		int exit = Dialog.ask("Anda yakin ingin keluar dari aplikasi?", new String[]{"Ya", "Tidak"}, new int[]{1,0}, 1);
		if(exit == 1){
			System.exit(0);
	    	return true;
		}else{
			return false;
		}
    }

	public void fieldChanged(Field field, int context) {
		if(field == menuNav){
			mediator.app.pushScreen(new MenuLain(mediator, this));
		}else if(field == menuBeli){
			mediator.app.pushScreen(new BeliBayarKategori(BeliBayarKategori.TYPE_BELI, this, mediator));
		}else if(field == menuBayar){
			mediator.app.pushScreen(new BeliBayarKategori(BeliBayarKategori.TYPE_BAYAR, this, mediator));
		}else if(field == menuToken){
			mediator.app.pushScreen(new MintaToken(mediator, this));
		}else if(field == menuTransfer){
			mediator.app.pushScreen(new Transfer(mediator, this));
		}
	}

	public void focusChanged(Field field, int eventType) {
		if(eventType == FOCUS_GAINED){
			if(field == menuNav)
				txtMenuTitle.setText("Menu Lain");
			else if(field == menuBeli)
				txtMenuTitle.setText("Beli");
			else if(field == menuBayar)
				txtMenuTitle.setText("Bayar");
			else if(field == menuToken)
				txtMenuTitle.setText("Minta Token");
			else if(field == menuTransfer)
				txtMenuTitle.setText("Transfer");
			else
				txtMenuTitle.setText("");
		}
	}

	public void onPageResult(int requestCode, int resultCode, Object data) {
        if(requestCode == Constant.REQUEST_CODE_REFRESH_HOME && resultCode == Constant.RESULT_OK){
        	new DoFetchHomeData();
        }else if(requestCode == Constant.REQUEST_CODE_GANTI_PIN && resultCode == Constant.RESULT_OK){
			close();
		}
	}
}
