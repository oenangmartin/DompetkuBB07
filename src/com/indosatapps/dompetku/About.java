package com.indosatapps.dompetku;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.ActiveRichTextField;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.HeaderField;

public class About extends MainScreen{
	Mediator mediator;
	HeaderField header;
	String text;
	int[] offsets, bg, fg;
	Font[] fonts;
	byte[] attributes;
	
	public static int ABOUT = 1;
    public static int ABOUT_SETOR_TUNAI = 2;
    public static int ABOUT_BELI_BAYAR = 3;
    public static int ABOUT_KIRIM_UANG = 4;
    public static int ABOUT_TARIK_TUNAI = 5;
    public static int ABOUT_CONTACT_US = 6;
    public static int ABOUT_TERM_CONDITION = 7;
	
	public About(Mediator mediator, int type) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		header = new HeaderField("", 0);
		add(header);
		
		BitmapField logoField = new BitmapField(Assets.logo, USE_ALL_WIDTH|FIELD_HCENTER);
		logoField.setMargin(0, 0, 10, 0);
		
		initText(type);
		ActiveRichTextField labelField = new ActiveRichTextField(text, offsets, attributes, fonts, fg, bg, HIGHLIGHT_FOCUS){
			protected void drawFocus(Graphics graphics, boolean on) {}
		};
		
		VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 15, 15, 15);
		scr.add(logoField);
		scr.add(labelField);
		
		add(scr);
	}
	
	public void initText(int type){
		if(type == ABOUT){
			header.setTitle("Tentang Dompetku");
			text = "Tentang Dompetku"+
					"\nDompetku adalah layanan inovatif untuk pelanggan Indosat melakukan berbagai transaksi finansial seperti pembelian di merchant, pembayaran tagihan, pengisian pulsa, P2P transfer, pengiriman uang melalui ponsel yang telah didaftarkan. Layanan ini dapat diakses semua jenis ponsel dimana saja dan kapan saja tanpa perlu membawa uang tunai."+
					"\n\nKeuntungan Dompetku"+
					"\nAman\nTransaksi menggunakan dompetku terjamin keamanannya, setiap transaksi yang dilakukan harus menggunakan PIN."+
					"\n\nMudah\nMemudahkan transaksi tanpa perlu ke ATM untuk pembelian, pembayaran tagihan, isi pulsa, dll melalui ponsel."+
					"\n\nMurah\nSemua transaksi yang dilakukan melalui dompetku biayanya murah dan sangat terjangkau.";

			offsets = new int[]{0, text.indexOf("Dompetku adalah"), text.indexOf("Keuntungan Dompetku"), text.indexOf("Aman"), text.length()};
			fonts = new Font[]{Assets.fontMini, Assets.fontMiniBold};
			bg = new int[]{Color.WHITE, Color.WHITE};
			fg = new int[]{Constant.colorFontGeneral, Constant.colorFontGeneral};
			attributes = new byte[]{1, 0, 1, 0};
		}else if(type == ABOUT_SETOR_TUNAI){
			header.setTitle("Setor Tunai");
			text = "Setor Tunai"+
				    "\nSetor tunai atau pengisian saldo Dompetku bisa dilakukan di seluruh Galeri Indosat, Agen Dompetku, ATM Bersama, ALTO, Prima atau merchant seperti Alfamart Group, Indomaret, 7 Eleven, Circle K."+
				    "\n\nMelalui ATM Bank\nPilih menu Transfer Ke Bank Lain lalu masukkan kode bank 789 dan Nomor handphone sebagai nomor rekening tujuan."+
				    "\n\nMelalui Merchant / Agen Dompetku\nDatang ke Merchant / Agen Dompetku lalu lakukan permintaan setor tunai (cash in) dengan memberikan informasi nomor handphone dan uang yang akan disetor kepada kasir."+
				    "\n\nMelalui Galeri Indosat\nIsi form setor tunai lalu serahkan uang yang akan diisi kepada Kasir Galeri Indosat.";
		
			offsets = new int[]{0, text.indexOf("Setor tunai atau"), text.length()};
			fonts = new Font[]{Assets.fontMini, Assets.fontMiniBold};
			bg = new int[]{Color.WHITE, Color.WHITE};
			fg = new int[]{Constant.colorFontGeneral, Constant.colorFontGeneral};
			attributes = new byte[]{1, 0};
		}else if(type == ABOUT_BELI_BAYAR){
			header.setTitle("Beli / Bayar");
			text = "Beli\nLakukan pembelian pulsa, voucher games, asuransi, voucher TV, donasi, dan isi ulang kartu KRL dari menu Beli pada bagian bawah aplikasi Dompetku."+
				    "\n\nBayar Tagihan\nLakukan pembayaran tagihan telepon, TV, air, pinjaman dan internet dari menu Bayar pada bagian bawah aplikasi Dompetku."+
				    "\n\nBelanja di Merchant\nLakukan permintaan token pada menu Minta Token pada bagian bawah aplikasi. Lalu serahkan token dan nomor handphone anda ke kasir Merchant."+
				    "\n\nUntuk transaksi di merchant online, cukup pilih metode pembayaran Dompetku dan masukkan nomor handphone anda yang terdaftar Dompetku. Notifikasi akan muncul di handphone anda lalu balas dengan memasukkan PIN untuk konfirmasi transaksi.";
			
			offsets = new int[]{0, text.indexOf("Lakukan pembelian"), text.indexOf("Bayar Tagihan"), text.indexOf("Lakukan pembayaran"), text.indexOf("Belanja di Merchant"), text.indexOf("Lakukan permintaan"), text.length()};
			fonts = new Font[]{Assets.fontMini, Assets.fontMiniBold};
			bg = new int[]{Color.WHITE, Color.WHITE};
			fg = new int[]{Constant.colorFontGeneral, Constant.colorFontGeneral};
			attributes = new byte[]{1, 0, 1, 0, 1, 0};
		}else if(type == ABOUT_KIRIM_UANG){
			header.setTitle("Kirim Uang");
			text = "Kirim Uang\nKirim uang dapat dilakukan ke sesama pengguna Dompetku serta operator uang elektronik lain seperti XL Tunai dan Telkomsel T-Cash. Kirim uang juga dapat dilakukan ke rekening bank yang tergabung dalam ATM Bersama. Pilih menu Kirim Uang pada bagian bawah aplikasi Dompetku.";
			
			offsets = new int[]{0, text.indexOf("Kirim uang dapat"), text.length()};
			fonts = new Font[]{Assets.fontMini, Assets.fontMiniBold};
			bg = new int[]{Color.WHITE, Color.WHITE};
			fg = new int[]{Constant.colorFontGeneral, Constant.colorFontGeneral};
			attributes = new byte[]{1, 0};
		}else if(type == ABOUT_TARIK_TUNAI){
			header.setTitle("Tarik Tunai");
			text = "Tarik Tunai\nLakukan permintaan token pada menu Minta Token pada bagian bawah aplikasi Dompetku. Lalu datang ke Agen/Merchant/Galeri  Indosat terdekat dan lakukan permintaan Tarik Tunai. Informasikan nomor handphone serta token yang diterima kepada Kasir.";
			
			offsets = new int[]{0, text.indexOf("Lakukan permintaan"), text.length()};
			fonts = new Font[]{Assets.fontMini, Assets.fontMiniBold};
			bg = new int[]{Color.WHITE, Color.WHITE};
			fg = new int[]{Constant.colorFontGeneral, Constant.colorFontGeneral};
			attributes = new byte[]{1, 0};
		}else if(type == ABOUT_CONTACT_US){
			header.setTitle("Hubungi Kami");
			text = "Sampaikan pertanyaan, keluhan, ataupun saran Anda terkait layanan Dompetku melalui Customer Service Indosat. Berikut adalah kontak yang bisa anda hubungi:"+
				    "\n\nMatrix & StarOne:\n111 (gratis)"+
				    "\n\nMentari & IM3:\n100 (berbayar, Rp 400/panggilan)"+
				    "\n\nPSTN/Telepon Rumah:\n(021) 5438 8888 dan (021) 3000 3000"+
				    "\n(021) 3011 1111 (khusus pelanggan StarOne)"+
				    "\n\nMesin Penjawab:\n222 (gratis)"+
				    "\n\nFax:\n(021) 5449501-06"+
					"\n\nEmail:\ncustomer_service@indosat.com"+
					"\n\nTwitter:\n@IndosatCare";
			
			offsets = new int[]{0, text.length()};
			fonts = new Font[]{Assets.fontMini, Assets.fontMiniBold};
			bg = new int[]{Color.WHITE, Color.WHITE};
			fg = new int[]{Constant.colorFontGeneral, Constant.colorFontGeneral};
			attributes = new byte[]{0};
		}else if(type == ABOUT_TERM_CONDITION){
			header.setTitle("Syarat & Ketentuan");
			text = "Ketentuan Umum dan Syarat-Syarat Layanan Dompetku"+
				    "\n\nKetentuan umum dan syarat-syarat layanan Dompetku ini (�Ketentuan dan Syarat�) berlaku dan mengikat bagi Pelanggan serta PT Indosat Tbk (�Indosat�)."+
				    "\n\n1.   DEFINISI"+
				    "\n\n1.1.	�Batas Dana� adalah jumlah maksimal dana yang ditetapkan Indosat untuk ditampung dalam setiap Rekening Pelanggan, termasuk setiap perubahannya dari waktu ke waktu."+
					"\n\n1.2.	�Layanan Dompetku� adalah layanan yang memungkinkan pelanggan melakukan pembayaran Transaksi dengan menggunakan fasilitas telekomunikasi berupa nomor MSISDN Pelanggan untuk mendebet dana yang ada di Rekening Pelanggan."+
					"\n\n1.3.	Rekening Pelanggan adalah nomor rekening Layanan Dompetku dengan menggunakan nomor MSISDN Pelanggan sebagai rekening virtual untuk menampung Dana Pelanggan yang akan digunakan untuk Transaksi."+
					"\n\n1.4.	�Pelanggan� adalah pelanggan jasa telekomunikasi Indosat baik pasca bayar maupun prabayar yang sudah mendaftarkan diri untuk menggunakan Layanan Dompetku."+
					"\n\n1.5.	�Masa Berlaku� adalah masa atau periode dimana Pelanggan dapat menggunakan Layanan Dompetku yaitu sejak Layanan Dompetku diaktifkan sampai dengan berakhirnya Layanan Dompetku."+
					"\n\n1.6.	�Transaksi� adalah transaksi jual-beli barang dan/atau jasa yang pembayarannya dilakukan dengan menggunakan fasilitas Layanan Dompetku, di tempat-tempat yang telah menjalin kerjasama dengan Indosat (�Merchant�)."+
					"\n\n1.7.	�Pengisian Dana� adalah penyetoran dana ke  Rekening Pelanggan oleh Pelanggan atau pihak lainnya."+
					"\n\n1.8.	�Pemberitahuan Tertulis� adalah pemberitahuan  terkait Layanan Dompetku yang disampaikan secara tertulis baik langsung dikirim ke alamat Pelanggan maupun disampaikan melalui media komunikasi lainnya."+
					"\n\n1.9.	�PIN� adalah nomor sandi pribadi yang dikeluarkan oleh Indosat untuk setiap Rekening Pelanggan untuk tujuan keamanan Rekening Pelanggan."+
					"\n\n1.10. �Force Majeure� adalah kejadian-kejadian di luar kekuasaan Indosat yang mengakibatkan terhenti atau tertundanya pelaksanaan kewajiban Indosat berdasarkan Ketentuan dan Syarat ini, termasuk : petir, gempa bumi, topan, kebakaran, perang, ledakan, banjir, tsunami, sabotase, huru hara atau kebijakan pemerintah/regulator atau tindakan pihak ketiga."+
					"\n\n2.   PENGGUNAAN LAYANAN DOMPETKU"+
					"\n\n2.1. Layanan Dompetku hanya boleh digunakan oleh Pelanggan dan tidak dapat dipindahtangankan kepada pihak lain tanpa persetujuan Indosat. Segala akibat atau kerugian yang timbul karena kesalahan, kelalaian, ketidakhati�hatian Pelanggan sendiri dalam menggunakan layanan ini adalah merupakan beban dan tanggung jawab Pelanggan sepenuhnya."+
					"\n\n2.2. 	Layanan Dompetku tidak dapat digunakan untuk transaksi Pembelanjaan/Pembayaran di Merchant apabila :"+
					"\n(a) Dana dalam Rekening Pelanggan tidak mencukupi;"+
					"\n(b) Kartu Prabayar Pelanggan dalam masa tenggang atau sudah kadaluarsa;"+
					"\n(c) Kartu Pasca Bayar Pelanggan dalam keadaan terblokir baik sebagian (outgoing) maupun seluruhnya (incoming & outgoing)."+
					"\n\n2.3. Dalam hal Pelanggan melanggar sebagian atau seluruh Ketentuan dan Syarat ini, maka Indosat berhak untuk: "+
					"\n(a) Menolak setiap Transaksi yang dilakukan Pelanggan."+ 
					"\n(b) Menutup dan/atau mengakhiri dan/atau memblokir Layanan Dompetku untuk Pelanggan bersangkutan."+ 
					"\n\n3.   PIN"+
					"\n\n3.1. Indosat akan menerbitkan PIN atas setiap nomor Rekening Pelanggan. Penjelasan lebih lanjut mengenai fungsi PIN diatur di dalam buku/brosur/leaflet/pamflet/flyer pedoman Layanan Dompetku."+
					"\n\n3.2. Pelanggan harus selalu menjaga kerahasiaan PIN dan tidak boleh memberitahukannya kepada pihak lain dalam keadaan atau dengan cara apapun. Segala akibat yang timbul karena kelalaian, ketidakhati�hatian, atau atas penggunaan atau penyalahgunaan PIN oleh Pelanggan atau orang lain dengan atau tanpa izin dari Pelanggan yang bersangkutan, adalah merupakan beban dan tanggung jawab sepenuhnya dari Pelanggan."+
					"\n\n4.   BATAS DANA"+
					"\n\nIndosat, dengan tetap memperhatikan ketentuan perundang�undangan yang berlaku, berhak menetapkan serta merubah Batas Dana sewaktu-waktu melalui Pemberitahuan Tertulis. Untuk saat ini Batas Dana Rekening Layanan Dompetku adalah :"+
					"\n4.1. Untuk pelanggan Reguler maksimum Rp 1.000.000,- (Satu Juta Rupiah)"+
					"\n4.2. Untuk pelanggan Premium maksimum Rp 5.000.000,-(Lima Juta Rupiah)"+ 
					"\n\n5.   PENGISIAN DANA"+
					"\n\n5.1. Pengisian Dana ke Rekening Pelanggan dapat dilakukan melalui fasilitas sebagai berikut:"+
					"\n(a) Galeri Indosat;"+
					"\n(b) Melalui Merchant / Agent yang ditunjuk;"+
					"\n(c) ATM Bank;"+
					"\n(d) Mobile Banking / SMS Banking;"+
					"\n(e) Internet Banking;"+
					"\n(f) Telephone Banking;"+
					"\n(g) Tunai melalui Bank yang ditunjuk; atau"+
					"\n(h) Fasilitas lainnya yang akan diberitahukan oleh Indosat dari waktu ke waktu."+
					"\n\n5.2. Indosat sewaktu�waktu berhak melakukan perubahan atas fasilitas�fasilitas tersebut di atas. Pelanggan mengetahui dan setuju bahwa atas penggunaan Layanan Dompetku ini, Indosat atau mitranya mengenakan biaya administrasi bulanan sebagaimana ditentukan di Formulir Pendaftaran yang akan dibebankan dan didebet dari Rekening Pelanggan secara bulanan. Biaya administrasi bulanan dapat berubah sesuai kebijakan yang berlaku di Indosat."+
					"\n\n5.3. Pelanggan tidak mendapatkan bunga atas penempatan Dananya di Rekening Pelanggan."+
					"\n\n5.4. Apabila Masa Berlaku Layanan Dompetku telah berakhir atau ditutup atau Kartu SIM diblokir atau dalam masa tenggang atau kadaluarsa dan masih terdapat sisa Dana di Rekening Pelanggan maka apabila Pelanggan tidak mengambil Dananya, Indosat berhak motong biaya administrasi bulanan sampai dengan habisnya sisa Dana yang tersimpan di Rekening Pelanggan atau ditutupnya Rekening Pelanggan."+ 
					"\n\n6.   PENGAKHIRAN LAYANAN DOMPETKU"+
					"\n\n6.1. Layanan Dompetku akan berakhir karena sebab-sebab sebagai berikut :"+
					"\n(a) Diakhiri oleh Pelanggan setiap saat dengan mengisi form penutupan yang tersedia di galeri-galeri Indosat."+
					"\n(b) Diakhiri secara seketika oleh Indosat setiap saat, apabila menurut pertimbangan Indosat, Pelanggan telah melanggar Ketentuan dan Syarat ini dan/atau melanggar ketentuan dan syarat berlangganan jasa telekomunikasi Indosat pasca bayar atau ketentuan dan syarat-syarat pemakaian kartu prabayar Indosat dan/atau peraturan perundang-undangan yang berlaku."+
					"\n(c) Kartu Prabayar Pelanggan dalam keadaan kadaluarsa."+
					"\n(d) Kartu pascabayar Pelanggan dalam keadaan terblokir baik sebagian (outgoing) maupun seluruhnya (incoming & outgoing)."+
					"\n\n6.2. Setiap penutupan atau pembatalan Layanan Dompetku baik atas kehendak Pelanggan maupun atas kehendak Indosat akan dikenakan biaya administrasi penutupan yang besarnya sebagaimana tercantum dalam Formulir Berlangganan dan dapat berubah sesuai kebijakan yang berlaku di Indosat."+
					"\n\n6.3. Pelanggan dengan ini menyatakan bertanggung jawab sepenuhnya dan karenanya membebaskan Indosat dari segala tuntutan dan/atau gugatan dalam bentuk apapun dari pihak ketiga manapun termasuk suami/istri/ahli waris Pelanggan sehubungan dengan proses pendebetan, penutupan dan/atau pembatalan dan/atau pemblokiran Layanan Dompetku. Pelanggan berjanji tidak akan melakukan suatu tindakan apapun yang membatasi atau mengurangi hak�hak Indosat berdasarkan Ketentuan dan Syarat ini."+ 
					"\n\n7.   BATAS TANGGUNG JAWAB INDOSAT"+
					"\n\n7.1. Pelanggan berjanji tidak akan menuntut Indosat atas segala kerugian tidak langsung termasuk namun tidak terbatas pada kehilangan keuntungan, kehilangan pendapatan atau kehilangan kesempatan maupun kerugian immateriil lainnya termasuk karena tuntutan dari pihak manapun yang timbul karena penggunaan Layanan Dompetku oleh Pelanggan."+
					"\n\n7.2. Indosat hanya berkewajiban untuk menanggung kerugian langsung yang diderita Pelanggan sepanjang dapat dibuktikan karena kesalahan Indosat, dengan jumlah maksimum sebesar jumlah nominal transaksi yang bermasalah."+
					"\n\n7.3. Indosat dibebaskan dari kewajibannya berdasarkan Ketentuan dan Syarat ini apabila terjadi Force Majeure atau karena tindakan pihak ketiga atau karena peraturan pemerintah yang menyebabkan Indosat tidak bisa memberikan Layanan Dompetku."+
					"\n\n8.   HUKUM YANG BERLAKU"+
					"\n\n8.1. Syarat dan Ketentuan ini dibuat dan dilaksanakan berdasarkan ketentuan hukum dan perundang-undangan yang berlaku di negara Republik Indonesia."+
					"\n\n8.2. Segala perselisihan yang timbul berkenaan dengan layanan Dompetku yang tidak dapat diselesaikan secara musyawarah akan diselesaikan melalui Pengadilan Negeri Jakarta Pusat.";
			
			offsets = new int[]{0, text.indexOf("Ketentuan umum dan syarat-syarat layanan Dompetku ini"), text.length()};
			fonts = new Font[]{Assets.fontMini, Assets.fontMiniBold};
			bg = new int[]{Color.WHITE, Color.WHITE};
			fg = new int[]{Constant.colorFontGeneral, Constant.colorFontGeneral};
			attributes = new byte[]{1, 0};
		}
	}

	public boolean onClose(){
		this.close();
    	return true;
    }
}
