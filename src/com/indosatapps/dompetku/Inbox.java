package com.indosatapps.dompetku;

import java.util.Enumeration;

import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.util.Comparator;
import net.rim.device.api.util.SimpleSortingVector;

import com.indosatapps.dompetku.bean.InboxData;
import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.LabelFieldColor;
import com.indosatapps.dompetku.field.ListInboxField;
import com.indosatapps.dompetku.utils.BasicUtils;

public class Inbox extends MainScreen{
	Mediator mediator;
	VerticalFieldManager scr;
	LabelFieldColor txtEmpty;
	SimpleSortingVector sortingVector = new SimpleSortingVector();
	
	public Inbox(Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
        HeaderField header = new HeaderField("Inbox", 0);
		add(header);
		
		txtEmpty = new LabelFieldColor("Tidak ada inbox", Constant.colorFontGeneral, USE_ALL_WIDTH|DrawStyle.HCENTER);
        txtEmpty.setFont(Assets.fontGlobal);
        int margin = ((Constant.deviceHeight-header.getPreferredHeight()) - txtEmpty.getPreferredHeight()) / 2;
        txtEmpty.setMargin(margin-15, 0, 0, 0);
		
		scr = new VerticalFieldManager(USE_ALL_WIDTH|USE_ALL_HEIGHT|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 15, 15, 15);
		add(scr);
		
		initInboxList();
	}
	
	public void initInboxList(){
		if(Constant.mHashMapInbox.size()>0){
            sortInbox();
            scr.deleteAll();
            for (Enumeration e = sortingVector.elements(); e.hasMoreElements();){
                String key = (String) e.nextElement();
                final InboxData sortedInbox  = (InboxData) Constant.mHashMapInbox.get(key);

                // Do whatever you have to with the sortedItem
                ListInboxField inboxField = new ListInboxField(sortedInbox, Constant.deviceWidth-30, 0);
                inboxField.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						InboxDialog d = new InboxDialog(mediator, sortedInbox);
						d.doModal();
					}
				});
                scr.add(inboxField);
            }
            scr.invalidate();
        }else{
        	scr.deleteAll();
            scr.add(txtEmpty);
            scr.invalidate();
        }
	}
	
	public void sortInbox(){
		sortingVector.removeAllElements();
		for (Enumeration e = Constant.mHashMapInbox.keys(); e.hasMoreElements();){
            String key = (String) e.nextElement();
            sortingVector.addElement(key);
        }

        // Pass in the comparator and sort the keys
        sortingVector.setSortComparator(new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((((String) o2).compareTo((String) o1)));
			}
		});
        sortingVector.reSort();
	}
	
	class InboxDialog extends Dialog{
		ButtonField btnDetail, btnDelete;
		
	    public InboxDialog(final Mediator mediator, final InboxData inboxData){
	        super("Inbox", null, null, Dialog.DISCARD, null, Dialog.GLOBAL_STATUS);
	        
	        VerticalFieldManager vfm = new VerticalFieldManager(FIELD_HCENTER);

	        btnDetail = new ButtonField("Lihat Detail", FIELD_HCENTER);
	        btnDetail.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					//Go to Detail
					mediator.app.pushScreen(new InboxDetail(inboxData, mediator));
					close();
				}
			});
	        
	        btnDelete = new ButtonField("Hapus", FIELD_HCENTER);
	        btnDelete.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					int d = Dialog.ask("Hapus dari Inbox?", new String[]{"Hapus", "Batal"}, new int[]{1,0}, 0);
					if(d==1){
						BasicUtils.deleteInboxData(Long.toString(inboxData.getDate()));
	                    initInboxList();
	                    
	                    Dialog.inform("Inbox berhasil dihapus!");
					}
					close();
				}
			});
	        
	        
		    vfm.add(btnDetail);
		    vfm.add(btnDelete);
	
		    this.add(vfm);
	    }
	    
	    public boolean keyChar(char key, int status, int time) {
	    	if(key==Characters.ESCAPE){
	    		this.close();
	    	}
	        return true;
	     }
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }
}
