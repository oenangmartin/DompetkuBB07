package com.indosatapps.dompetku.data;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;

public class Assets {
	public static FontFamily fontFamily;
	public static Font fontGlobal;
	public static Font fontGlobalBold;
	public static Font fontGlobalUnderlined;
	public static Font fontSaldo;
	public static Font fontMini;
	public static Font fontMiniBold;
	
	public static Bitmap splash;
	public static Bitmap tutorial;
	public static Bitmap headerLogo;
	public static Bitmap logo;
	public static Bitmap basket;
	public static Bitmap basketFocus;
	public static Bitmap iconDropdown;
	public static Bitmap noImage;
	public static Bitmap icDelete;
	public static Bitmap icDeleteFocus;
	public static Bitmap icFavorite;
	public static Bitmap icFavoriteFocus;
	public static Bitmap icSearch;
	public static Bitmap icSearchFocus;
	public static Bitmap bgMerchantList;
	public static Bitmap bgMerchantListFocus;
	public static Bitmap icNext;
	public static Bitmap icNextFocus;
	public static Bitmap icPrev;
	public static Bitmap icPrevFocus;
	
	public static Bitmap menuNav;
	public static Bitmap menuNavFocus;
	public static Bitmap menuBeli;
	public static Bitmap menuBeliFocus;
	public static Bitmap menuBayar;
	public static Bitmap menuBayarFocus;
	public static Bitmap menuToken;
	public static Bitmap menuTokenFocus;
	public static Bitmap menuTransfer;
	public static Bitmap menuTransferFocus;
	public static Bitmap menuTransferMoney;
	public static Bitmap menuTransferBank;
	
	public static Bitmap[] bgEditText;
	public static Bitmap[] bgEditTextFocus;
	public static Bitmap[] bgEditTextError;
	public static Bitmap[] bgEditTextDisable;
	public static Bitmap[] btn;
	public static Bitmap[] btnFocus;
	public static Bitmap[] btnDisable;
}
