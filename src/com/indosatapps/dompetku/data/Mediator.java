package com.indosatapps.dompetku.data;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;

import com.indosatapps.dompetku.utils.BasicUtils;

public class Mediator {
	public UiApplication app;
	private boolean isPortrait;
	
	public Mediator(UiApplication app) {
		this.app = app;
		
		Constant.deviceWidth = Display.getWidth();
    	Constant.deviceHeight = Display.getHeight();
    	
    	isPortrait = false;
    	String model = DeviceInfo.getDeviceName();
    	if(model.startsWith("98") || model.startsWith("95") || model.startsWith("91")){
    		//force portrait display
    		isPortrait = true;
        	int directions = Display.DIRECTION_NORTH;
    	    UiEngineInstance engineInstance = Ui.getUiEngineInstance();
    	    if (engineInstance != null){
    	        engineInstance.setAcceptableDirections(directions);
    	    }
    	    
	    	if(Display.getOrientation()==Display.ORIENTATION_LANDSCAPE){
				int tmp = Constant.deviceWidth;
				Constant.deviceWidth = Constant.deviceHeight;
				Constant.deviceHeight = tmp;
			}
    	}
    	BasicUtils.disableOrientationChange();
    	
    	//load persist data
    	BasicUtils.loadPersistData();
    	
    	String strBgImg = "splash480.jpg";
    	if(isPortrait) strBgImg = "splash360.jpg";
    	Assets.splash = BasicUtils.resizeBitmapNoScale(Bitmap.getBitmapResource(strBgImg), Constant.deviceWidth, Constant.deviceHeight);
	}
	
	public void loadRes(){
		//for load custom font
		/*InputStream is = (InputStream)getClass().getResourceAsStream("/FS_Joey_Medium.ttf");
		int fontLoader = FontManager.getInstance().load(is, "SuperWiFiFont", FontManager.APPLICATION_FONT);
    	if(fontLoader == FontManager.SUCCESS || fontLoader == FontManager.DUPLICATE_DATA || fontLoader == FontManager.DUPLICATE_NAME ) {
    		System.out.println(">> Font loader success");
            try {
            	Banks.fontFamily = FontFamily.forName("SuperWiFiFont");
            } catch (ClassNotFoundException e){
                System.out.println(e.getMessage());
                Banks.fontFamily = FontFamily.getFontFamilies()[1];
            }
        }else{
    		System.out.println(">> Font loader error");
            Banks.fontFamily = FontFamily.getFontFamilies()[1];
        }*/
    	
		Assets.fontFamily = FontFamily.getFontFamilies()[1];
    	System.out.println(">> Font: "+Assets.fontFamily.getName());
    	System.out.println(">> isPlainSupported: "+Assets.fontFamily.isStyleSupported(Font.PLAIN));
    	System.out.println(">> isBoldSupported: "+Assets.fontFamily.isStyleSupported(Font.BOLD));
    	System.out.println(">> isItalicSupported: "+Assets.fontFamily.isStyleSupported(Font.ITALIC));
    	
    	Assets.fontGlobal = Assets.fontFamily.getFont(Font.PLAIN, 25);
    	Assets.fontGlobalBold = Assets.fontFamily.getFont(Font.BOLD, 25);
    	Assets.fontGlobalUnderlined = Assets.fontFamily.getFont(Font.UNDERLINED, 25);
    	Assets.fontSaldo = Assets.fontFamily.getFont(Font.BOLD, 30);
    	Assets.fontMini = Assets.fontFamily.getFont(Font.PLAIN, 20);
    	Assets.fontMiniBold = Assets.fontFamily.getFont(Font.BOLD, 20);
    	
    	String strTutImg = "tutorial480.png";
    	if(isPortrait) strTutImg = "tutorial360.png";
    	Assets.tutorial = BasicUtils.resizeBitmapNoScale(Bitmap.getBitmapResource(strTutImg), Constant.deviceWidth, Constant.deviceHeight);
    	Assets.headerLogo = Bitmap.getBitmapResource("header_logo.png");
    	int logoH = (int) ((Constant.deviceWidth*2/3)/3.5);
    	Assets.logo = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("logo.png"), (Constant.deviceWidth*2/3), logoH, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_TO_FIT);
    	Assets.basket = Bitmap.getBitmapResource("basket.png");
    	Assets.basketFocus = Bitmap.getBitmapResource("basket_hover.png");
    	Assets.noImage = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("ic_launcher.png"), Assets.fontGlobalBold.getHeight()+10, Assets.fontGlobalBold.getHeight()+10, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_TO_FIT);
    	Assets.icDelete = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("ic_trash_inactive.png"), Assets.fontGlobalBold.getHeight()+20, Assets.fontGlobalBold.getHeight()+20, Bitmap.FILTER_LANCZOS, 1);
    	Assets.icDeleteFocus = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("ic_trash_active.png"), Assets.fontGlobalBold.getHeight()+20, Assets.fontGlobalBold.getHeight()+20, Bitmap.FILTER_LANCZOS, 1);
    	//Assets.icNext = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("slide_next.png"), Assets.fontGlobalBold.getHeight()+20, Assets.fontGlobalBold.getHeight()+20, Bitmap.FILTER_LANCZOS, 1);
    	//Assets.icNextFocus = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("slide_next_hover.png"), Assets.fontGlobalBold.getHeight()+20, Assets.fontGlobalBold.getHeight()+20, Bitmap.FILTER_LANCZOS, 1);
    	Assets.icNext = Bitmap.getBitmapResource("slide_next.png");
    	Assets.icNextFocus = Bitmap.getBitmapResource("slide_next_hover.png");
    	Assets.icPrev = Bitmap.getBitmapResource("slide_prev.png");
    	Assets.icPrevFocus = Bitmap.getBitmapResource("slide_prev_hover.png");
    			
    	Assets.menuNav = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("menu1_nav.png"), Constant.deviceWidth/6, Constant.deviceWidth/6, Bitmap.FILTER_LANCZOS, 1);
    	Assets.menuNavFocus = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("menu1_nav_hover.png"), Constant.deviceWidth/6, Constant.deviceWidth/6, Bitmap.FILTER_LANCZOS, 1);
    	Assets.menuBeli = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("menu2_beli.png"), Constant.deviceWidth/6, Constant.deviceWidth/6, Bitmap.FILTER_LANCZOS, 1);
    	Assets.menuBeliFocus = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("menu2_beli_hover.png"), Constant.deviceWidth/6, Constant.deviceWidth/6, Bitmap.FILTER_LANCZOS, 1);
    	Assets.menuBayar = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("menu3_bayar.png"), Constant.deviceWidth/6, Constant.deviceWidth/6, Bitmap.FILTER_LANCZOS, 1);
    	Assets.menuBayarFocus = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("menu3_bayar_hover.png"), Constant.deviceWidth/6, Constant.deviceWidth/6, Bitmap.FILTER_LANCZOS, 1);
    	Assets.menuToken = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("menu4_token.png"), Constant.deviceWidth/6, Constant.deviceWidth/6, Bitmap.FILTER_LANCZOS, 1);
    	Assets.menuTokenFocus = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("menu4_token_hover.png"), Constant.deviceWidth/6, Constant.deviceWidth/6, Bitmap.FILTER_LANCZOS, 1);
    	Assets.menuTransfer = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("menu5_transfer.png"), Constant.deviceWidth/6, Constant.deviceWidth/6, Bitmap.FILTER_LANCZOS, 1);
    	Assets.menuTransferFocus = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("menu5_transfer_hover.png"), Constant.deviceWidth/6, Constant.deviceWidth/6, Bitmap.FILTER_LANCZOS, 1);
    	Assets.menuTransferMoney = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("menu_transfer_emoney.png"), Assets.fontGlobalBold.getHeight()+20, Assets.fontGlobalBold.getHeight()+20, Bitmap.FILTER_LANCZOS, 1);
    	Assets.menuTransferBank = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("menu_transfer_bank.png"), Assets.fontGlobalBold.getHeight()+20, Assets.fontGlobalBold.getHeight()+20, Bitmap.FILTER_LANCZOS, 1);
    	
    	Bitmap bgMLTemp = Bitmap.getBitmapResource("bg_merchant_logo.png");
    	int wMLTemp = (Constant.deviceWidth-30)/2;
    	double ratioWLTemp = ((double) bgMLTemp.getWidth())/wMLTemp;
    	int hMLTemp = (int) (bgMLTemp.getHeight()/ratioWLTemp);
    	Assets.bgMerchantList = BasicUtils.ResizeTransparentBitmap(bgMLTemp, wMLTemp, hMLTemp, Bitmap.FILTER_LANCZOS, 1);
    	Assets.bgMerchantListFocus = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("bg_merchant_logo_hover.png"), wMLTemp, hMLTemp, Bitmap.FILTER_LANCZOS, 1);
		
    	String btnTemp = "";
		if(Constant.deviceWidth < 360) btnTemp = "_320";
		
		Assets.btn = new Bitmap[3];
		Assets.btn[0] = Bitmap.getBitmapResource("btn_1"+btnTemp+".png");
		Assets.btn[1] = Bitmap.getBitmapResource("btn_2"+btnTemp+".png");
		Assets.btn[2] = Bitmap.getBitmapResource("btn_3"+btnTemp+".png");
		
		Assets.btnFocus = new Bitmap[3];
		Assets.btnFocus[0] = Bitmap.getBitmapResource("btn_pressed_1"+btnTemp+".png");
		Assets.btnFocus[1] = Bitmap.getBitmapResource("btn_pressed_2"+btnTemp+".png");
		Assets.btnFocus[2] = Bitmap.getBitmapResource("btn_pressed_3"+btnTemp+".png");
		
		Assets.btnDisable = new Bitmap[3];
		Assets.btnDisable[0] = Bitmap.getBitmapResource("btn_disable_1"+btnTemp+".png");
		Assets.btnDisable[1] = Bitmap.getBitmapResource("btn_disable_2"+btnTemp+".png");
		Assets.btnDisable[2] = Bitmap.getBitmapResource("btn_disable_3"+btnTemp+".png");
		
		Assets.bgEditText = new Bitmap[3];
		Assets.bgEditText[0] = Bitmap.getBitmapResource("bg_edittext_1"+btnTemp+".png");
		Assets.bgEditText[1] = Bitmap.getBitmapResource("bg_edittext_2"+btnTemp+".png");
		Assets.bgEditText[2] = Bitmap.getBitmapResource("bg_edittext_3"+btnTemp+".png");
		
		Assets.bgEditTextFocus = new Bitmap[3];
		Assets.bgEditTextFocus[0] = Bitmap.getBitmapResource("bg_edittext_hover_1"+btnTemp+".png");
		Assets.bgEditTextFocus[1] = Bitmap.getBitmapResource("bg_edittext_hover_2"+btnTemp+".png");
		Assets.bgEditTextFocus[2] = Bitmap.getBitmapResource("bg_edittext_hover_3"+btnTemp+".png");
		
		Assets.bgEditTextError = new Bitmap[3];
		Assets.bgEditTextError[0] = Bitmap.getBitmapResource("bg_edittext_error_1"+btnTemp+".png");
		Assets.bgEditTextError[1] = Bitmap.getBitmapResource("bg_edittext_error_2"+btnTemp+".png");
		Assets.bgEditTextError[2] = Bitmap.getBitmapResource("bg_edittext_error_3"+btnTemp+".png");
		
		Assets.bgEditTextDisable = new Bitmap[3];
		Assets.bgEditTextDisable[0] = Bitmap.getBitmapResource("bg_edittext_isi_1"+btnTemp+".png");
		Assets.bgEditTextDisable[1] = Bitmap.getBitmapResource("bg_edittext_isi_2"+btnTemp+".png");
		Assets.bgEditTextDisable[2] = Bitmap.getBitmapResource("bg_edittext_isi_3"+btnTemp+".png");
		
		Assets.iconDropdown = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("ic_dropdown.png"), Assets.bgEditText[0].getHeight()-25, Assets.bgEditText[0].getHeight()-10, Bitmap.FILTER_LANCZOS, 1);
		Assets.icFavorite = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("ic_fav_inactive.png"), Assets.bgEditText[0].getHeight()-25, Assets.bgEditText[0].getHeight()-10, Bitmap.FILTER_LANCZOS, 1);
		Assets.icFavoriteFocus = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("ic_fav_active.png"), Assets.bgEditText[0].getHeight()-25, Assets.bgEditText[0].getHeight()-10, Bitmap.FILTER_LANCZOS, 1);
		Assets.icSearch = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("ic_search_inactive.png"), Assets.bgEditText[0].getHeight()-25, Assets.bgEditText[0].getHeight()-10, Bitmap.FILTER_LANCZOS, 1);
		Assets.icSearchFocus = BasicUtils.ResizeTransparentBitmap(Bitmap.getBitmapResource("ic_search.png"), Assets.bgEditText[0].getHeight()-25, Assets.bgEditText[0].getHeight()-10, Bitmap.FILTER_LANCZOS, 1);
		
		/* 
		Banks.fontButton = Banks.fontFamily.getFont(Font.BOLD, 30);
    	Banks.fontTitle = Banks.fontFamily.getFont(Font.BOLD, 30);
    	Banks.fontSearchResult2 = Banks.fontFamily.getFont(Font.PLAIN, 20);
    	Banks.fontBookmark1= Banks.fontFamily.getFont(Font.BOLD, 35);
    	Banks.fontBookmark2= Banks.fontFamily.getFont(Font.BOLD, 30);
    	Banks.fontMapsCenter= Banks.fontFamily.getFont(Font.PLAIN, 20);
    	if(Banks.deviceWidth == 360){
    		Banks.fontButton = Banks.fontFamily.getFont(Font.BOLD, 25);
    	}else if(Banks.deviceWidth < 360){
    		Banks.fontGlobal = Banks.fontFamily.getFont(Font.PLAIN, 20);
        	Banks.fontGlobalBold = Banks.fontFamily.getFont(Font.BOLD, 20);
        	Banks.fontButton = Banks.fontFamily.getFont(Font.BOLD, 20);
        	Banks.fontTitle = Banks.fontFamily.getFont(Font.BOLD, 20);
        	Banks.fontSearchResult2 = Banks.fontFamily.getFont(Font.PLAIN, 15);
        	Banks.fontBookmark1= Banks.fontFamily.getFont(Font.BOLD, 25);
        	Banks.fontBookmark2= Banks.fontFamily.getFont(Font.BOLD, 20);
        	Banks.fontMapsCenter= Banks.fontFamily.getFont(Font.PLAIN, 15);
    	}else if(Banks.deviceWidth > 480){
    		
    	}
    	
		Banks.logo = Bitmap.getBitmapResource("logo_superwifi.png");
		Banks.bg_header = Bitmap.getBitmapResource("bg_topheader.png");
		Banks.bg_yellow = Bitmap.getBitmapResource("bg_yellow.png");
		Banks.bg_screen = BasicUtils.resizeBitmap(Bitmap.getBitmapResource("bgscreen480.png"),
				Banks.deviceWidth, Banks.deviceHeight, Bitmap.SCALE_STRETCH);
		
		Banks.icon_search = Bitmap.getBitmapResource("iconpage_search.png");
		Banks.icon_profile = Bitmap.getBitmapResource("iconpage_profile.png");
		Banks.icon_bookmark = Bitmap.getBitmapResource("iconpage_bookmark.png");
		Banks.icon_help = Bitmap.getBitmapResource("iconpage_help.png");
		
		Banks.home_menuBtn = Bitmap.getBitmapResource("btn_listmenu.png");
		Banks.home_menuBtnFocus = Bitmap.getBitmapResource("btn_listmenuhvr.png");
		Banks.home_menu_search = Bitmap.getBitmapResource("menu_search.png");
		Banks.home_menu_profile = Bitmap.getBitmapResource("menu_profile.png");
		Banks.home_menu_update = Bitmap.getBitmapResource("menu_update.png");
		Banks.home_menu_update_new = Bitmap.getBitmapResource("menu_update_new.png");
		Banks.home_menu_bookmark = Bitmap.getBitmapResource("menu_bookmark.png");
		Banks.home_menu_help = Bitmap.getBitmapResource("menu_help.png");
		Banks.home_menu_searchFocus = Bitmap.getBitmapResource("menu_search_hvr.png");
		Banks.home_menu_profileFocus = Bitmap.getBitmapResource("menu_profile_hvr.png");
		Banks.home_menu_updateFocus = Bitmap.getBitmapResource("menu_update_hvr.png");
		Banks.home_menu_update_newFocus = Bitmap.getBitmapResource("menu_update_new_hvr.png");
		Banks.home_menu_bookmarkFocus = Bitmap.getBitmapResource("menu_bookmark_hvr.png");
		Banks.home_menu_helpFocus = Bitmap.getBitmapResource("menu_help_hvr.png");
		
		String btnTemp = "";
		if(Banks.deviceWidth < 360) btnTemp = "_320";
		
		Banks.btn = new Bitmap[7];
		Banks.btn[0] = Bitmap.getBitmapResource("bg_btn_01"+btnTemp+".png");
		Banks.btn[1] = Bitmap.getBitmapResource("bg_btn_02"+btnTemp+".png");
		Banks.btn[2] = Bitmap.getBitmapResource("bg_btn_03"+btnTemp+".png");
		Banks.btn[3] = Bitmap.getBitmapResource("bg_btn_04"+btnTemp+".png");
		Banks.btn[4] = Bitmap.getBitmapResource("bg_btn_05"+btnTemp+".png");
		Banks.btn[5] = Bitmap.getBitmapResource("bg_btn_06"+btnTemp+".png");
		Banks.btn[6] = Bitmap.getBitmapResource("bg_btn_07"+btnTemp+".png");
		
		Banks.iconBtnCurrentLoc = Bitmap.getBitmapResource("icon_currentloc"+btnTemp+".png");
		Banks.iconBtnSearch = Bitmap.getBitmapResource("icon_searchlocation"+btnTemp+".png");
		Banks.iconBtnEdit = Bitmap.getBitmapResource("icon_edit"+btnTemp+".png");
		Banks.iconBtnEditDis = Bitmap.getBitmapResource("icon_edit_disable"+btnTemp+".png");
		Banks.iconBtnReset = Bitmap.getBitmapResource("icon_reset"+btnTemp+".png");
		Banks.iconBtnResetDis = Bitmap.getBitmapResource("icon_reset_disable"+btnTemp+".png");
		Banks.iconBtnSetting = Bitmap.getBitmapResource("icon_setting"+btnTemp+".png");
		Banks.iconBtnSettingDis = Bitmap.getBitmapResource("icon_setting_disable"+btnTemp+".png");
		Banks.iconBtnWifi = Bitmap.getBitmapResource("icon_wifi"+btnTemp+".png");
		Banks.iconBtnWifiDis = Bitmap.getBitmapResource("icon_wifi_disable"+btnTemp+".png");
		Banks.lineWhite = Bitmap.getBitmapResource("line.png");
		Banks.lineOrange = Bitmap.getBitmapResource("line_orange.png");
		
		Banks.bg_searchResult = Bitmap.getBitmapResource("bg_listitem.png");
		Banks.bg_searchResultFocus = Bitmap.getBitmapResource("bg_listitem_hvr.png");
		Banks.btnArrowLeft = Bitmap.getBitmapResource("arrow_left.png");
		Banks.btnArrowLeftFocus = Bitmap.getBitmapResource("arrow_left_hover.png");
		Banks.btnArrowRight = Bitmap.getBitmapResource("arrow_right.png");
		Banks.btnArrowRightFocus = Bitmap.getBitmapResource("arrow_right_hover.png");
		Banks.btnArrowDisable = Bitmap.getBitmapResource("arrow_disable.png");
		Banks.btnPlace = Bitmap.getBitmapResource("icon_placeorange.png");
		Banks.btnPlaceFocus = Bitmap.getBitmapResource("icon_placeorange_hov.png");
		Banks.btnPlaceDisabled = Bitmap.getBitmapResource("icon_placeorange_disable.png");
		Banks.btnStar = Bitmap.getBitmapResource("icon_starorange.png");
		Banks.btnStarFocus = Bitmap.getBitmapResource("icon_starorange_hov.png");
		Banks.btnStarDis = Bitmap.getBitmapResource("icon_stardisable.png");
		Banks.btnStarDisFocus = Bitmap.getBitmapResource("icon_stardisable_hov.png");
	
		Banks.mapIcon = Bitmap.getBitmapResource("map_pointer.png");
		Banks.mapIconDot = Bitmap.getBitmapResource("dot_location.png");
		Banks.mapIconCenter = Bitmap.getBitmapResource("location_pointer.png");
		Banks.mapIconCurrLoc = Bitmap.getBitmapResource("curr_loc.png");
		Banks.mapIconTarget = Bitmap.getBitmapResource("icon_target.png");
		
		Banks.iconPlus = Bitmap.getBitmapResource("icon_plusgreen.png");
		
		Banks.guide0 = Bitmap.getBitmapResource("guide0.png");
		Banks.guide1 = Bitmap.getBitmapResource("guide1.png");
		Banks.guide2 = Bitmap.getBitmapResource("guide2.png");
		Banks.guide3 = Bitmap.getBitmapResource("guide3.png");
		Banks.guide4 = Bitmap.getBitmapResource("guide4.png");
		Banks.guide5 = Bitmap.getBitmapResource("guide5.png");
		
		if(!isPortrait){
			Banks.guide1 = BasicUtils.resizeBitmap(Banks.guide1, Banks.deviceWidth-50, Banks.deviceHeight-50, Bitmap.SCALE_STRETCH);
			Banks.guide2 = BasicUtils.resizeBitmap(Banks.guide2, Banks.deviceWidth-50, Banks.deviceHeight-50, Bitmap.SCALE_STRETCH);
			Banks.guide3 = BasicUtils.resizeBitmap(Banks.guide3, Banks.deviceWidth-50, Banks.deviceHeight-50, Bitmap.SCALE_STRETCH);
			Banks.guide4 = BasicUtils.resizeBitmap(Banks.guide4, Banks.deviceWidth-50, Banks.deviceHeight-50, Bitmap.SCALE_STRETCH);
			Banks.guide5 = BasicUtils.resizeBitmap(Banks.guide5, Banks.deviceWidth-50, Banks.deviceHeight-50, Bitmap.SCALE_STRETCH);
		}else{
			Banks.guide1 = BasicUtils.resizeBitmap(Banks.guide1, Banks.deviceWidth-50, Banks.deviceWidth-75, Bitmap.SCALE_STRETCH);
			Banks.guide2 = BasicUtils.resizeBitmap(Banks.guide2, Banks.deviceWidth-50, Banks.deviceWidth-75, Bitmap.SCALE_STRETCH);
			Banks.guide3 = BasicUtils.resizeBitmap(Banks.guide3, Banks.deviceWidth-50, Banks.deviceWidth-75, Bitmap.SCALE_STRETCH);
			Banks.guide4 = BasicUtils.resizeBitmap(Banks.guide4, Banks.deviceWidth-50, Banks.deviceWidth-75, Bitmap.SCALE_STRETCH);
			Banks.guide5 = BasicUtils.resizeBitmap(Banks.guide5, Banks.deviceWidth-50, Banks.deviceWidth-75, Bitmap.SCALE_STRETCH);
		}
		
		if(Banks.deviceWidth!=480 && Banks.deviceWidth!=360){
			System.out.println(">> Resize image!");
			Banks.bg_header = BasicUtils.ResizeTransparentBitmap(Banks.bg_header, 2, (Banks.deviceHeight/5)-6, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
			Banks.bg_yellow = BasicUtils.ResizeTransparentBitmap(Banks.bg_yellow, 6, (Banks.deviceHeight/5)+8, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
			Banks.logo = BasicUtils.resizeBitmap(Banks.logo, Banks.deviceWidth/4, (Banks.deviceHeight/5)-16, Bitmap.SCALE_TO_FIT);
			
			Banks.icon_search = BasicUtils.ResizeTransparentBitmap(Banks.icon_search, Banks.deviceWidth/7, Banks.deviceWidth/7, Bitmap.FILTER_LANCZOS, 1);
			Banks.icon_profile = BasicUtils.ResizeTransparentBitmap(Banks.icon_profile, Banks.deviceWidth/7, Banks.deviceWidth/7, Bitmap.FILTER_LANCZOS, 1);
			Banks.icon_bookmark = BasicUtils.ResizeTransparentBitmap(Banks.icon_bookmark, Banks.deviceWidth/7, Banks.deviceWidth/7, Bitmap.FILTER_LANCZOS, 1);
			Banks.icon_help = BasicUtils.ResizeTransparentBitmap(Banks.icon_help, Banks.deviceWidth/7, Banks.deviceWidth/7, Bitmap.FILTER_LANCZOS, 1);
			
			Banks.home_menuBtn = BasicUtils.ResizeTransparentBitmap(Banks.home_menuBtn, Banks.deviceWidth/8, Banks.deviceWidth/8, Bitmap.FILTER_LANCZOS, 1);
			Banks.home_menuBtnFocus = BasicUtils.ResizeTransparentBitmap(Banks.home_menuBtnFocus, Banks.deviceWidth/8, Banks.deviceWidth/8, Bitmap.FILTER_LANCZOS, 1);
			Banks.home_menu_search = BasicUtils.ResizeTransparentBitmap(Banks.home_menu_search, Banks.deviceWidth/5, Banks.deviceWidth/5, Bitmap.FILTER_LANCZOS, 1);
			Banks.home_menu_profile = BasicUtils.ResizeTransparentBitmap(Banks.home_menu_profile, Banks.deviceWidth/5, Banks.deviceWidth/5, Bitmap.FILTER_LANCZOS, 1);
			Banks.home_menu_update = BasicUtils.ResizeTransparentBitmap(Banks.home_menu_update, Banks.deviceWidth/5, Banks.deviceWidth/5, Bitmap.FILTER_LANCZOS, 1);
			Banks.home_menu_update_new = BasicUtils.ResizeTransparentBitmap(Banks.home_menu_update_new, Banks.deviceWidth/5, Banks.deviceWidth/5, Bitmap.FILTER_LANCZOS, 1);
			Banks.home_menu_bookmark = BasicUtils.ResizeTransparentBitmap(Banks.home_menu_bookmark, Banks.deviceWidth/5, Banks.deviceWidth/5, Bitmap.FILTER_LANCZOS, 1);
			Banks.home_menu_help = BasicUtils.ResizeTransparentBitmap(Banks.home_menu_help, Banks.deviceWidth/5, Banks.deviceWidth/5, Bitmap.FILTER_LANCZOS, 1);
			Banks.home_menu_searchFocus = BasicUtils.ResizeTransparentBitmap(Banks.home_menu_searchFocus, Banks.deviceWidth/5, Banks.deviceWidth/5, Bitmap.FILTER_LANCZOS, 1);
			Banks.home_menu_profileFocus = BasicUtils.ResizeTransparentBitmap(Banks.home_menu_profileFocus, Banks.deviceWidth/5, Banks.deviceWidth/5, Bitmap.FILTER_LANCZOS, 1);
			Banks.home_menu_updateFocus = BasicUtils.ResizeTransparentBitmap(Banks.home_menu_updateFocus, Banks.deviceWidth/5, Banks.deviceWidth/5, Bitmap.FILTER_LANCZOS, 1);
			Banks.home_menu_update_newFocus = BasicUtils.ResizeTransparentBitmap(Banks.home_menu_update_newFocus, Banks.deviceWidth/5, Banks.deviceWidth/5, Bitmap.FILTER_LANCZOS, 1);
			Banks.home_menu_bookmarkFocus = BasicUtils.ResizeTransparentBitmap(Banks.home_menu_bookmarkFocus, Banks.deviceWidth/5, Banks.deviceWidth/5, Bitmap.FILTER_LANCZOS, 1);
			Banks.home_menu_helpFocus = BasicUtils.ResizeTransparentBitmap(Banks.home_menu_helpFocus, Banks.deviceWidth/5, Banks.deviceWidth/5, Bitmap.FILTER_LANCZOS, 1);
			
			Banks.lineWhite = BasicUtils.ResizeTransparentBitmap(Banks.lineWhite, 2, Banks.deviceWidth/13, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
			Banks.lineOrange = BasicUtils.ResizeTransparentBitmap(Banks.lineOrange, 2, Banks.deviceWidth/13, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);
		
			Banks.mapIconCenter = BasicUtils.ResizeTransparentBitmap(Banks.mapIconCenter, (Banks.deviceWidth*2)/3, Banks.deviceWidth/6, Bitmap.FILTER_LANCZOS, 1);
		}*/
	}
}
