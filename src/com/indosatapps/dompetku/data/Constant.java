package com.indosatapps.dompetku.data;

import java.util.Hashtable;
import java.util.Vector;

import com.indosatapps.dompetku.bean.DompetkuPersistData;

public class Constant {
	public static String TXT_URI = "file:///store/home/user/Databases/Dompetku/";
	public static String version = "0";
	
	public static String txt_error_no_connection = "Koneksi gagal. Cobalah beberapa saat lagi.";
	public static String txt_error_json = "Terjadi kesalahan pada server. Cobalah beberapa saat lagi.";
	public static String txt_category_empty = "Kategori kosong.";
	
	public static int deviceWidth;
	public static int deviceHeight;
	
    public static Vector mListBeli = null;
    public static Vector mListBayar = null;
    public static Vector mListFavorite = new Vector();
    public static Vector mListInbox = new Vector();
    public static Hashtable mHashMapBeli = new Hashtable();
    public static Hashtable mHashMapBayar = new Hashtable();
    public static Hashtable mHashMapFavorite = new Hashtable();
    public static Hashtable mHashMapInbox = new Hashtable();
    public static DompetkuPersistData mPersistData = new DompetkuPersistData();
    
    public static String INTENT_EXTRA_ACCOUNT_ID = "com.indosat.dompetku.accountID";
    public static String INTENT_EXTRA_MENU = "com.indosat.dompetku.menu";
    public static String INTENT_EXTRA_BELI_BAYAR = "com.indosat.dompetku.belibayar";
    public static String INTENT_EXTRA_BELI_BAYAR_CATEGORY_ID = "com.indosat.dompetku.belibayar.categoryID";
    public static String INTENT_EXTRA_BELI_BAYAR_CATEGORY_NAME = "com.indosat.dompetku.belibayar.categoryName";
    public static String INTENT_EXTRA_BELI_BAYAR_MERCHANT_ID = "com.indosat.dompetku.belibayar.merchantID";
    public static String INTENT_EXTRA_BELI_BAYAR_MERCHANT_NAME = "com.indosat.dompetku.belibayar.merchantName";
    public static String INTENT_EXTRA_BELI_BAYAR_FIELD_DROPDOWN_LIST_TYPE = "com.indosat.dompetku.belibayar.dropdownListType";
    public static String INTENT_EXTRA_BELI_BAYAR_FIELD_DROPDOWN_LIST_RESULT = "com.indosat.dompetku.belibayar.dropdownListResult";
    public static String INTENT_EXTRA_BELI_BAYAR_FIELD_DROPDOWN_LIST_FIELD_POS = "com.indosat.dompetku.belibayar.dropdownListFieldPos";
    public static String INTENT_EXTRA_FAVORITE_TEXT = "com.indosat.dompetku.favorite.text";
    public static String INTENT_EXTRA_INBOX_DATA = "com.indosat.dompetku.inbox.data";
    public static String INTENT_EXTRA_PROMO_URL = "com.indosat.dompetku.promoURL";
    public static String INTENT_EXTRA_ABOUT = "com.indosat.dompetku.about";
    public static String INTENT_EXTRA_OTP = "com.indosat.dompetku.otp";
    
    public static int MENU_LIST_TEXT = 0;
    public static int MENU_LIST_TEXT_WITH_ICON = 1;
    public static int MENU_LIST_IMAGE = 2;
    
    public static String FIELD_TYPE_TEXTVIEW = "0";
    public static String FIELD_TYPE_DROPDOWN = "1";
    public static String FIELD_TEXTVIEW_TYPE_NORMAL = "0";
    public static String FIELD_TEXTVIEW_TYPE_CONTACT = "1";
    public static String FIELD_TEXTVIEW_TYPE_CALENDAR = "2";
    public static String FIELD_TEXTVIEW_TYPE_PIN = "3";
    public static String FIELD_TEXTVIEW_TYPE_NUMBER = "4";
    public static String FIELD_TEXTVIEW_TYPE_BANK = "5";
    public static String FIELD_TEXTVIEW_TYPE_STATION = "6";
    public static String FIELD_DROPDOWN_SOURCE_DB = "0";
    public static String FIELD_DROPDOWN_SOURCE_BANK = "1";
    public static String FIELD_DROPDOWN_SOURCE_STATION = "2";
    
    public static int RESULT_OK = 1;
    
    public static int REQUEST_CODE_DROPDOWN_LIST = 50;
    public static int REQUEST_CODE_CONTACT_PICKER = 40;
    public static int REQUEST_CODE_REFRESH_HOME = 55;
    public static int REQUEST_CODE_GANTI_PIN = 60;
    
    public static long PREFERENCE_FILE_KEY = 0x9a83b3dc7da89036L;
    
    public static String PATH_FAVORITE = "favorite";
    public static String PATH_INBOX = "inbox";
    
    public static int colorPageBackground = 0xF2F2F2;
    public static int colorPrimary = 0xf37022;
    public static int colorFontGeneral = 0x262626;
    public static int colorFontFormInput = 0xb4b4b4;
    public static int colorFontFormError = 0xFF0000;
    public static int colorFontHyperlink = 0x989898;
    public static int colorBgHyperlink = 0xDBDBDB;
    
}
