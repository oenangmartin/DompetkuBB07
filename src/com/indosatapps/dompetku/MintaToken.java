package com.indosatapps.dompetku;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.DialogClosedListener;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.text.TextFilter;

import org.json.me.JSONObject;

import com.indosatapps.dompetku.bean.InboxData;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.ButtonFieldCustom;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.PasswordEditFieldCustom;
import com.indosatapps.dompetku.field.PleaseWaitPopupScreen;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.Connection;
import com.indosatapps.dompetku.utils.PageResultListener;

public class MintaToken extends MainScreen implements FieldChangeListener{
	Mediator mediator;
	PageResultListener listener;
	PasswordEditFieldCustom editPIN;
	ButtonFieldCustom btnRequest;
	
	public MintaToken(Mediator mediator, PageResultListener listener) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		this.listener = listener;
		HeaderField header = new HeaderField("Minta Token", 0);
		add(header);
		
		editPIN = new PasswordEditFieldCustom("PIN", "", Constant.deviceWidth-30, 6, FIELD_HCENTER);
		editPIN.setFilter(TextFilter.get(TextFilter.NUMERIC));
		
		btnRequest = new ButtonFieldCustom("KIRIM", Constant.deviceWidth/3, FIELD_HCENTER);
		btnRequest.setEditable(true);
		btnRequest.setMargin(10, 0, 10, 0);
		btnRequest.setChangeListener(this);
		
		VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL);
		scr.setMargin(15, 15, 15, 15);
		scr.add(editPIN.getField());
		scr.add(btnRequest);
		
		add(scr);
	}

	public void fieldChanged(Field field, int arg1) {
		if(field == btnRequest){
			editPIN.setIsError(false);
            String txtPIN = editPIN.getText();
            if(txtPIN.length() == 0){
            	editPIN.setIsError(true);
            	Dialog.alert("Mohon isi PIN");
            }else{
                new DoReqTokenTask(txtPIN);
            }
		}
	}
	
	public class DoReqTokenTask implements Runnable{
		String username, pin;
		
		public DoReqTokenTask(String pin) {
			username = Constant.mPersistData.getUsername();
			this.pin = pin;
			PleaseWaitPopupScreen.showScreenAndWait(this, "Meminta token...");
		}
		
		public void run() {
			final String entity = Connection.doGenerateToken(username, pin);
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject objs = new JSONObject(entity);
                            int status = objs.getInt("status");
                            if(status == 0){
                                String tokenID = objs.getString("couponId");
                                String trxID = objs.getString("trxid");
                                long currTimeMilis = System.currentTimeMillis();
                                long expiredTimeMilis = currTimeMilis + 3600000;
                                String expiredTime = BasicUtils.formatDateFromMilliseconds(expiredTimeMilis, "dd/MM/yyyy HH:mm");
                                InboxData inboxData = new InboxData(trxID, currTimeMilis, "-",
                                        "-", tokenID+" (berlaku s/d "+expiredTime+")", "Minta", "Token", "-", "-", "-");
                                Constant.mListInbox.addElement(inboxData);
                                Constant.mHashMapInbox.put(Long.toString(inboxData.getDate()), inboxData);
                                BasicUtils.saveInboxData();
                                
                                String msg = "Token anda: "+tokenID+" akan kadaluwarsa pada "+expiredTime+". Ref: "+trxID;
                                BasicUtils.showMessageDialog(msg, new DialogClosedListener() {
									public void dialogClosed(Dialog dialog, int choice) {
										if(choice == Dialog.OK){
											listener.onPageResult(0, Constant.RESULT_OK, null);
		                                	close();
										}
									}
								});
                            }else if(status == 1014){
                                Dialog.alert("PIN salah. Mohon periksa kembali PIN Anda.");
                            }else{
                            	Dialog.alert("Transaksi gagal.");
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Dialog.alert(Constant.txt_error_json);
                        }
                    }else{
                    	Dialog.alert(Constant.txt_error_no_connection);
                    }
                }
			});
		}
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }
}
