package com.indosatapps.dompetku;

import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.PageResultListener;

public class Tutorial extends MainScreen{
	PageResultListener listener;
	
	public Tutorial(PageResultListener listener) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createBitmapBackground(Assets.tutorial));
		
		this.listener = listener;
		BasicUtils.saveShowTutorial();
	}
	
	protected boolean keyChar(char character, int status, int time){
		listener.onPageResult(0, Constant.RESULT_OK, null);
		close();
		return super.keyChar(character, status, time);
	}
	
	protected boolean navigationClick(int status, int time) {
		listener.onPageResult(0, Constant.RESULT_OK, null);
		close();
		return true;
	}
	
	protected boolean touchEvent(TouchEvent message){
		listener.onPageResult(0, Constant.RESULT_OK, null);
		close();
		return true;
	}
	
	public boolean onClose(){
		listener.onPageResult(0, Constant.RESULT_OK, null);
		this.close();
    	return true;
    }
}
