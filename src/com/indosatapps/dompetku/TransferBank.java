package com.indosatapps.dompetku;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TouchGesture;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.DialogClosedListener;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.text.NumericTextFilter;
import net.rim.device.api.ui.text.TextFilter;

import org.json.me.JSONObject;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.ButtonFieldCustom;
import com.indosatapps.dompetku.field.EditFieldCustom;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.PasswordEditFieldCustom;
import com.indosatapps.dompetku.field.PleaseWaitPopupScreen;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.Connection;
import com.indosatapps.dompetku.utils.PageResultListener;

public class TransferBank extends MainScreen implements FieldChangeListener, PageResultListener{
	Mediator mediator;
	PageResultListener listener;
	EditFieldCustom editKodeBank, editNoRek, editJumlah;
	PasswordEditFieldCustom editPIN;
	ButtonFieldCustom btnKirim;
	String mKodeBank, mNoRek, mJumlah, mPIN;
	
	public TransferBank(Mediator mediator, PageResultListener listener) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.listener = listener;
		this.mediator = mediator;
		HeaderField header = new HeaderField("Rekening Bank", 0);
		add(header);
		
		EditField editField = new EditField("", "Klik untuk membuka Kode Bank", EditField.DEFAULT_MAXCHARS, FIELD_HCENTER|EditField.NO_NEWLINE){
			protected void paint(Graphics g){
				g.setColor(Constant.colorFontFormInput);
        	    super.paint(g);
        	}
			
			protected boolean navigationClick(int status, int time) {
				UiApplication.getUiApplication().invokeLater(new Runnable() {
	                public void run() {
	                	//go to Bank Code List
	                	TransferBank.this.mediator.app.pushScreen(new ListDropdown(ListDropdown.KODE_BANK, 0, TransferBank.this));
	                }
				});
				return true;
			}
			
			protected boolean touchEvent(TouchEvent message){
				if(message.getEvent()==TouchEvent.GESTURE)
				{
					if(message.getGesture().getEvent()==TouchGesture.TAP)
					{
						TransferBank.this.mediator.app.pushScreen(new ListDropdown(ListDropdown.KODE_BANK, 0, TransferBank.this));
						return true;
					}
				}
				return super.touchEvent(message);
			}
		};
		editField.setEditable(false);
		editField.setFont(Assets.fontGlobal);
		editKodeBank = new EditFieldCustom("Kode Bank", editField, "", Constant.deviceWidth-30, EditField.DEFAULT_MAXCHARS, false, false, FIELD_HCENTER);
		
		editNoRek = new EditFieldCustom("No Rekening Tujuan", "", Constant.deviceWidth-30, 15, false, false, FIELD_HCENTER);
		editNoRek.setFilter(NumericTextFilter.get(NumericTextFilter.NUMERIC));
		editJumlah = new EditFieldCustom("Jumlah", "", Constant.deviceWidth-30, 10, false, false, FIELD_HCENTER);
		editJumlah.setFilter(NumericTextFilter.get(NumericTextFilter.NUMERIC));
		editPIN = new PasswordEditFieldCustom("PIN", "", Constant.deviceWidth-30, 6, FIELD_HCENTER);
		editPIN.setFilter(TextFilter.get(TextFilter.NUMERIC));
		
		btnKirim = new ButtonFieldCustom("KIRIM", Constant.deviceWidth/3, FIELD_HCENTER);
		btnKirim.setEditable(true);
		btnKirim.setMargin(10, 0, 10, 0);
		btnKirim.setChangeListener(this);
		
		VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL);
		scr.setMargin(15, 15, 15, 15);
		scr.add(editKodeBank.getField());
		scr.add(editNoRek.getField());
		scr.add(editJumlah.getField());
		scr.add(editPIN.getField());
		scr.add(btnKirim);
		
		add(scr);
	}
	
	public void onPageResult(int requestCode, int resultCode, Object data) {
		if(requestCode == Constant.REQUEST_CODE_DROPDOWN_LIST){
			if(resultCode == Constant.RESULT_OK){
				//set Bank Code to edittext
				String[] result = (String[]) data;
				editKodeBank.setText(result[0]);
			}
		}
	}
	
	public void resetFieldState(){
		editKodeBank.setIsError(false);
        editNoRek.setIsError(false);
        editJumlah.setIsError(false);
        editPIN.setIsError(false);
    }
	
	public void fieldChanged(Field field, int context) {
		if(field == btnKirim){
			mKodeBank = editKodeBank.getText();
            mNoRek = editNoRek.getText();
            mJumlah = editJumlah.getText();
            mPIN = editPIN.getText();
            
            resetFieldState();
            if(mKodeBank.equals("Klik untuk membuka Kode Bank")){
				editKodeBank.setIsError(true);
            	Dialog.alert("Mohon isi Kode Bank");
			}else if(mNoRek.length() == 0){
                editNoRek.setIsError(true);
                Dialog.alert("Mohon isi Nomor Rekening Tujuan");
            }else if(mJumlah.length() == 0){
                editJumlah.setIsError(true);
                Dialog.alert("Mohon isi Jumlah");
            }else if(mPIN.length() == 0){
                editPIN.setIsError(true);
                Dialog.alert("Mohon isi PIN");
            }else{
            	mKodeBank = mKodeBank.substring(0, 3);
                new DoTransferTask(mKodeBank, mNoRek, mJumlah, mPIN);
            }
		}
	}
	
	public class DoTransferTask implements Runnable{
		String username, pin, bankCode, bankAccNo, amount;
		
		public DoTransferTask(String bankCode, String bankAccNo, String amount, String pin) {
			username = Constant.mPersistData.getUsername();
			this.bankCode = bankCode;
			this.bankAccNo = bankAccNo;
			this.amount = amount;
			this.pin = pin;
			PleaseWaitPopupScreen.showScreenAndWait(this, "Memproses transaksi...");
		}
		
		public void run() {
			final String entity = Connection.doTransferBank(username, pin, bankCode, bankAccNo, amount);
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject objs = new JSONObject(entity);
                            int status = objs.getInt("status");
                            if(status == 0){
                        		/*String trxID = objs.getString("trxid");
                                long currTimeMilis = System.currentTimeMillis();
                                String currTime = BasicUtils.formatDateFromMilliseconds(currTimeMilis, "dd/MM/yyyy HH:mm");
                                InboxData inboxData = new InboxData(trxID, currTimeMilis, editJumlah.getText(),
                                        editNoTujuan.getText(), "-", "Kirim", "Uang", "-", "-", "-");
                                Constant.mListInbox.addElement(inboxData);
                                Constant.mHashMapInbox.put(Long.toString(inboxData.getDate()), inboxData);
                                BasicUtils.saveInboxData();*/
                                
                            	String msg = "Anda akan menerima SMS berisi cara konfirmasi transfer ke rekening bank dari 789";
                                BasicUtils.showMessageDialog(msg, new DialogClosedListener() {
									public void dialogClosed(Dialog dialog, int choice) {
										if(choice == Dialog.OK){
											listener.onPageResult(0, Constant.RESULT_OK, null);
		                                	close();
										}
									}
								});
                            }else if(status == 1001){
                            	Dialog.alert("Saldo Anda tidak mencukupi untuk melakukan transaksi ini.");
                            }else if(status == 1014){
                            	Dialog.alert("PIN salah. Mohon periksa kembali PIN Anda.");
                            }else if(status == 23){
                            	Dialog.alert("Jumlah uang yang ditransfer kurang dari jumlah minimum  transaksi.");
                            }else if(status == 24){
                            	Dialog.alert("Jumlah terlalu besar");
                            }else{
                            	Dialog.alert("Transaksi gagal.");
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Dialog.alert(Constant.txt_error_json);
                        }
                    }else{
                    	Dialog.alert(Constant.txt_error_no_connection);
                    }
                }
			});
		}
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }
}
