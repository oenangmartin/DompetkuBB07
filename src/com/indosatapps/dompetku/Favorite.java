package com.indosatapps.dompetku;

import java.util.Enumeration;

import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.util.Comparator;
import net.rim.device.api.util.SimpleSortingVector;

import com.indosatapps.dompetku.bean.FavoriteData;
import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.LabelFieldColor;
import com.indosatapps.dompetku.field.ListFavoriteField;
import com.indosatapps.dompetku.utils.BasicUtils;

public class Favorite extends MainScreen{
	Mediator mediator;
	VerticalFieldManager scr;
	LabelFieldColor txtEmpty;
	SimpleSortingVector sortingVector = new SimpleSortingVector();
	
	public Favorite(Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
        HeaderField header = new HeaderField("Favorit", 0);
		add(header);
		
		txtEmpty = new LabelFieldColor("Tidak ada favorit", Constant.colorFontGeneral, USE_ALL_WIDTH|DrawStyle.HCENTER);
        txtEmpty.setFont(Assets.fontGlobal);
        int margin = ((Constant.deviceHeight-header.getPreferredHeight()) - txtEmpty.getPreferredHeight()) / 2;
        txtEmpty.setMargin(margin-15, 0, 0, 0);
		
		scr = new VerticalFieldManager(USE_ALL_WIDTH|USE_ALL_HEIGHT|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 15, 15, 15);
		add(scr);
		
		initFavoriteList();
	}
	
	public void initFavoriteList(){
		if(Constant.mHashMapFavorite.size()>0){
            sortFavorite();
            scr.deleteAll();
            for (Enumeration e = sortingVector.elements(); e.hasMoreElements();){
                String key = (String) e.nextElement();
                final FavoriteData sortedFav = (FavoriteData) Constant.mHashMapFavorite.get(key);

                // Do whatever you have to with the sortedItem
                ListFavoriteField favField = new ListFavoriteField(sortedFav, 0);
                favField.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						FavoriteDialog d = new FavoriteDialog(mediator, sortedFav);
						d.doModal();
					}
				});
                scr.add(favField);
            }
            scr.invalidate();
        }else{
        	scr.deleteAll();
            scr.add(txtEmpty);
            scr.invalidate();
        }
	}
	
	public void sortFavorite(){
		sortingVector.removeAllElements();
		for (Enumeration e = Constant.mHashMapFavorite.keys(); e.hasMoreElements();){
            String key = (String) e.nextElement();
            sortingVector.addElement(key);
        }

        // Pass in the comparator and sort the keys
        sortingVector.setSortComparator(new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((((String) o1).compareTo((String) o2)));
			}
		});
        sortingVector.reSort();
	}
	
	class FavoriteDialog extends Dialog{
		ButtonField btnDetail, btnDelete;
		
	    public FavoriteDialog(final Mediator mediator, final FavoriteData favData){
	        super(null, null, null, Dialog.DISCARD, null, Dialog.GLOBAL_STATUS);
	        
	        VerticalFieldManager vfm = new VerticalFieldManager(FIELD_HCENTER);

	        btnDetail = new ButtonField("Buka Favorit", FIELD_HCENTER);
	        btnDetail.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					//Go to BeliBayarFields
					mediator.app.pushScreen(new BeliBayarFields(favData.getMenu(), favData.getCategory(),
							favData.getCategoryID(), favData.getMerchantID(), favData.getMerchant(),
							favData.getFavoriteText(), null, mediator));
					close();
				}
			});
	        
	        btnDelete = new ButtonField("Hapus", FIELD_HCENTER);
	        btnDelete.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					int d = Dialog.ask("Hapus dari Favorit?", new String[]{"Hapus", "Batal"}, new int[]{1,0}, 0);
					if(d==1){
						BasicUtils.deleteFavoriteData(favData.getFavoriteID());
	                    initFavoriteList();
	                    
	                    Dialog.inform("Favorit berhasil dihapus!");
					}
					close();
				}
			});
	        
	        
		    vfm.add(btnDetail);
		    vfm.add(btnDelete);
	
		    this.add(vfm);
	    }
	    
	    public boolean keyChar(char key, int status, int time) {
	    	if(key==Characters.ESCAPE){
	    		this.close();
	    	}
	        return true;
	     }
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }
}
