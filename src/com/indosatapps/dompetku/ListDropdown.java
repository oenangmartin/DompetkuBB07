package com.indosatapps.dompetku;

import java.util.Vector;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.indosatapps.dompetku.bean.KodeBankData;
import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.LabelFieldColor;
import com.indosatapps.dompetku.field.ListItemField;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.PageResultListener;

public class ListDropdown extends MainScreen implements FieldChangeListener{
	public static String KODE_BANK = "Kode Bank";
    public static String STASIUN = "Stasiun KRL";
    PageResultListener listener;
	EditField editSearch;
	LabelFieldColor txtEmpty;
	VerticalFieldManager scr;
	String type, pos;
	Vector mListDropdown = new Vector();
	
	public ListDropdown(String type, int pos, PageResultListener listener) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.listener = listener;
		this.type = type;
		this.pos = Integer.toString(pos);
		HeaderField header = new HeaderField(type, 0);
		add(header);
		
		txtEmpty = new LabelFieldColor("Pencarian tidak ditemukan.", Constant.colorFontGeneral, USE_ALL_WIDTH|DrawStyle.HCENTER);
        txtEmpty.setFont(Assets.fontGlobal);
        int margin = ((Constant.deviceHeight-header.getPreferredHeight()) - txtEmpty.getPreferredHeight()) / 2;
        txtEmpty.setMargin(margin-15, 0, 0, 0);
		
		editSearch = new EditField("", "", 10, EditField.NO_NEWLINE){
			protected void paint(Graphics g){
				g.setColor(Constant.colorFontFormInput);
        	    super.paint(g);
        	}
		};
		editSearch.setFont(Assets.fontGlobal);
		editSearch.setChangeListener(this);
		
		HorizontalFieldManager hfm = new HorizontalFieldManager(HorizontalFieldManager.HORIZONTAL_SCROLL);
		hfm.setMargin((Assets.bgEditText[1].getHeight()-Assets.fontGlobal.getHeight())/2,
						Assets.bgEditText[0].getWidth()+Assets.icSearchFocus.getWidth()+17,
						(Assets.bgEditText[1].getHeight()-Assets.fontGlobal.getHeight())/2,
						Assets.bgEditText[2].getWidth());
		hfm.add(editSearch);
		
		HorizontalFieldManager finalEditField = new HorizontalFieldManager(){
			protected void paint(Graphics g) {
				Bitmap[] bg = editSearch.isFocus()?Assets.bgEditTextFocus:Assets.bgEditText;
				
				//draw the left side
				g.drawBitmap(0, 0, bg[0].getWidth(), bg[0].getHeight(), bg[0], 0, 0);
				
				//draw the middle side
				int ulang = Constant.deviceWidth - 30 - (bg[0].getWidth()+bg[2].getWidth());
				for(int i=0 ; i<ulang ; i+=bg[1].getWidth()){
					g.drawBitmap(bg[0].getWidth()+i, 0, bg[1].getWidth(), bg[1].getHeight(), bg[1], 0, 0);
				}
				
				//draw the right side
				g.drawBitmap(bg[0].getWidth()+ulang, 0, bg[2].getWidth(), bg[2].getHeight(), bg[2], 0, 0);
				
				//draw icon
				g.drawBitmap(Constant.deviceWidth-30-Assets.icSearchFocus.getWidth()-12, (bg[0].getHeight()-Assets.icSearchFocus.getHeight())/2,
						Assets.icSearchFocus.getWidth(), Assets.icSearchFocus.getHeight(), Assets.icSearchFocus, 0, 0);
				
				super.paint(g);
			}
		};
		finalEditField.add(hfm);
		
		VerticalFieldManager finalField = new VerticalFieldManager();
		finalField.add(finalEditField);
		finalField.setMargin(0, 15, 0, 15);
		
		scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 15, 15, 15);
		
		//init();
		if(type.equals(KODE_BANK)){
            mListDropdown = BasicUtils.readListKodeBank(getClass());
        }else if(type.equals(STASIUN)){
        	mListDropdown = BasicUtils.readListStasiun(getClass());
        }
		refreshList(mListDropdown);
		
		add(finalField);
		add(scr);
	}
	
	public void fieldChanged(Field field, int context) {
		if(field==editSearch){
			String keyword = editSearch.getText().toLowerCase();
			if(!keyword.equals("")){
				System.out.println(">> Searching for: "+keyword);
				Vector result = search(mListDropdown, keyword);
				refreshList(result);
			}else{
				System.out.println("keyword empty");
				refreshList(mListDropdown);
			}
		}
	}
	
	public Vector search(Vector v, String keyword){
		Vector result = new Vector();
		if(!keyword.equals("")){
			result.removeAllElements();
			String temp = "";
			KodeBankData data = null;
			for(int j=0;j<v.size();j++){
				if(type.equals(KODE_BANK)){
					data = (KodeBankData) v.elementAt(j);
		            temp = data.kode+" "+data.nama;
				}else if(type.equals(STASIUN)){
					temp = v.elementAt(j).toString();
				}
				
				//System.out.println(">>Comparing "+temp+" :");
		        String[] split = BasicUtils.split(temp, " ");
				for(int i=0;i<split.length;i++){
					//System.out.println(">>	Compare "+keyword+" with "+split[i]);
			        if(split[i].toLowerCase().startsWith(keyword)){
			        	//System.out.println(">>	Match found: "+temp);
			        	if(type.equals(KODE_BANK)) result.addElement(data);
			        	else if(type.equals(STASIUN)) result.addElement(temp);
			        	break;
			        }
				}
		    }
		}else{
			result = v;
		}
		
		return result;
	}
	
	public void refreshList(Vector vector){
		scr.deleteAll();
		if(vector.size()>0){
			String temp = "";
			for(int i=0;i<vector.size();i++){
				if(type.equals(KODE_BANK)){
					KodeBankData data = (KodeBankData) vector.elementAt(i);
		            temp = data.kode+" "+data.nama;
				}else if(type.equals(STASIUN)){
					temp = vector.elementAt(i).toString();
				}
				final String item = temp;
				
	            ListItemField list = new ListItemField(item, Constant.deviceWidth-30, 0);
	            list.setChangeListener(new FieldChangeListener() {
					public void fieldChanged(Field field, int context) {
						listener.onPageResult(Constant.REQUEST_CODE_DROPDOWN_LIST, Constant.RESULT_OK, new String[]{item, pos});
						close();
					}
				});
	            scr.add(list);
	        }
		}else{
			scr.add(txtEmpty);
		}
		scr.invalidate();
	}
	
	protected boolean keyChar(char key, int status, int time) {
		char temp = Character.toLowerCase(key);
		if(temp=='a' || temp=='b' || temp=='c' || temp=='d' || temp=='e' || temp=='f' || temp=='g' ||
				temp=='h' || temp=='i' || temp=='j' || temp=='k' || temp=='l' || temp=='m' || temp=='n' ||
				temp=='o' || temp=='p' || temp=='q' || temp=='r' || temp=='s' || temp=='t' || temp=='u' ||
				temp=='v' || temp=='w' || temp=='x' || temp=='y' || temp=='z' || key==Characters.BACKSPACE){
			System.out.println(">>keypress: "+temp);
    		editSearch.setFocus();
    	}
		return super.keyChar(key, status, time);
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }
}
