package com.indosatapps.dompetku;

import org.json.me.JSONObject;

import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.TouchGesture;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.text.TextFilter;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.ButtonFieldCustom;
import com.indosatapps.dompetku.field.EditFieldCustom;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.LabelFieldColor;
import com.indosatapps.dompetku.field.PasswordEditFieldCustom;
import com.indosatapps.dompetku.field.PleaseWaitPopupScreen;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.Connection;
import com.thirdparty.device.api.ui.container.JustifiedVerticalFieldManager;

public class Login extends MainScreen implements FieldChangeListener{
	Mediator mediator;
	EditFieldCustom editMSISDN;
	PasswordEditFieldCustom editPIN;
	ButtonFieldCustom btnLogin;
	LabelField btnLupaPIN;
	
	public Login(Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		HeaderField header = new HeaderField("", 0);
		
		LabelFieldColor label1 = new LabelFieldColor("No HP", Constant.colorFontGeneral, NON_FOCUSABLE|FIELD_LEFT|FIELD_VCENTER);
		LabelFieldColor label2 = new LabelFieldColor("PIN", Constant.colorFontGeneral, NON_FOCUSABLE|FIELD_LEFT|FIELD_VCENTER);
		label1.setFont(Assets.fontGlobal);
		label2.setFont(Assets.fontGlobal);
		
		editMSISDN = new EditFieldCustom("", "", Constant.deviceWidth/2, 16, false, false, FIELD_VCENTER);
		editPIN = new PasswordEditFieldCustom("", "", Constant.deviceWidth/2, 6, FIELD_VCENTER);
		editMSISDN.setFilter(TextFilter.get(TextFilter.PHONE));
		editPIN.setFilter(TextFilter.get(TextFilter.NUMERIC));
		
		label1.setMargin(0, (Constant.deviceWidth/3)-Assets.fontGlobal.getAdvance("No HP")-10, 0, 10);
		label2.setMargin(0, (Constant.deviceWidth/3)-Assets.fontGlobal.getAdvance("PIN")-10, 0, 10);
		
		HorizontalFieldManager line1 = new HorizontalFieldManager(USE_ALL_WIDTH);
		line1.add(label1);
		line1.add(editMSISDN.getField());
		
		HorizontalFieldManager line2 = new HorizontalFieldManager(USE_ALL_WIDTH);
		line2.add(label2);
		line2.add(editPIN.getField());
		
		btnLogin = new ButtonFieldCustom("MASUK", Constant.deviceWidth/3, FIELD_HCENTER);
		btnLogin.setEditable(true);
		btnLogin.setMargin(10, 0, 10, 0);
		btnLogin.setChangeListener(this);
		
		btnLupaPIN = new LabelField("Lupa PIN?", FOCUSABLE|FIELD_HCENTER){
			protected void paint(Graphics g) {
				if(isFocus()){
					g.setColor(Constant.colorBgHyperlink);
					g.drawRoundRect(0, 0, getWidth(), getHeight(), 10, 10);
					g.fillRoundRect(0, 0, getWidth(), getHeight(), 10, 10);
				}
				g.setColor(Constant.colorFontHyperlink);
				super.paint(g);
			}
			
			protected void drawFocus(Graphics g, boolean bo){}
			
			protected void onFocus(int direction){
				super.onFocus(direction);
				invalidate();
			}
			
			protected void onUnfocus(){
				super.onUnfocus();
				invalidate();
			}
			
			protected boolean keyChar(char character, int status, int time)
			{
				if(character==Characters.ENTER){
					fieldChangeNotify(0);
					return true;
				}
				return super.keyChar(character, status, time);
			}
			
			protected boolean navigationClick(int status, int time)
			{
				fieldChangeNotify(0);
				return true;
			}
			
			protected boolean touchEvent(TouchEvent message)
			{
				if(message.getEvent()==TouchEvent.GESTURE)
				{
					if(message.getGesture().getEvent()==TouchGesture.TAP)
					{
						fieldChangeNotify(0);
						return true;
					}
				}
				return super.touchEvent(message);
			}
		};
		btnLupaPIN.setFont(Assets.fontGlobal);
		btnLupaPIN.setChangeListener(this);
		
		VerticalFieldManager vfm = new VerticalFieldManager(USE_ALL_WIDTH|FIELD_VCENTER);
		vfm.setPadding(10, 10, 10, 10);
		vfm.add(line1);
		vfm.add(line2);
		vfm.add(btnLogin);
		vfm.add(btnLupaPIN);
		
		JustifiedVerticalFieldManager scr = new JustifiedVerticalFieldManager(header, vfm, null, USE_ALL_WIDTH|USE_ALL_HEIGHT);
		add(scr);
		
		String username = Constant.mPersistData.getUsername();
        if(username!=""){
            editMSISDN.setText(username);
            editMSISDN.setEditable(false);
        }
	}
	
	public boolean onClose(){
		System.exit(0);
    	return true;
    }

	public void fieldChanged(Field field, int context) {
		if(field == btnLogin){
            String txtMSISDN = editMSISDN.getText();
            String txtPIN = editPIN.getText();
            
            resetFieldState();
            if(txtMSISDN.length() == 0){
                editMSISDN.setIsError(true);
                Dialog.alert("Mohon isi No HP");
            }else if(txtPIN.length() == 0){
                editPIN.setIsError(true);
                Dialog.alert("Mohon isi PIN");
            }else{
            	PleaseWaitPopupScreen.showScreenAndWait(new DoLoginTask(txtMSISDN, txtPIN), "Logging in...");
            }
		}else if(field == btnLupaPIN){
			mediator.app.pushScreen(new LupaPIN(mediator));
		}
	}

	public void resetFieldState(){
        editMSISDN.setIsError(false);
        editPIN.setIsError(false);
    }
	
	public void saveUserPref(String username){
        BasicUtils.saveUsernamePref(username);
        BasicUtils.saveLastSession();
    }
	
	
	public class DoLoginTask implements Runnable{
		String username, pin;
		
		public DoLoginTask(String username, String pin) {
			this.username = username;
			this.pin = pin;
		}
		
		public void run() {
			final String entity = Connection.doLogin(username, pin);
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                    if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject obj = new JSONObject(entity);
                            int status = obj.getInt("status");
                        	if(status == 0){
                                saveUserPref(editMSISDN.getText());
                                mediator.app.pushScreen(new Home(mediator));
                                close();
                            }else if(status == 1012){
                                Dialog.alert("Pin anda telah terblokir. Silahkan menghubungi Customer Service Indosat di 100/111.");
                            }else{
                            	Dialog.alert("No HP atau PIN salah. Periksa kembali No HP dan PIN Anda.");
                            }
                        }catch(Exception e){
                            e.printStackTrace();
                            Dialog.alert(Constant.txt_error_json);
                        }
                    }else{
                    	Dialog.alert(Constant.txt_error_no_connection);
                    }
                }
			});
		}
	}
}
