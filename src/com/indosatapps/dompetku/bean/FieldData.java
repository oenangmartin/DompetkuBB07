package com.indosatapps.dompetku.bean;

public class FieldData {
    String kategori_id;
    String kategori_nama;
    String sub_merchant_id;
    String sub_merchant_nama;
    String field_id;
    String param_name;
    String label;
    String order;
    String field_tipe;
    String textview_type;
    String isi_dropdown;
    String id_isi_dropdown;
    String dropdown_src;
    String is_favorite;
    
    public String getKategori_id() {
        return kategori_id;
    }
    public void setKategori_id(String kategori_id) {
        this.kategori_id = kategori_id;
    }
    public String getKategori_nama() {
        return kategori_nama;
    }
    public void setKategori_nama(String kategori_nama) {
        this.kategori_nama = kategori_nama;
    }
    public String getSub_merchant_id() {
        return sub_merchant_id;
    }
    public void setSub_merchant_id(String sub_merchant_id) {
        this.sub_merchant_id = sub_merchant_id;
    }
    public String getSub_merchant_nama() {
        return sub_merchant_nama;
    }
    public void setSub_merchant_nama(String sub_merchant_nama) {
        this.sub_merchant_nama = sub_merchant_nama;
    }
    public String getField_id() {
        return field_id;
    }
    public void setField_id(String field_id) {
        this.field_id = field_id;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public String getOrder() {
        return order;
    }
    public void setOrder(String order) {
        this.order = order;
    }
    public String getField_tipe() {
        return field_tipe;
    }
    public void setField_tipe(String field_tipe) {
        this.field_tipe = field_tipe;
    }
    public String getTextview_type() {
        return textview_type;
    }
    public void setTextview_type(String textview_type) {
        this.textview_type = textview_type;
    }
    public String getIsi_dropdown() {
        return isi_dropdown;
    }
    public void setIsi_dropdown(String isi_dropdown) {
        this.isi_dropdown = isi_dropdown;
    }
    public String getDropdown_src() {
        return dropdown_src;
    }
    public void setDropdown_src(String dropdown_src) {
        this.dropdown_src = dropdown_src;
    }
    public String getIs_favorite() {
        return is_favorite;
    }
    public void setIs_favorite(String is_favorite) {
        this.is_favorite = is_favorite;
    }
    public String getParam_name() {
        return param_name;
    }
    public void setParam_name(String param_name) {
        this.param_name = param_name;
    }
    public String getId_isi_dropdown() {
        return id_isi_dropdown;
    }
    public void setId_isi_dropdown(String id_isi_dropdown) {
        this.id_isi_dropdown = id_isi_dropdown;
    }
}
