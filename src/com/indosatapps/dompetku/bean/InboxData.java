package com.indosatapps.dompetku.bean;


public class InboxData{
    String trxID;
    long date;
    String amount;
    String to;
    String additional;
    String menu;
    String category;
    String categoryID;
    String merchant;
    String merchantID;
    
    public InboxData(String trxID, long date, String amount, String to, String additional, String menu, String category, String categoryID,
            String merchant, String merchantID) {
        setTrxID(trxID);
        setDate(date);
        setAmount(amount);
        setTo(to);
        setAdditional(additional);
        setMenu(menu);
        setCategory(category);
        setCategoryID(categoryID);
        setMerchant(merchant);
        setMerchantID(merchantID);
    }
    
    public String getMenu() {
        return menu;
    }
    public void setMenu(String menu) {
        this.menu = menu;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getCategoryID() {
        return categoryID;
    }
    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }
    public String getMerchant() {
        return merchant;
    }
    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }
    public String getMerchantID() {
        return merchantID;
    }
    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTrxID() {
        return trxID;
    }

    public void setTrxID(String trxID) {
        this.trxID = trxID;
    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }
}
