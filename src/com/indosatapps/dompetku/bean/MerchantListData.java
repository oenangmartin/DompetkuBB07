package com.indosatapps.dompetku.bean;

public class MerchantListData {
    String list_merchant_nama;
    String list_merchant_img;
    String list_merchant_url;
    
    public String getList_merchant_nama() {
        return list_merchant_nama;
    }
    public void setList_merchant_nama(String list_merchant_nama) {
        this.list_merchant_nama = list_merchant_nama;
    }
    public String getList_merchant_img() {
        return list_merchant_img;
    }
    public void setList_merchant_img(String list_merchant_img) {
        this.list_merchant_img = list_merchant_img;
    }
    public String getList_merchant_url() {
        return list_merchant_url;
    }
    public void setList_merchant_url(String list_merchant_url) {
        this.list_merchant_url = list_merchant_url;
    }

}
