package com.indosatapps.dompetku.bean;

public class FavoriteData {
    String favoriteID;
    String favoriteText;
    String menu;
    String category;
    String categoryID;
    String merchant;
    String merchantID;
    String paramName;
    
    public FavoriteData(String favoriteID, String favoriteText, String menu, String category, String categoryID, String merchant, 
            String merchantID, String paramName) {
        setFavoriteID(favoriteID);
        setFavoriteText(favoriteText);
        setMenu(menu);
        setCategory(category);
        setCategoryID(categoryID);
        setMerchant(merchant);
        setMerchantID(merchantID);
        setParamName(paramName);
    }
    
    public String getFavoriteID() {
        return favoriteID;
    }
    public void setFavoriteID(String favoriteID) {
        this.favoriteID = favoriteID;
    }
    public String getMenu() {
        return menu;
    }
    public void setMenu(String menu) {
        this.menu = menu;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getCategoryID() {
        return categoryID;
    }
    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }
    public String getMerchant() {
        return merchant;
    }
    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }
    public String getMerchantID() {
        return merchantID;
    }
    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }
    public String getFavoriteText() {
        return favoriteText;
    }
    public void setFavoriteText(String favoriteText) {
        this.favoriteText = favoriteText;
    }
    public String getParamName() {
        return paramName;
    }
    public void setParamName(String paramName) {
        this.paramName = paramName;
    }
    
}
