package com.indosatapps.dompetku.bean;

import java.util.Vector;

public class CategoryData {
    String mid;
    String mnama;
    String mimage;
    Vector sub_merchant;
    
    public CategoryData(String mid, String mnama, String mimage, Vector sub_merchant) {
		setMid(mid);
		setMnama(mnama);
		setMimage(mimage);
		setSub_merchant(sub_merchant);
	}
    
    public String getMid() {
        return mid;
    }
    public void setMid(String mid) {
        this.mid = mid;
    }
    public String getMnama() {
        return mnama;
    }
    public void setMnama(String mnama) {
        this.mnama = mnama;
    }
    public String getMimage() {
        return mimage;
    }
    public void setMimage(String mimage) {
        this.mimage = mimage;
    }
    public Vector getSub_merchant() {
        return sub_merchant;
    }
    public void setSub_merchant(Vector sub_merchant) {
        this.sub_merchant = sub_merchant;
    }
}
