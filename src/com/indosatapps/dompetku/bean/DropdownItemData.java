package com.indosatapps.dompetku.bean;

public class DropdownItemData {
    String id;
    String label;
    
    public DropdownItemData(String id, String label) {
        setId(id);
        setLabel(label);
    }
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
}
