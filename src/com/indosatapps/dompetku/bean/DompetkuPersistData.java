package com.indosatapps.dompetku.bean;

import java.util.Hashtable;

import net.rim.device.api.util.Persistable;

public class DompetkuPersistData implements Persistable{
	private String username;
	private String lastSession;
	private String isFirstInstall;
	private String isShowTutorial;
	private String isRegisPending;
	private Hashtable imageCache;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getLastSession() {
		return lastSession;
	}
	public void setLastSession(String lastSession) {
		this.lastSession = lastSession;
	}
	public String getIsFirstInstall() {
		return isFirstInstall;
	}
	public void setIsFirstInstall(String isFirstInstall) {
		this.isFirstInstall = isFirstInstall;
	}
	public String getIsShowTutorial() {
		return isShowTutorial;
	}
	public void setIsShowTutorial(String isShowTutorial) {
		this.isShowTutorial = isShowTutorial;
	}
	public void setIsRegisPending(String isRegisPending) {
		this.isRegisPending = isRegisPending;
	}
	public String getIsRegisPending() {
		return isRegisPending;
	}
	public void setImageCache(Hashtable imageCache) {
		this.imageCache = imageCache;
	}
	public Hashtable getImageCache() {
		return imageCache;
	}
}
