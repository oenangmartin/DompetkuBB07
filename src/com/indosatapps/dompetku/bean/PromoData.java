package com.indosatapps.dompetku.bean;

public class PromoData {
    String promo_id;
    String promo_text;
    String promo_url;
    
    public PromoData(String promo_id, String promo_text, String promo_url) {
        setPromo_id(promo_id);
        setPromo_text(promo_text);
        setPromo_url(promo_url);
    }
    
    public String getPromo_id() {
        return promo_id;
    }
    public void setPromo_id(String promo_id) {
        this.promo_id = promo_id;
    }
    public String getPromo_text() {
        return promo_text;
    }
    public void setPromo_text(String promo_text) {
        this.promo_text = promo_text;
    }
    public String getPromo_url() {
        return promo_url;
    }
    public void setPromo_url(String promo_url) {
        this.promo_url = promo_url;
    }
}
