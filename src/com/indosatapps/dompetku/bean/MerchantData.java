package com.indosatapps.dompetku.bean;

public class MerchantData {
    String sub_merchant_id;
    String sub_merchant_nama;
    String img_path;
    
    public MerchantData(String sub_merchant_id, String sub_merchant_nama, String img_path) {
		this.sub_merchant_id = sub_merchant_id;
		this.sub_merchant_nama = sub_merchant_nama;
		this.img_path = img_path;
	}
    
    public String getSub_merchant_id() {
        return sub_merchant_id;
    }
    public void setSub_merchant_id(String sub_merchant_id) {
        this.sub_merchant_id = sub_merchant_id;
    }
    public String getSub_merchant_nama() {
        return sub_merchant_nama;
    }
    public void setSub_merchant_nama(String sub_merchant_nama) {
        this.sub_merchant_nama = sub_merchant_nama;
    }
    public String getImg_path() {
        return img_path;
    }
    public void setImg_path(String img_path) {
        this.img_path = img_path;
    }
}
