package com.indosatapps.dompetku.bean;

public class TransactionData {
    String date;
    int transid;
    String type;
    int amount;
    String agent;
    
    public TransactionData(String date, int transid, String type, int amount, String agent) {
		setDate(date);
		setTransid(transid);
		setType(type);
		setAmount(amount);
		setAgent(agent);
	}
    
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public int getTransid() {
        return transid;
    }
    public void setTransid(int transid) {
        this.transid = transid;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }
    public String getAgent() {
        return agent;
    }
    public void setAgent(String agent) {
        this.agent = agent;
    }
}
