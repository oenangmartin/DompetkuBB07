package com.indosatapps.dompetku.bean;

public class KodeBankData {
    public String kode;
    public String nama;
    
    public KodeBankData(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }
}
