package com.indosatapps.dompetku;

import java.util.Vector;

import net.rim.device.api.system.GIFEncodedImage;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import org.json.me.JSONArray;
import org.json.me.JSONObject;

import com.indosatapps.dompetku.bean.TransactionData;
import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.AnimatedGIFField;
import com.indosatapps.dompetku.field.ButtonFieldCustom;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.LabelFieldColor;
import com.indosatapps.dompetku.field.ListHistoryTrxField;
import com.indosatapps.dompetku.utils.Connection;

public class HistoriTransaksi extends MainScreen implements FieldChangeListener{
	Mediator mediator;
	Vector mListHistory;
	AnimatedGIFField _ourAnimation;
	VerticalFieldManager scr, layoutError;
	ButtonFieldCustom btnRetry;
	HeaderField header;
	LabelFieldColor txtError;
	
	public HistoriTransaksi(Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		header = new HeaderField("Histori Transaksi", 0);
		add(header);
		
		mListHistory = new Vector();
		GIFEncodedImage ourAnimation = (GIFEncodedImage) GIFEncodedImage.getEncodedImageResource("loading_orange.gif");
        _ourAnimation = new AnimatedGIFField(ourAnimation, FIELD_VCENTER|FIELD_HCENTER);
        int topMargin = ((Constant.deviceHeight-header.getPreferredHeight()) - _ourAnimation.getPreferredHeight()) / 2;
        _ourAnimation.setMargin(topMargin-15, 0, 0, 0);
        
        txtError = new LabelFieldColor("", Constant.colorFontGeneral, USE_ALL_WIDTH|DrawStyle.HCENTER);
        txtError.setFont(Assets.fontGlobal);
        btnRetry = new ButtonFieldCustom("COBA LAGI", Constant.deviceWidth/3, FIELD_HCENTER);
        btnRetry.setEditable(true);
        btnRetry.setMargin(5, 0, 0, 0);
        btnRetry.setChangeListener(this);
        layoutError = new VerticalFieldManager(USE_ALL_WIDTH);
        
        scr = new VerticalFieldManager(USE_ALL_WIDTH|USE_ALL_HEIGHT|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 15, 15, 15);
		scr.add(_ourAnimation);
		
		add(scr);
		
		mediator.app.invokeLater(new Runnable() {
			public void run() {
				new DoHistoryTrxTask().run();
			}
		});
	}
	
	public class DoHistoryTrxTask implements Runnable{
		String username;
		
		public DoHistoryTrxTask() {
			username = Constant.mPersistData.getUsername();
			//PleaseWaitPopupScreen.showScreenAndWait(this, "Mengambil transaksi...");
		}
		
		public void run() {
			final String entity = Connection.doGetHistoryTrx(username, "20");
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject objs = new JSONObject(entity);
                        	int status = objs.getInt("status");
                            if(status == 0){
                            	JSONArray array = objs.getJSONArray("trxList");
	                        	if(array.length()>0){
	                        		scr.deleteAll();
	                        		for(int i=0;i<array.length();i++){
	                        			//{"date":"18/2/2015 11:29","transid":26197304,"type":"transfer","amount":-5000,"agent":"085771042074"}
	                        			JSONObject data = array.getJSONObject(i);
	                        			String date = data.getString("date");
	                        			int trxID = data.getInt("transid");
	                        			String type = data.getString("type");
	                        			int amount = data.getInt("amount");
	                        			String agent = data.getString("agent");
	                        			TransactionData trxData = new TransactionData(date, trxID, type, amount, agent);
	                        			ListHistoryTrxField trxField = new ListHistoryTrxField(trxData, 0);
	                        			scr.add(trxField);
	                        		}
	                        		scr.invalidate();
	                        	}else{
	                        		//No History
	                        		setupLayoutError("Tidak ada transaksi.", false);
	                        	}
                            }else{
                            	setupLayoutError("Gagal mengambil histori transaksi.", true);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            setupLayoutError(Constant.txt_error_json, true);
                        }
                    }else{
                    	setupLayoutError(Constant.txt_error_no_connection, true);
                    }
                }
			});
		}
	}
	
	public void fieldChanged(Field field, int context) {
		if(field == btnRetry){
			scr.deleteAll();
        	scr.add(_ourAnimation);
        	scr.invalidate();
        	mediator.app.invokeLater(new Runnable() {
    			public void run() {
    				new DoHistoryTrxTask().run();
    			}
    		});
		}
	}
	
	public void setupLayoutError(String error, boolean isShowButton){
		layoutError.deleteAll();
		txtError.setText(error);
		layoutError.add(txtError);
		if(isShowButton) layoutError.add(btnRetry);
        int marginError = ((Constant.deviceHeight-header.getPreferredHeight()) - layoutError.getPreferredHeight()) / 2;
        layoutError.setMargin(marginError-15, 0, 0, 0);
        
        scr.deleteAll();
    	scr.add(layoutError);
    	scr.invalidate();
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }
}
