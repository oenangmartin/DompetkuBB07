package com.indosatapps.dompetku;

import java.util.Vector;

import net.rim.device.api.system.GIFEncodedImage;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import org.json.me.JSONArray;
import org.json.me.JSONObject;

import com.indosatapps.dompetku.bean.CategoryData;
import com.indosatapps.dompetku.bean.MerchantData;
import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.AnimatedGIFField;
import com.indosatapps.dompetku.field.ButtonFieldCustom;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.LabelFieldColor;
import com.indosatapps.dompetku.field.ListItemIconField;
import com.indosatapps.dompetku.utils.Connection;
import com.indosatapps.dompetku.utils.PageResultListener;

public class BeliBayarKategori extends MainScreen implements FieldChangeListener, PageResultListener{
	public static String TYPE_BELI = "Beli";
    public static String TYPE_BAYAR = "Bayar";
	Mediator mediator;
	Vector mListCategory;
	String type;
	AnimatedGIFField _ourAnimation;
	VerticalFieldManager scr, layoutError;
	ButtonFieldCustom btnRetry;
	HeaderField header;
	LabelFieldColor txtError;
	PageResultListener listener;
	
	public BeliBayarKategori(String type, PageResultListener listener, Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.listener = listener;
		this.mediator = mediator;
		this.type = type;
        header = new HeaderField(type, 0);
		add(header);
		
		GIFEncodedImage ourAnimation = (GIFEncodedImage) GIFEncodedImage.getEncodedImageResource("loading_orange.gif");
        _ourAnimation = new AnimatedGIFField(ourAnimation, FIELD_VCENTER|FIELD_HCENTER);
        int topMargin = ((Constant.deviceHeight-header.getPreferredHeight()) - _ourAnimation.getPreferredHeight()) / 2;
        _ourAnimation.setMargin(topMargin-15, 0, 0, 0);
        
        txtError = new LabelFieldColor("", Constant.colorFontGeneral, USE_ALL_WIDTH|DrawStyle.HCENTER);
        txtError.setFont(Assets.fontGlobal);
        btnRetry = new ButtonFieldCustom("COBA LAGI", Constant.deviceWidth/3, FIELD_HCENTER);
        btnRetry.setEditable(true);
        btnRetry.setMargin(5, 0, 0, 0);
        btnRetry.setChangeListener(this);
        layoutError = new VerticalFieldManager(USE_ALL_WIDTH);
        
        scr = new VerticalFieldManager(USE_ALL_WIDTH|USE_ALL_HEIGHT|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 15, 15, 15);
		scr.add(_ourAnimation);
		
		add(scr);
		
		initVariableList();
		mediator.app.invokeLater(new Runnable() {
			public void run() {
				if(mListCategory == null){
		            new GetCategoryListTask().run();
		        }else{
		            //set listview
		            initListView(mListCategory);
		        }
			}
		});
        
	}
	
	public class GetCategoryListTask implements Runnable{
		
		public GetCategoryListTask() {}
		
		public void run() {
			final String entity = Connection.getCategoryMerchantList();
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	Constant.mListBeli = new Vector();
                        	Constant.mListBayar = new Vector();
                        	JSONObject objs = new JSONObject(entity);
                        	JSONArray arrayBeli = objs.getJSONArray("Beli");
                        	JSONArray arrayBayar = objs.getJSONArray("Bayar");
                        	
                        	for(int i=0;i<arrayBeli.length();i++){
                        		JSONObject objBeli = arrayBeli.getJSONObject(i);
                        		String id = objBeli.getString("mid");
                        	    String label = objBeli.getString("mnama");
                        	    String imgURL = objBeli.getString("mimage");
                        	    JSONArray subMerchantBeli = objBeli.getJSONArray("sub_merchant");
                        	    Vector subMerchant = new Vector();
                        	    for(int j=0;j<subMerchantBeli.length();j++){
                        	    	JSONObject objSMBeli = subMerchantBeli.getJSONObject(j);
                            		String smId = objSMBeli.getString("sub_merchant_id");
                            	    String smLabel = objSMBeli.getString("sub_merchant_nama");
                            	    String smImgURL = objSMBeli.getString("img_path");
                            	    MerchantData smBeliData = new MerchantData(smId, smLabel, smImgURL);
                        	    	subMerchant.addElement(smBeliData);
                        	    }
                        	    
                        		CategoryData beliData = new CategoryData(id, label, imgURL, subMerchant);
                        		Constant.mListBeli.addElement(beliData);
                                Constant.mHashMapBeli.put(id, beliData);
                            }
                        	
                            for(int x=0;x<arrayBayar.length();x++){
                            	JSONObject objBayar = arrayBayar.getJSONObject(x);
                        		String id = objBayar.getString("mid");
                        	    String label = objBayar.getString("mnama");
                        	    String imgURL = objBayar.getString("mimage");
                        	    JSONArray subMerchantBayar = objBayar.getJSONArray("sub_merchant");
                        	    Vector subMerchant = new Vector();
                        	    for(int y=0;y<subMerchantBayar.length();y++){
                        	    	JSONObject objSMBayar = subMerchantBayar.getJSONObject(y);
                            		String smId = objSMBayar.getString("sub_merchant_id");
                            	    String smLabel = objSMBayar.getString("sub_merchant_nama");
                            	    String smImgURL = objSMBayar.getString("img_path");
                            	    MerchantData smBayarData = new MerchantData(smId, smLabel, smImgURL);
                        	    	subMerchant.addElement(smBayarData);
                        	    }
                        	    
                        		CategoryData bayarData = new CategoryData(id, label, imgURL, subMerchant);
                        		Constant.mListBayar.addElement(bayarData);
                                Constant.mHashMapBayar.put(id, bayarData);
                            }
                            
                            System.out.println(">> ListBeli size: "+Constant.mListBeli.size()+" | HashMapBeli size: "+Constant.mHashMapBeli.size());
                            System.out.println(">> ListBayar size: "+Constant.mListBayar.size()+" | HashMapBayar size: "+Constant.mHashMapBayar.size());
                        	
                        	initVariableList();
                        	if(mListCategory.size()>0){
                        		scr.deleteAll();
                        		initListView(mListCategory);
                        	}else{
                        		//No Data
                        		setupLayoutError("Tidak ada kategori.", false);
                        	}
                        }catch (Exception e){
                        	System.out.println(">> BeliBayarKategoriTask error: "+e.getMessage());
                            e.printStackTrace();
                            setupLayoutError(Constant.txt_error_json, true);
                        }
                    }else{
                    	setupLayoutError(Constant.txt_error_no_connection, true);
                    }
                }
			});
		}
	}
	
	public void initVariableList(){
        if(type.equals(TYPE_BELI)) mListCategory = Constant.mListBeli;
        else if(type.equals(TYPE_BAYAR)) mListCategory = Constant.mListBayar;
    }
	
	public void initListView(Vector menuItems){
		scr.deleteAll();
		for(int i=0;i<menuItems.size();i++){
			final CategoryData data = (CategoryData) menuItems.elementAt(i);
			ListItemIconField category = new ListItemIconField(Assets.noImage, data.getMnama(), data.getMimage(), Constant.deviceWidth-30, 0);
			category.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					//go to BeliBayarMerchant
					mediator.app.pushScreen(new BeliBayarMerchant(type, data.getMnama(), data.getMid(), BeliBayarKategori.this, mediator));
				}
			});
			scr.add(category);
		}
		scr.invalidate();
    }
	
	public void fieldChanged(Field field, int context) {
		if(field == btnRetry){
			scr.deleteAll();
        	scr.add(_ourAnimation);
        	scr.invalidate();
        	mediator.app.invokeLater(new Runnable() {
    			public void run() {
    				new GetCategoryListTask().run();
    			}
    		});
		}
	}
	
	public void setupLayoutError(String error, boolean isShowButton){
		layoutError.deleteAll();
		txtError.setText(error);
		layoutError.add(txtError);
		if(isShowButton) layoutError.add(btnRetry);
        int marginError = ((Constant.deviceHeight-header.getPreferredHeight()) - layoutError.getPreferredHeight()) / 2;
        layoutError.setMargin(marginError-15, 0, 0, 0);
        
        scr.deleteAll();
    	scr.add(layoutError);
    	scr.invalidate();
	}

	public void onPageResult(int requestCode, int resultCode, Object data) {
		if(resultCode == Constant.RESULT_OK){
			listener.onPageResult(Constant.REQUEST_CODE_REFRESH_HOME, resultCode, data);
        	close();
        }
	}
	
	public boolean onClose() {
		this.close();
		return true;
	}
}
