package com.indosatapps.dompetku;

import com.indosatapps.dompetku.bean.InboxData;
import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.utils.BasicUtils;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class InboxDetail extends MainScreen{
	Mediator mediator;
	
	public InboxDetail(InboxData inboxData, Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
        HeaderField header = new HeaderField("Inbox Detail", 0);
		add(header);
		
		String inboxText = "Ref ID: "+inboxData.getTrxID()+
		        "\nTanggal: "+BasicUtils.formatDateFromMilliseconds(inboxData.getDate(), "dd/MM/yyyy HH:mm")+
		        "\nAction: "+inboxData.getMenu()+" "+inboxData.getCategory();
		
		if(!inboxData.getMerchant().equals("-")){
			inboxText = inboxText + "\nMerchant: "+inboxData.getMerchant();
		}
		if(!inboxData.getTo().equals("-")){
			inboxText = inboxText + "\nKe: "+inboxData.getTo();
		}
		if(!inboxData.getAmount().equals("-")){
			inboxText = inboxText + "\nJumlah: "+inboxData.getAmount();
		}
		if(!inboxData.getAdditional().equals("-")){
			if(inboxData.getCategory().equals("Token"))
				inboxText = inboxText + "\nToken: "+inboxData.getAdditional();
			else
				inboxText = inboxText + "\nVoucher: "+inboxData.getAdditional();
		}

		EditField detailField = new EditField("", inboxText){
			protected void paint(Graphics graphics) {
				graphics.setFont(Assets.fontGlobal);
				graphics.setColor(Constant.colorFontGeneral);
				super.paint(graphics);
			}
		};
		detailField.setEditable(false);
		
		VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|USE_ALL_HEIGHT|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setPadding(15, 15, 15, 15);
		scr.add(detailField);
		
		add(scr);
	}
}
