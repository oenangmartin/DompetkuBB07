package com.indosatapps.dompetku;

import com.indosatapps.dompetku.data.Mediator;

import net.rim.device.api.applicationcontrol.ApplicationPermissions;
import net.rim.device.api.applicationcontrol.ApplicationPermissionsManager;
import net.rim.device.api.ui.UiApplication;


public class DompetkuApp extends UiApplication{
	/**
     * Entry point for application
     * @param args Command line arguments (not used)
     */ 
    public static void main(String[] args){
    	DompetkuApp app = new DompetkuApp();
    	app.enterEventDispatcher();
    }
    
    /**
     * Creates a new MyApp object
     */
    public DompetkuApp(){   
    	checkPermissions();
    	// Push a screen onto the UI stack for rendering.
    	pushScreen(new Splash(new Mediator(this)));
    }
    
    private static void checkPermissions() {
		ApplicationPermissionsManager apm = ApplicationPermissionsManager.getInstance();
		ApplicationPermissions original = apm.getApplicationPermissions();
		if ((original.getPermission(ApplicationPermissions.PERMISSION_INTERNET) == ApplicationPermissions.VALUE_ALLOW)
				&& (original.getPermission(ApplicationPermissions.PERMISSION_EMAIL) == ApplicationPermissions.VALUE_ALLOW)
				&& (original.getPermission(ApplicationPermissions.PERMISSION_FILE_API) == ApplicationPermissions.VALUE_ALLOW)
				&& (original.getPermission(ApplicationPermissions.PERMISSION_ORGANIZER_DATA) == ApplicationPermissions.VALUE_ALLOW)
				) {
			return;
		}
		
		ApplicationPermissions permRequest = new ApplicationPermissions();
		permRequest.addPermission(ApplicationPermissions.PERMISSION_INTERNET);
		permRequest.addPermission(ApplicationPermissions.PERMISSION_EMAIL);
		permRequest.addPermission(ApplicationPermissions.PERMISSION_FILE_API);
		permRequest.addPermission(ApplicationPermissions.PERMISSION_ORGANIZER_DATA);
		
		boolean acceptance = ApplicationPermissionsManager.getInstance().invokePermissionsRequest(permRequest);

		if (acceptance) {
			return;
		} else {
		}
	}
}
