package com.indosatapps.dompetku;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.ListItemField;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.PageResultListener;

public class MenuLain extends MainScreen implements FieldChangeListener, PageResultListener{
	Mediator mediator;
	ListItemField menuQR, menuInbox, menuFav, menuAdmin, menuInform, menuLogout;
	PageResultListener listener;
	
	public MenuLain(Mediator mediator, PageResultListener listener) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		this.listener = listener;
		HeaderField header = new HeaderField("Menu Lain", 0);
		add(header);
		
		menuQR = new ListItemField("QR", Constant.deviceWidth-30, 0);
		menuInbox = new ListItemField("Inbox", Constant.deviceWidth-30, 0);
		menuFav = new ListItemField("Favorit", Constant.deviceWidth-30, 0);
		menuAdmin = new ListItemField("Administrasi", Constant.deviceWidth-30, 0);
		menuInform = new ListItemField("Informasi", Constant.deviceWidth-30, 0);
		menuLogout = new ListItemField("Logout", Constant.deviceWidth-30, 0);
		
		menuQR.setChangeListener(this);
		menuInbox.setChangeListener(this);
		menuFav.setChangeListener(this);
		menuAdmin.setChangeListener(this);
		menuInform.setChangeListener(this);
		menuLogout.setChangeListener(this);
		
		VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 15, 15, 15);
		//scr.add(menuQR);
		scr.add(menuInbox);
		scr.add(menuFav);
		scr.add(menuAdmin);
		scr.add(menuInform);
		scr.add(menuLogout);
		
		add(scr);
	}

	public void fieldChanged(Field field, int context) {
		if(field == menuQR){
			//TBA
		}else if(field == menuInbox){
			mediator.app.pushScreen(new Inbox(mediator));
		}else if(field == menuFav){
			mediator.app.pushScreen(new Favorite(mediator));
		}else if(field == menuAdmin){
			mediator.app.pushScreen(new Administrasi(mediator, this));
		}else if(field == menuInform){
			mediator.app.pushScreen(new Informasi(mediator));
		}else if(field == menuLogout){
			int logout = Dialog.ask("Anda yakin ingin keluar dari aplikasi?", new String[]{"Ya", "Tidak"}, new int[]{1,0}, 1);
			if(logout == 1){
				Constant.mPersistData.setLastSession("");
				BasicUtils.savePersistData();
				System.exit(0);
			}
		}
	}
	
	public void onPageResult(int requestCode, int resultCode, Object data) {
		if(requestCode == Constant.REQUEST_CODE_GANTI_PIN && resultCode == Constant.RESULT_OK){
			listener.onPageResult(Constant.REQUEST_CODE_GANTI_PIN, Constant.RESULT_OK, null);
        	close();
		}
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }

}
