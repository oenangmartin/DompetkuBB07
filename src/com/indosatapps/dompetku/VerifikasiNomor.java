package com.indosatapps.dompetku;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.text.TextFilter;

import org.json.me.JSONObject;

import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.ButtonFieldCustom;
import com.indosatapps.dompetku.field.EditFieldCustom;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.PleaseWaitPopupScreen;
import com.indosatapps.dompetku.utils.Connection;
import com.indosatapps.dompetku.utils.PageResultListener;

public class VerifikasiNomor extends MainScreen implements FieldChangeListener, PageResultListener{
	Mediator mediator;
	EditFieldCustom editMSISDN;
	ButtonFieldCustom btnRequest;
	
	public VerifikasiNomor(Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		HeaderField header = new HeaderField("Verifikasi Nomor", 0);
		add(header);
		
		editMSISDN = new EditFieldCustom("Nomor HP", "", Constant.deviceWidth-30, 16, false, false, FIELD_HCENTER);
		editMSISDN.setFilter(TextFilter.get(TextFilter.NUMERIC));
		
		btnRequest = new ButtonFieldCustom("KIRIM", Constant.deviceWidth/3, FIELD_HCENTER);
		btnRequest.setEditable(true);
		btnRequest.setMargin(10, 0, 10, 0);
		btnRequest.setChangeListener(this);
		
		VerticalFieldManager scr = new VerticalFieldManager(USE_ALL_WIDTH|VERTICAL_SCROLL);
		scr.setPadding(15, 15, 15, 15);
		scr.add(editMSISDN.getField());
		scr.add(btnRequest);
		
		add(scr);
	}

	public void fieldChanged(Field field, int arg1) {
		if(field == btnRequest){
			editMSISDN.setIsError(false);
            String txtMSISDN = editMSISDN.getText();
            if(txtMSISDN.length() == 0){
            	editMSISDN.setIsError(true);
            	Dialog.alert("Mohon isi Nomor HP");
            }else{
                new DoRequestOTP(txtMSISDN);
            }
		}
	}
	
	public class DoRequestOTP implements Runnable{
		String msisdn;
		
		public DoRequestOTP(String msisdn) {
			this.msisdn = msisdn;
			PleaseWaitPopupScreen.showScreenAndWait(this, "Meminta kode verifikasi...");
		}
		
		public void run() {
			final String entity = Connection.doRequestOTP(msisdn);
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject objs = new JSONObject(entity);
                            int status = objs.getInt("status");
                            if(status == 0){
                            	mediator.app.pushScreen(new OTP(editMSISDN.getText(), mediator, VerifikasiNomor.this));
                            }else{
                            	Dialog.alert("Gagal meminta kode verifikasi.");
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Dialog.alert(Constant.txt_error_json);
                        }
                    }else{
                    	Dialog.alert(Constant.txt_error_no_connection);
                    }
                }
			});
		}
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }

	public void onPageResult(int requestCode, int resultCode, Object data) {
		if(resultCode == Constant.RESULT_OK){
            close();
        }
	}
}
