package com.indosatapps.dompetku;

import java.util.Hashtable;
import java.util.Vector;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.indosatapps.dompetku.bean.CategoryData;
import com.indosatapps.dompetku.bean.MerchantData;
import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.ListItemField;
import com.indosatapps.dompetku.utils.PageResultListener;

public class BeliBayarMerchant extends MainScreen implements PageResultListener{
	public static String TYPE_BELI = "Beli";
    public static String TYPE_BAYAR = "Bayar";
	Mediator mediator;
	String type, categoryName, categoryID;
    Hashtable mHashMapCategory;
    Vector mListCategory;
    Vector mListMerchant;
    VerticalFieldManager scr;
    PageResultListener listener;
    
	public BeliBayarMerchant(String type, String categoryName, String categoryID, PageResultListener listener, Mediator mediator) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.listener = listener;
		this.mediator = mediator;
        HeaderField header = new HeaderField(categoryName, 0);
		add(header);
		
		this.type = type;
		this.categoryName = categoryName;
		this.categoryID = categoryID;
		
		scr = new VerticalFieldManager(USE_ALL_WIDTH|USE_ALL_HEIGHT|VERTICAL_SCROLL|VERTICAL_SCROLLBAR);
		scr.setMargin(15, 15, 15, 15);
		
		add(scr);
		
		initVariableList();
		initListView(mListMerchant);
	}
	
	public void initVariableList(){
        if(type.equals(TYPE_BELI)){
            mListCategory = Constant.mListBeli;
            mHashMapCategory = Constant.mHashMapBeli;
        }else if(type.equals(TYPE_BAYAR)){
            mListCategory = Constant.mListBayar;
            mHashMapCategory = Constant.mHashMapBayar;
        }
        
        mListMerchant = ((CategoryData)mHashMapCategory.get(categoryID)).getSub_merchant();
    }
    
    public void initListView(Vector menuItems){
    	scr.deleteAll();
		for(int i=0;i<menuItems.size();i++){
			final MerchantData data = (MerchantData) menuItems.elementAt(i);
			ListItemField merchant;
			System.out.println(">> Merchant "+i+": "+data.getSub_merchant_nama()+"|"+data.getImg_path());
			if(data.getImg_path()!=null && data.getImg_path()!=""){
				merchant = new ListItemField(data.getSub_merchant_nama(), Assets.noImage, data.getImg_path(), Constant.deviceWidth-30, 0);
			}else{
				merchant = new ListItemField(data.getSub_merchant_nama(), Constant.deviceWidth-30, 0);
			}
			
			merchant.setChangeListener(new FieldChangeListener() {
				public void fieldChanged(Field field, int context) {
					//go to BeliBayarField
					mediator.app.pushScreen(new BeliBayarFields(type, categoryName, categoryID, data.getSub_merchant_id(), data.getSub_merchant_nama(), null, BeliBayarMerchant.this, mediator));
				}
			});
			scr.add(merchant);
		}
		scr.invalidate();
    }
	
	public void onPageResult(int requestCode, int resultCode, Object data) {
		if(resultCode == Constant.RESULT_OK){
			listener.onPageResult(Constant.REQUEST_CODE_REFRESH_HOME, resultCode, data);
        	close();
        }
	}
	
	public boolean onClose() {
		this.close();
		return true;
	}
}
