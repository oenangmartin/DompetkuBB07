package com.indosatapps.dompetku;

import java.util.Timer;
import java.util.TimerTask;

import javax.microedition.io.Connector;
import javax.wireless.messaging.BinaryMessage;
import javax.wireless.messaging.Message;
import javax.wireless.messaging.MessageConnection;
import javax.wireless.messaging.TextMessage;

import org.json.me.JSONObject;

import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.text.TextFilter;

import com.indosatapps.dompetku.data.Assets;
import com.indosatapps.dompetku.data.Constant;
import com.indosatapps.dompetku.data.Mediator;
import com.indosatapps.dompetku.field.ButtonFieldCustom;
import com.indosatapps.dompetku.field.EditFieldCustom;
import com.indosatapps.dompetku.field.HeaderField;
import com.indosatapps.dompetku.field.LabelFieldColor;
import com.indosatapps.dompetku.field.PleaseWaitPopupScreen;
import com.indosatapps.dompetku.utils.BasicUtils;
import com.indosatapps.dompetku.utils.Connection;
import com.indosatapps.dompetku.utils.PageResultListener;
import com.thirdparty.device.api.ui.container.JustifiedHorizontalFieldManager;
import com.thirdparty.device.api.ui.container.JustifiedVerticalFieldManager;

public class OTP extends MainScreen implements FieldChangeListener{
	Mediator mediator;
	PageResultListener listener;
	String msisdn;
	EditFieldCustom editOTP;
	ButtonFieldCustom btnResend, btnOK;
	LabelFieldColor txtTimer, txtDesc;
	JustifiedVerticalFieldManager scr;
	Timer timer = null;
	TimerTask task = null;
	long mille = 300000;
	SimpleDateFormat timeFormat = new SimpleDateFormat("mm:ss");
    
	public OTP(String msisdn, Mediator mediator, PageResultListener listener) {
		super(NO_VERTICAL_SCROLL|NO_VERTICAL_SCROLLBAR|NO_SYSTEM_MENU_ITEMS);
		VerticalFieldManager _manager = (VerticalFieldManager)getMainManager();
		_manager.setBackground(BackgroundFactory.createSolidBackground(Constant.colorPageBackground));
		
		this.mediator = mediator;
		this.listener = listener;
		this.msisdn = msisdn;
		HeaderField header = new HeaderField("Kode Verifikasi", 0);
		add(header);
		
		txtTimer = new LabelFieldColor("00:00", Constant.colorFontGeneral, USE_ALL_WIDTH|DrawStyle.HCENTER);
		txtTimer.setFont(Assets.fontGlobal);
		txtTimer.setMargin(20, 0, 0, 0);
		txtDesc = new LabelFieldColor("Mohon tunggu Kode Verifikasi Anda", Constant.colorFontGeneral, USE_ALL_WIDTH|DrawStyle.HCENTER);
		txtDesc.setFont(Assets.fontMini);
		
		LabelFieldColor txtOTP = new LabelFieldColor("Masukkan kode verifikasi dari SMS", Constant.colorFontGeneral, USE_ALL_WIDTH|DrawStyle.HCENTER);
		txtOTP.setFont(Assets.fontMini);
		editOTP = new EditFieldCustom("", "", Constant.deviceWidth-30, 6, false, false, 0);
		editOTP.setFilter(TextFilter.get(TextFilter.NUMERIC));
		
		int marginBtn = (Constant.deviceWidth/9)-15;
		btnResend = new ButtonFieldCustom("RESEND", Constant.deviceWidth/3, FIELD_LEFT);
		btnResend.setEditable(true);
		btnResend.setMargin(0, 0, 0, marginBtn);
		btnResend.setChangeListener(this);
		
		btnOK = new ButtonFieldCustom("OK", Constant.deviceWidth/3, FIELD_RIGHT);
		btnOK.setEditable(true);
		btnOK.setMargin(0, marginBtn, 0, 0);
		btnOK.setChangeListener(this);
		
		JustifiedHorizontalFieldManager btnManager = new JustifiedHorizontalFieldManager(btnResend, null, btnOK, USE_ALL_WIDTH);
		
		VerticalFieldManager bottomField = new VerticalFieldManager(USE_ALL_WIDTH);
		bottomField.add(txtOTP);
		bottomField.add(editOTP.getField());
		bottomField.add(btnManager);
		
		scr = new JustifiedVerticalFieldManager(txtDesc, txtTimer, bottomField, USE_ALL_HEIGHT|USE_ALL_WIDTH);
		scr.setPadding(15, 15, 15, 15);
		
		add(scr);
		
		SMSListener smsListener = new SMSListener();
		smsListener.start();
		
		timer = new Timer();
        startTimer();
	}

	public void startTimer(){
		task = new TimerTask() {
            public void run() {
                synchronized (UiApplication.getEventLock()) {
                    if(mille!=0){
                        //System.out.println("================="+timeFormat.formatLocal(mille)+"====================="+Thread.activeCount());
                        txtTimer.setText(timeFormat.formatLocal(mille));
                        mille = mille - 1000;
                    }else{
						txtTimer.setText("");
						mille = 300000;
                        task.cancel();
                        txtDesc.setText("Sistem kami tidak dapat mendeteksi Kode Verifikasi dari HP Anda. Silakan klik Resend untuk kirim ulang Kode Verifikasi atau "
                        		+ "masukkan manual Kode Verifikasi. Pastikan nomor HP yang Anda masukkan benar.");
                    }
                }
            }
        };
        timer.scheduleAtFixedRate(task, 0, 1000);
	}
	
	public void fieldChanged(Field field, int context) {
		if(field == btnResend){
			new DoRequestOTP(msisdn);
		}else if(field == btnOK){
			String mOTP = editOTP.getText();
			editOTP.setIsError(false);
	        if(mOTP.length() > 0){
	            new DoVerifOTP(msisdn, mOTP);
	        }else{
	        	editOTP.setIsError(true);
	        	Dialog.alert("Mohon isi Kode Verifikasi");
	        }
		}
	}
	
	public class DoRequestOTP implements Runnable{
		String msisdn;
		
		public DoRequestOTP(String msisdn) {
			this.msisdn = msisdn;
			PleaseWaitPopupScreen.showScreenAndWait(this, "Meminta kode verifikasi...");
		}
		
		public void run() {
			final String entity = Connection.doRequestOTP(msisdn);
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject objs = new JSONObject(entity);
                            int status = objs.getInt("status");
                            if(status == 0){
                            	txtTimer.setText("00:00");
        						mille = 300000;
                                txtDesc.setText("Mohon tunggu Kode Verifikasi Anda");
                                
                                task.cancel();
                                startTimer();
                            }else{
                            	Dialog.alert("Gagal meminta kode verifikasi.");
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Dialog.alert(Constant.txt_error_json);
                        }
                    }else{
                    	Dialog.alert(Constant.txt_error_no_connection);
                    }
                }
			});
		}
	}
	
	public class DoVerifOTP implements Runnable{
		String msisdn, otp;
		
		public DoVerifOTP(String msisdn, String otp) {
			this.msisdn = msisdn;
			this.otp = otp;
			PleaseWaitPopupScreen.showScreenAndWait(this, "Verifikasi kode...");
		}
		
		public void run() {
			final String entity = Connection.doConfirmOTP(msisdn, otp);
			UiApplication.getUiApplication().invokeLater(new Runnable() {
                public void run() {
                	if (!entity.equalsIgnoreCase("false")) {
                        try{
                        	JSONObject objs = new JSONObject(entity);
                            int status = objs.getInt("status");
                            if(status == 0 || status == -11){
                            	BasicUtils.saveFirstTimeInstall();
                                BasicUtils.saveUsernamePref(msisdn);
                                
                                //if registered, go to Login
                                //else go to Register
                                if(status == 0){
                                	mediator.app.pushScreen(new Login(mediator));
                                }else{
                                    BasicUtils.saveRegisterPending(Boolean.TRUE.toString());
                                    mediator.app.pushScreen(new Register(mediator));
                                }
                                listener.onPageResult(0, Constant.RESULT_OK, null);
                                close();
                            }else if(status == -14){
                            	Dialog.alert("Akun anda terblokir.");
                            }else if(status == -98){
                            	Dialog.alert("Koneksi ke server gagal. Silakan coba kembali.");
                            }else{
                            	Dialog.alert(""+objs.optString("msg", "Verifikasi kode gagal."));
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Dialog.alert(Constant.txt_error_json);
                        }
                    }else{
                    	Dialog.alert(Constant.txt_error_no_connection);
                    }
                }
			});
		}
	}
	
	class SMSListener extends Thread{
		/*MainScreen screen;
		
		public SMSListener(MainScreen screen) {
			this.screen = screen;
		}*/
		
		public void run() {
			try{
				//DatagramConnection _dc = (DatagramConnection)Connector.open("sms://");
				MessageConnection _mc = (MessageConnection)Connector.open("sms://:0"); 
				for(;;){
        			System.out.println(">> Check for incoming SMS.");
		        	try{
			        	/*Datagram d = _dc.newDatagram(_dc.getMaximumLength());
			        	_dc.receive(d);
						byte[] bytes = d.getData();
						String address = d.getAddress();
						String msg = new String(bytes);
						System.out.println(">> Received SMS text from " + address + " : " + msg);*/
		        		
		        		Message m = _mc.receive();
		        		String address = m.getAddress();
		        		String msg = null;
	        		     if ( m instanceof TextMessage ){
	        		          TextMessage tm = (TextMessage)m;
	        		          msg = tm.getPayloadText();
	        		     }
	        		     else if (m instanceof BinaryMessage) {

	        		          //StringBuffer buf = new StringBuffer();
	        		          byte[] data = ((BinaryMessage) m).getPayloadData();
	        		          // convert Binary Data to Text
	        		          msg = new String(data, "UTF-8"); 
	        		     }
	        		     else
	        		          System.out.println("Invalid Message Format");
	        		     
	        		     System.out.println("Received SMS text from " + address + " : " + msg);
	        		     if(address.equals("sms://788") || address.equals("sms://789")){
							System.out.println("Parsing sms now...");
							//Kode verifikasi anda: XXXXXX Silakan masukkan kode verifikasi ini pada aplikasi Dompetku
			                if(msg.startsWith("Kode verifikasi anda")){
			                    String otp = msg.substring(msg.indexOf(":")+2, msg.indexOf(":")+8);
			                    System.out.println(">> OTP: "+otp);
			                    editOTP.setText(otp);
			                    new DoVerifOTP(msisdn, otp);
			                }
						}
		        	}catch (Exception e) {
		        		System.out.println(">> SMS Reciever error: "+e.getMessage());
		    			e.printStackTrace();
					}
		        }
			}catch (Exception e) {
				System.out.println(">> SMS Connector error: "+e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	public boolean onClose(){
		this.close();
    	return true;
    }
}
